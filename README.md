# lms

# installation process:
Step 1: Install npm
Step 2: Get `.env` file and paste into root directory
Step 2: run `npm i`
Step 3: run `npm start`
Step 4: run `npm run format-code` before commit to fix code format

# .env rules:
Step 1: variable name should starts with `REACT_APP_<variable name>` otherwise env variables won't work

# commit code procedure:
 Step 1: run `npm run format-code` before commit to fix code format
 Step 2: then commit your code

# common mistakes:
1) file/folder name should be always lowercase : 
    some good e.g: test-file.js,test.js
    some bad e.g: Testfile.js,TestFile.js,testfilr.js

2) Variable name should be camel case:
   some good e.g: testCase,
   some bad e.g: TestCase, Testcase, testcase

3) constant variable should be in upper case:
   some good e.g: RULES
   don't use any other format for defining constant variable

5) Component Name should start with Capital letter:
    some good e.g: TestCase etc
    some bad e.g: testCase,Testcase etc

4) create new folder with index.js
      inside index.js file define all the components so we can reduce import lines another folders/components

5) do not use `div`. try to use `Box`, `Flex`, `FlexWrap`

      