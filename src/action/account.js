import {
  GET_ADMINS_SUCCESS,
  GET_TEACHERS_SUCCESS,
  GET_STUDENTS_SUCCESS,
} from './defined/account';

import { apiCall, checkAPIResponse, addToCookie } from '../utils/helper';

export const setUserData = (data) => {
  return async (dispatch) => {
    try {
      dispatch({ type: 'LOGIN_SUCCESS', data });
    } catch (e) {
      dispatch({ type: 'LOGIN_FAIL' });
    }
  };
};

export const login = (userId, password) => {
  return async (dispatch) => {
    dispatch({ type: 'LOGIN_START' });
    try {
      let response = await apiCall('/login', 'POST', { userId, password });
      checkAPIResponse(response);
      response = await response.json();
      const { data } = response;
      const { data: responseData = {} } = data;
      const { token = '' } = responseData;
      if (token) {
        addToCookie('user', token);
      }
      dispatch({ type: 'LOGIN_SUCCESS', data: responseData });
    } catch (e) {
      dispatch({ type: 'LOGIN_FAIL' });
    }
  };
};

export const fetchUsersByRole = ({ role }) => {
  return async (dispatch, getState) => {
    const { account = {} } = getState();
    const list = account[role + 'List'];
    !list?.length && dispatch({ type: 'USERS_LIST_FETHCING', payload: { [role + 'Fetching']: true } });
    try {
      let response = await apiCall(`/users/${role}`, 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { users } = response.data;
      dispatch({ type: 'USERS_LIST_SUCCESS', payload: { [role + 'List']: users, [role + 'Fetching']: false } });
    } catch (e) {
      dispatch({ type: 'USER_LIST_FAIL', payload: { [role + 'Fetching']: false } });
    }
  };
};

export const fetchAdmins = () => {
  return async (dispatch) => {
    dispatch({ type: 'USER_START' });
    try {
      let response = await apiCall(`/users/admin`, 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { users } = response.data;
      dispatch({ type: GET_ADMINS_SUCCESS, admins: users });
    } catch (e) {
      dispatch({ type: 'USER_FAIL' });
    }
  };
};

export const fetchTeachers = () => {
  return async (dispatch) => {
    dispatch({ type: 'USER_START' });
    try {
      let response = await apiCall(`/users/teacher`, 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { users } = response.data;
      dispatch({ type: GET_TEACHERS_SUCCESS, teachers: users });
    } catch (e) {
      dispatch({ type: 'USER_FAIL' });
    }
  };
};

export const fetchTeachersByGroup = ({ id }) => {
  return async (dispatch) => {
    dispatch({ type: 'USER_START' });
    try {
      let response = await apiCall(`/users/group/${id}/teacher`, 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { users } = response.data;
      dispatch({ type: GET_TEACHERS_SUCCESS, teachers: users });
    } catch (e) {
      dispatch({ type: 'USER_FAIL' });
    }
  };
};

export const fetchStudents = () => {
  return async (dispatch) => {
    dispatch({ type: 'USER_START' });
    try {
      let response = await apiCall(`/users/student`, 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { users } = response.data;
      dispatch({ type: GET_STUDENTS_SUCCESS, students: users });
    } catch (e) {
      dispatch({ type: 'USER_FAIL' });
    }
  };
};

export const createUser = (data) => {
  return async (dispatch) => {
    dispatch({ type: 'CREATE_USER_START' });
    try {
      let response = await apiCall('/user', 'POST', data.payload);

      checkAPIResponse(response);
      dispatch(fetchUsersByRole({ role: data.payload.users[0].role }));
      dispatch({ type: 'USER_CREATE_SUCCESS' });
    } catch (error) {
      dispatch({ type: 'USER_FAIL' });
    }
  };
};

export const updateUser = (data) => {
  return async (dispatch) => {
    dispatch({ type: 'UPDATE_USER_START' });
    try {
      let response = await apiCall(`/user/${data.id}`, 'PUT', data.payload);
      checkAPIResponse(response);
      dispatch(fetchUsersByRole({ role: data.payload.role }));
      dispatch({ type: 'USER_UPDATE_SUCCESS' });
    } catch (error) {
      dispatch({ type: 'USER_FAIL' });
    }
  };
};

export const deleteUser = ({ id }) => {
  return async (dispatch, getState) => {
    dispatch({ type: 'USER_START' });
    try {
      let response = await apiCall(`/user/${id}`, 'DELETE');
      checkAPIResponse(response);
      let { users } = getState().account;
      users = users.filter((user) => user._id !== id);
      dispatch({ type: 'USERS_SUCCESS', users });
    } catch (e) {
      dispatch({ type: 'USER_FAIL' });
    }
  };
};


export const logout = () => {
  return async (dispatch) => {
    dispatch({ type: 'LOGOUT' });
  };
}