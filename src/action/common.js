import { SET_GROUP, SET_SUBJECT, SET_LESSON, SET_TOPIC, RESET_LOADER } from './defined/common';

export const setGroup = (id = '', name = '') => {
  return (dispatch) => {
    dispatch({ type: SET_GROUP, data: { id, name } });
  };
};

export const setSubject = (id = '', name = '') => {
  return (dispatch) => {
    dispatch({ type: SET_SUBJECT, data: { id, name } });
  };
};

export const setLesson = (id = '', name = '') => {
  return (dispatch) => {
    dispatch({ type: SET_LESSON, data: { id, name } });
  };
};

export const setTopic = (id = '', name = '') => {
  return (dispatch) => {
    dispatch({ type: SET_TOPIC, data: { id, name } });
  };
};

export const resetLoaders = () => {
  return (dispatch) => {
    dispatch({ type: RESET_LOADER });
  };
}