export const SET_GROUP = 'SET_GROUP';
export const SET_SUBJECT = 'SET_SUBJECT';
export const SET_LESSON = 'SET_LESSON';
export const SET_TOPIC = 'SET_TOPIC';
export const RESET_LOADER = 'RESET_LOADER';
