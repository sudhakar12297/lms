export const GET_DOUBTS_START = 'GET_DOUBTS_START';
export const GET_DOUBTS_SUCCESS = 'GET_DOUBTS_SUCCESS';
export const GET_DOUBTS_FAIL = 'GET_DOUBTS_FAIL';

export const POST_DOUBT_START = 'POST_DOUBT_START';
export const POST_DOUBT_SUCCESS = 'POST_DOUBT_SUCCESS';
export const POST_DOUBT_FAIL = 'POST_DOUBT_FAIL';

export const UPDATE_DOUBT_START = 'UPDATE_DOUBT_START';
export const UPDATE_DOUBT_SUCCESS = 'UPDATE_DOUBT_SUCCESS';
export const UPDATE_DOUBT_FAIL = 'UPDATE_DOUBT_FAIL';

export const DELETE_DOUBT_START = 'DELETE_DOUBT_START';
export const DELETE_DOUBT_SUCCESS = 'DELETE_DOUBT_SUCCESS';
export const DELETE_DOUBT_FAIL = 'DELETE_DOUBT_FAIL';
