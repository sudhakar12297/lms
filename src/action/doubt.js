import { apiCall, checkAPIResponse } from '../utils/helper';
import {
    GET_DOUBTS_START,
    GET_DOUBTS_SUCCESS,
    GET_DOUBTS_FAIL,
    POST_DOUBT_START,
    POST_DOUBT_SUCCESS,
    POST_DOUBT_FAIL,
    UPDATE_DOUBT_START,
    UPDATE_DOUBT_SUCCESS,
    UPDATE_DOUBT_FAIL,
    DELETE_DOUBT_START,
    DELETE_DOUBT_SUCCESS,
    DELETE_DOUBT_FAIL,
} from './defined/doubt';

export const fetchMyDoubts = () => {
    return async (dispatch, getState) => {
        dispatch({ type: GET_DOUBTS_START });
        try {
            const { user } = getState().account
            let response = await apiCall(`/doubt/user/${user.userId}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { doubts } = response.data;
            dispatch({ type: 'GET_MY_DOUBTS_SUCCESS', doubts });
        } catch (e) {
            console.log(e);
            dispatch({ type: GET_DOUBTS_FAIL });
        }
    };
};

export const fetchDoubtsByForTopic = ({ id }) => {
    return async (dispatch) => {
        dispatch({ type: GET_DOUBTS_START });
        try {
            let response = await apiCall(`/doubt/${id}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { doubts } = response.data;
            dispatch({ type: GET_DOUBTS_SUCCESS, doubts });
        } catch (e) {
            dispatch({ type: GET_DOUBTS_FAIL });
        }
    };
};

export const createDoubt = (data) => {
    const { payload, parentId } = data;
    return async (dispatch) => {
        dispatch({ type: POST_DOUBT_START });
        try {
            const route = parentId ? `/doubt/reply/${parentId}` : '/doubt'
            let response = await apiCall(route, 'POST', payload);
            checkAPIResponse(response);
            dispatch(fetchDoubtsByForTopic({ id: payload.topicId }));
            dispatch({ type: POST_DOUBT_SUCCESS });
        } catch (e) {
            dispatch({ type: POST_DOUBT_FAIL });
        }
    };
};

export const updateDoubt = (data) => {
    const { isEdit, payload, replyId, parentId } = data;
    return async (dispatch) => {
        dispatch({ type: UPDATE_DOUBT_START });
        try {
            const route = isEdit ? `/doubt/update/${parentId}/${replyId}` : `/doubt/update/${parentId}`
            let response = await apiCall(route, 'PUT', payload);
            checkAPIResponse(response);
            // dispatch(fetchDoubtsByForTopic({ id: payload.topicId }));
            dispatch({ type: UPDATE_DOUBT_SUCCESS });
        } catch (e) {
            dispatch({ type: UPDATE_DOUBT_FAIL });
        }
    };
};

export const deleteDoubt = (data) => {
    const { isParent, parentId, replyId, topicId } = data;
    return async (dispatch) => {
        dispatch({ type: DELETE_DOUBT_START });
        try {
            const route = isParent ? `/doubt/${parentId}` : `/doubt/${parentId}/${replyId}`
            let response = await apiCall(route, 'DELETE');
            checkAPIResponse(response);
            dispatch(fetchDoubtsByForTopic({ id: topicId }));
            dispatch({ type: DELETE_DOUBT_SUCCESS });
        } catch (e) {
            dispatch({ type: DELETE_DOUBT_FAIL });
        }
    };
};
