import { apiCall, checkAPIResponse } from '../utils/helper';

const GROUP_START = 'GROUP_START';
const DELETE_GROUP_START = 'DELETE_GROUP_START';
const CREATE_GROUP_START = 'CREATE_GROUP_START';
const UPDATE_GROUP_START = 'UPDATE_GROUP_START';
const GROUP_FAIL = 'GROUP_FAIL';

export const fetchGroups = () => {
  return async (dispatch) => {
    dispatch({ type: GROUP_START });
    try {
      let response = await apiCall('/groups', 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { data } = response;
      dispatch({ type: 'GET_GROUPS_SUCCESS', data });
    } catch (e) {
      dispatch({ type: GROUP_FAIL });
    }
  };
};

export const fetchMyGroups = () => {
  return async (dispatch) => {
    dispatch({ type: GROUP_START });
    try {
      let response = await apiCall('/mygroups', 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { groups } = response.data;
      dispatch({ type: 'GET_MY_GROUPS_SUCCESS', groups });
    } catch (e) {
      dispatch({ type: GROUP_FAIL });
    }
  };
};

export const fetchGroupsByUser = () => {
  return async (dispatch, getState) => {
    const { account = {} } = getState(),
      { user = {} } = account,
      { groupId = [] } = user;
    dispatch({ type: GROUP_START });
    try {
      let response = await apiCall('/groups/user', 'POST', { groupIds: groupId });
      checkAPIResponse(response);
      response = await response.json();
      const { data } = response;
      dispatch({ type: 'GET_GROUPS_SUCCESS', data });
    } catch (e) {
      dispatch({ type: GROUP_FAIL });
    }
  };
};

export const fetchGroupById = ({ id }) => {
  return async (dispatch) => {
    dispatch({ type: GROUP_START });
    try {
      let response = await apiCall(`/group/${id}`, 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { data } = response;
      const { group = [] } = data;
      const [firstGroup = {}] = group;
      dispatch({ type: 'GET_GROUP_SUCCESS', data: firstGroup });
    } catch (e) {
      dispatch({ type: GROUP_FAIL });
    }
  };
};

export const createGroup = (data) => {
  return async (dispatch) => {
    dispatch({ type: CREATE_GROUP_START });
    try {
      let response = await apiCall('/group', 'POST', data.payload);
      checkAPIResponse(response);
      dispatch(fetchGroups());
      dispatch({ type: 'POST_GROUP_SUCCESS' });
    } catch (e) {
      dispatch({ type: GROUP_FAIL });
    }
  };
};

export const updateGroup = (data) => {
  return async (dispatch) => {
    dispatch({ type: UPDATE_GROUP_START });
    try {
      let response = await apiCall(`/group/${data.id}`, 'PUT', data.payload);
      checkAPIResponse(response);
      dispatch(fetchGroups());
      dispatch({ type: 'PUT_GROUP_SUCCESS' });
    } catch (e) {
      dispatch({ type: GROUP_FAIL });
    }
  };
};

export const deleteGroup = (data) => {
  return async (dispatch, getState) => {
    dispatch({ type: DELETE_GROUP_START });
    try {
      let response = await apiCall(`/group/${data.id}`, 'DELETE');
      checkAPIResponse(response);
      let { groups } = getState().group;
      groups = groups.filter((group) => group._id !== data.id);
      dispatch({ type: 'DELETE_GROUP_SUCCESS', groups });
    } catch (e) {
      dispatch({ type: GROUP_FAIL });
    }
  };
};
