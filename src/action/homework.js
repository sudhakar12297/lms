import { apiCall, checkAPIResponse } from '../utils/helper';
import {
    GET_HOMEWORKS_START,
    GET_HOMEWORKS_SUCCESS,
    GET_HOMEWORKS_FAIL,
    POST_HOMEWORK_START,
    POST_HOMEWORK_SUCCESS,
    POST_HOMEWORK_FAIL,
    UPDATE_HOMEWORK_START,
    UPDATE_HOMEWORK_SUCCESS,
    UPDATE_HOMEWORK_FAIL,
    DELETE_HOMEWORK_START,
    DELETE_HOMEWORK_SUCCESS,
    DELETE_HOMEWORK_FAIL,
} from './defined/homework';

export const fetchMyHomeworks = () => {
    return async (dispatch) => {
        dispatch({ type: GET_HOMEWORKS_START });
        try {
            let response = await apiCall(`/myhomework`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { homeworks } = response.data;
            dispatch({ type: GET_HOMEWORKS_SUCCESS, homeworks });
        } catch (e) {
            dispatch({ type: GET_HOMEWORKS_FAIL });
        }
    };
};

export const createHomework = (data) => {
    const { payload } = data;
    return async (dispatch) => {
        dispatch({ type: POST_HOMEWORK_START });
        try {
            let response = await apiCall('/homework', 'POST', payload);
            checkAPIResponse(response);
            dispatch(fetchMyHomeworks());
            dispatch({ type: POST_HOMEWORK_SUCCESS });
        } catch (e) {
            dispatch({ type: POST_HOMEWORK_FAIL });
        }
    };
};

export const updateHomework = (data) => {
    const { id, payload, } = data;
    return async (dispatch) => {
        dispatch({ type: UPDATE_HOMEWORK_START });
        try {
            let response = await apiCall(`/homework/${id}`, 'PUT', payload);
            checkAPIResponse(response);
            dispatch({ type: UPDATE_HOMEWORK_SUCCESS });
        } catch (e) {
            dispatch({ type: UPDATE_HOMEWORK_FAIL });
        }
    };
};

export const deleteHomework = (data) => {
    return async (dispatch, getState) => {
        dispatch({ type: DELETE_HOMEWORK_START });
        try {
            let response = await apiCall(`/homework/${data.id}`, 'DELETE');
            checkAPIResponse(response);
            let { homeworks } = getState().homework
            homeworks = homeworks.filter(homework => homework._id !== data.id)
            dispatch({ type: DELETE_HOMEWORK_SUCCESS, homeworks });
        } catch (e) {
            dispatch({ type: DELETE_HOMEWORK_FAIL });
        }
    };
};
