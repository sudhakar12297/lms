import { apiCall, checkAPIResponse } from '../utils/helper';
import {
  GET_LESSONS_START,
  GET_LESSONS_SUCCESS,
  GET_LESSONS_FAIL,
  POST_LESSON_START,
  POST_LESSON_SUCCESS,
  POST_LESSON_FAIL,
  UPDATE_LESSON_START,
  UPDATE_LESSON_SUCCESS,
  UPDATE_LESSON_FAIL,
  DELETE_LESSON_START,
  DELETE_LESSON_SUCCESS,
  DELETE_LESSON_FAIL,
} from './defined/lesson';

export const fetchLessons = () => {
  return async (dispatch) => {
    dispatch({ type: GET_LESSONS_START });
    try {
      let response = await apiCall('/lesson', 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { lessons } = response.data;
      dispatch({ type: GET_LESSONS_SUCCESS, lessons });
    } catch (e) {
      dispatch({ type: GET_LESSONS_FAIL });
    }
  };
};

export const fetchLessonsBySubject = ({ id }) => {
  return async (dispatch) => {
    dispatch({ type: GET_LESSONS_START });
    try {
      let response = await apiCall(`/chapter/${id}`, 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { chapters } = response.data;
      dispatch({ type: GET_LESSONS_SUCCESS, lessons: chapters });
    } catch (e) {
      dispatch({ type: GET_LESSONS_FAIL });
    }
  };
};

export const createLesson = (data) => {
  const { payload, subjectId } = data;
  return async (dispatch) => {
    dispatch({ type: POST_LESSON_START });
    try {
      let response = await apiCall(`/chapter/${subjectId}`, 'POST', payload);
      checkAPIResponse(response);
      dispatch(fetchLessonsBySubject({ id: subjectId }));
      dispatch({ type: POST_LESSON_SUCCESS });
    } catch (e) {
      dispatch({ type: POST_LESSON_FAIL });
    }
  };
};

export const updateLesson = (data) => {
  const { id, payload, subjectId } = data;
  return async (dispatch) => {
    dispatch({ type: UPDATE_LESSON_START });
    try {
      let response = await apiCall(
        `/chapter/${subjectId}/${id}`,
        'PUT',
        payload
      );
      checkAPIResponse(response);
      dispatch(fetchLessonsBySubject({ id: subjectId }));
      dispatch({ type: UPDATE_LESSON_SUCCESS });
    } catch (e) {
      dispatch({ type: UPDATE_LESSON_FAIL });
    }
  };
};

export const deleteLesson = (data) => {
  const { id, subjectId } = data;
  return async (dispatch) => {
    dispatch({ type: DELETE_LESSON_START });
    try {
      let response = await apiCall(`/chapter/${subjectId}/${id}`, 'DELETE');
      checkAPIResponse(response);
      dispatch(fetchLessonsBySubject({ id: subjectId }));
      dispatch({ type: DELETE_LESSON_SUCCESS });
    } catch (e) {
      dispatch({ type: DELETE_LESSON_FAIL });
    }
  };
};
