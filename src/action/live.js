import { apiCall, checkAPIResponse, filterLives } from '../utils/helper';
import {
    GET_LIVES_START,
    GET_LIVES_SUCCESS,
    GET_LIVES_FAIL,
    POST_LIVE_START,
    POST_LIVE_SUCCESS,
    POST_LIVE_FAIL,
    UPDATE_LIVE_START,
    UPDATE_LIVE_SUCCESS,
    UPDATE_LIVE_FAIL,
    DELETE_LIVE_START,
    DELETE_LIVE_SUCCESS,
    DELETE_LIVE_FAIL,
} from './defined/live';

export const fetchLives = () => {
    return async (dispatch) => {
        dispatch({ type: GET_LIVES_START });
        try {
            let response = await apiCall('/live', 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { lives } = response.data;
            dispatch({ type: GET_LIVES_SUCCESS, lives });
        } catch (e) {
            dispatch({ type: GET_LIVES_FAIL });
        }
    };
};

export const fetchLivesByUserId = () => {
    return async (dispatch, getState) => {
        dispatch({ type: GET_LIVES_START });
        try {
            const { account } = getState()
            let response = await apiCall(`/live-session/user/${account.user.userId}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { sessions } = response.data;
            dispatch({ type: GET_LIVES_SUCCESS, lives: sessions });
        } catch (e) {
            dispatch({ type: GET_LIVES_FAIL });
        }
    };
};

export const fetchLivesByGroups = ({ groupId }) => {
    return async (dispatch) => {
        dispatch({ type: GET_LIVES_START });
        try {
            let response = await apiCall(`/live-session/group/${groupId}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { sessions } = response.data;
            dispatch({ type: GET_LIVES_SUCCESS, lives: sessions });
        } catch (e) {
            dispatch({ type: GET_LIVES_FAIL });
        }
    };
};

export const createLive = (data) => {
    const { payload } = data;
    return async (dispatch) => {
        dispatch({ type: POST_LIVE_START });
        try {
            let response = await apiCall('/live-session', 'POST', payload);
            checkAPIResponse(response);
            dispatch(fetchLivesByUserId())
            dispatch({ type: POST_LIVE_SUCCESS });
        } catch (e) {
            dispatch({ type: POST_LIVE_FAIL });
        }
    };
};

export const updateLive = (data) => {
    const { id, payload } = data;
    return async (dispatch) => {
        dispatch({ type: UPDATE_LIVE_START });
        try {
            let response = await apiCall(`/live-session/${id}`, 'PUT', payload);
            checkAPIResponse(response);
            dispatch(fetchLivesByUserId())
            dispatch({ type: UPDATE_LIVE_SUCCESS });
        } catch (e) {
            dispatch({ type: UPDATE_LIVE_FAIL });
        }
    };
};

export const deleteLive = (data) => {
    const { id } = data;
    return async (dispatch, getState) => {
        dispatch({ type: DELETE_LIVE_START });
        try {
            let response = await apiCall(`/live-session/${id}`, 'DELETE');
            checkAPIResponse(response);
            let { lives } = getState().live
            lives = lives.filter(live => live._id !== id)
            dispatch({ type: DELETE_LIVE_SUCCESS, lives });
        } catch (e) {
            console.log(e);

            dispatch({ type: DELETE_LIVE_FAIL });
        }
    };
};
