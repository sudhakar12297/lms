import { apiCall, checkAPIResponse } from '../utils/helper';
import {
    GET_NOTE_START,
    GET_NOTE_SUCCESS,
    GET_NOTE_FAIL,
    UPDATE_NOTE_START,
    UPDATE_NOTE_SUCCESS,
    UPDATE_NOTE_FAIL,
} from './defined/note';

export const fetchMyNotes = () => {
    return async (dispatch, getState) => {
        dispatch({ type: GET_NOTE_START });
        try {
            const { user } = getState().account
            let response = await apiCall(`/notes/${user.userId}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { notes } = response.data;
            dispatch({ type: 'GET_MY_NOTES_SUCCESS', notes });
        } catch (e) {
            dispatch({ type: GET_NOTE_FAIL });
        }
    };
};

export const fetchNote = (data) => {
    const { topicId, userId } = data;
    return async (dispatch) => {
        dispatch({ type: GET_NOTE_START });
        try {
            let response = await apiCall(`/notes/${topicId}/${userId}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { notes } = response.data;
            dispatch({ type: GET_NOTE_SUCCESS, note: notes[0].notes });
        } catch (e) {
            dispatch({ type: GET_NOTE_FAIL });
        }
    };
};

export const updateNote = (data) => {
    const { payload, topicId, userId } = data;
    console.log(data);

    return async (dispatch, getState) => {
        dispatch({ type: UPDATE_NOTE_START });
        try {
            const { note } = getState().note
            payload['notes'] = note
            let response = await apiCall(`/notes/${topicId}/${userId}`, 'PUT', payload);
            checkAPIResponse(response);
            // dispatch(fetchNote({ topicId, userId }))
            dispatch({ type: UPDATE_NOTE_SUCCESS });
        } catch (e) {
            dispatch({ type: UPDATE_NOTE_FAIL });
        }
    };
};
