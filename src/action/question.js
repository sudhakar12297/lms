import { apiCall, checkAPIResponse } from '../utils/helper';
import {
    GET_QUESTIONS_START,
    GET_QUESTIONS_SUCCESS,
    GET_QUESTIONS_FAIL,
    POST_QUESTION_START,
    POST_QUESTION_SUCCESS,
    POST_QUESTION_FAIL,
    UPDATE_QUESTION_START,
    UPDATE_QUESTION_SUCCESS,
    UPDATE_QUESTION_FAIL,
    DELETE_QUESTION_START,
    DELETE_QUESTION_SUCCESS,
    DELETE_QUESTION_FAIL,
} from './defined/question';

export const fetchQuestions = () => {
    return async (dispatch) => {
        dispatch({ type: GET_QUESTIONS_START });
        try {
            let response = await apiCall('/question', 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { questions } = response.data;
            dispatch({ type: GET_QUESTIONS_SUCCESS, questions });
        } catch (e) {
            dispatch({ type: GET_QUESTIONS_FAIL });
        }
    };
};

export const fetchQuestionsBySubject = ({ id }) => {
    return async (dispatch) => {
        dispatch({ type: GET_QUESTIONS_START });
        try {
            let response = await apiCall(`/question/${id}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { questions } = response.data;
            dispatch({ type: GET_QUESTIONS_SUCCESS, questions });
        } catch (e) {
            dispatch({ type: GET_QUESTIONS_FAIL });
        }
    };
};

export const fetchQuestionsByQuestionIds = ({ questionIds }) => {
    return async (dispatch) => {
        dispatch({ type: GET_QUESTIONS_START });
        try {
            let response = await apiCall(`/questionIds`, 'POST', { questionIds });
            checkAPIResponse(response);
            response = await response.json();
            const { questions } = response.data;
            dispatch({ type: GET_QUESTIONS_SUCCESS, questions });
        } catch (e) {
            dispatch({ type: GET_QUESTIONS_FAIL });
        }
    };
};

export const createQuestion = (data) => {
    const { payload } = data;
    return async (dispatch) => {
        dispatch({ type: POST_QUESTION_START });
        try {
            let response = await apiCall('/question', 'POST', payload);
            checkAPIResponse(response);
            dispatch(fetchQuestions())
            // dispatch(fetchQuestionsByGroup({ id: groupId }));s
            dispatch({ type: POST_QUESTION_SUCCESS });
        } catch (e) {
            dispatch({ type: POST_QUESTION_FAIL });
        }
    };
};

export const updateQuestion = (data) => {
    const { id, payload } = data;
    return async (dispatch) => {
        dispatch({ type: UPDATE_QUESTION_START });
        try {
            let response = await apiCall(`/question/${id}`, 'PUT', payload);
            checkAPIResponse(response);
            dispatch(fetchQuestions());
            dispatch({ type: UPDATE_QUESTION_SUCCESS });
        } catch (e) {
            dispatch({ type: UPDATE_QUESTION_FAIL });
        }
    };
};

export const deleteQuestion = (data) => {
    const { id } = data;
    return async (dispatch) => {
        dispatch({ type: DELETE_QUESTION_START });
        try {
            let response = await apiCall(`/question/${id}`, 'DELETE');
            checkAPIResponse(response);
            dispatch(fetchQuestions());
            dispatch({ type: DELETE_QUESTION_SUCCESS });
        } catch (e) {
            dispatch({ type: DELETE_QUESTION_FAIL });
        }
    };
};

