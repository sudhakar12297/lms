import { apiCall, checkAPIResponse, filterSubjects } from '../utils/helper';
import {
  GET_SUBJECTS_START,
  GET_SUBJECTS_SUCCESS,
  GET_SUBJECTS_FAIL,
  POST_SUBJECT_START,
  POST_SUBJECT_SUCCESS,
  POST_SUBJECT_FAIL,
  UPDATE_SUBJECT_START,
  UPDATE_SUBJECT_SUCCESS,
  UPDATE_SUBJECT_FAIL,
  DELETE_SUBJECT_START,
  DELETE_SUBJECT_SUCCESS,
  DELETE_SUBJECT_FAIL,
} from './defined/subject';

export const fetchSubjects = () => {
  return async (dispatch) => {
    dispatch({ type: GET_SUBJECTS_START });
    try {
      let response = await apiCall('/subject', 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { subjects } = response.data;
      dispatch({ type: GET_SUBJECTS_SUCCESS, subjects });
    } catch (e) {
      dispatch({ type: GET_SUBJECTS_FAIL });
    }
  };
};

export const fetchMySubjects = () => {
  return async (dispatch) => {
    dispatch({ type: GET_SUBJECTS_START });
    try {
      let response = await apiCall('/mysubjects', 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { subjects } = response.data;
      dispatch({ type: 'GET_MY_SUBJECTS_SUCCESS', subjects });
    } catch (e) {
      dispatch({ type: GET_SUBJECTS_FAIL });
    }
  };
};

export const fetchSubjectsBySubjectIds = ({ subjectIds }) => {
  return async (dispatch) => {
    dispatch({ type: GET_SUBJECTS_START });
    try {
      let response = await apiCall('/subjects/user', 'POST', { subjectIds });
      checkAPIResponse(response);
      response = await response.json();
      const { subjects } = response.data;
      dispatch({ type: GET_SUBJECTS_SUCCESS, subjects });
    } catch (e) {
      dispatch({ type: GET_SUBJECTS_FAIL });
    }
  };
};

export const fetchSubjectsByGroups = ({ groupIds }) => {
  return async (dispatch, getState) => {
    dispatch({ type: GET_SUBJECTS_START });
    try {
      const { account = {} } = getState(),
        { user = {} } = account;
      let response = await apiCall(`/subjects/group`, 'POST', { groupIds });
      checkAPIResponse(response);
      response = await response.json();
      const { subjects } = response.data;
      dispatch({ type: GET_SUBJECTS_SUCCESS, subjects });
    } catch (e) {
      dispatch({ type: GET_SUBJECTS_FAIL });
    }
  };
};

export const createSubject = (data) => {
  const { payload, groupId } = data;
  return async (dispatch) => {
    dispatch({ type: POST_SUBJECT_START });
    try {
      let response = await apiCall('/subject', 'POST', payload);
      checkAPIResponse(response);
      dispatch(fetchSubjectsByGroups({ groupIds: [groupId] }));
      dispatch({ type: POST_SUBJECT_SUCCESS });
    } catch (e) {
      dispatch({ type: POST_SUBJECT_FAIL });
    }
  };
};

export const updateSubject = (data) => {
  const { id, payload, groupId } = data;
  return async (dispatch) => {
    dispatch({ type: UPDATE_SUBJECT_START });
    try {
      let response = await apiCall(`/subject/${id}`, 'PUT', payload);
      checkAPIResponse(response);
      dispatch(fetchSubjectsByGroups({ groupIds: [groupId] }));
      dispatch({ type: UPDATE_SUBJECT_SUCCESS });
    } catch (e) {
      dispatch({ type: UPDATE_SUBJECT_FAIL });
    }
  };
};

export const deleteSubject = (data) => {
  const { id, groupId } = data;
  return async (dispatch) => {
    dispatch({ type: DELETE_SUBJECT_START });
    try {
      let response = await apiCall(`/subject/${id}`, 'DELETE');
      checkAPIResponse(response);
      dispatch(fetchSubjectsByGroups({ groupIds: [groupId] }));
      dispatch({ type: DELETE_SUBJECT_SUCCESS });
    } catch (e) {
      dispatch({ type: DELETE_SUBJECT_FAIL });
    }
  };
};
