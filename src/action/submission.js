import { apiCall, checkAPIResponse } from '../utils/helper';
import {
    GET_SUBMISSIONS_START,
    GET_SUBMISSIONS_SUCCESS,
    GET_SUBMISSIONS_FAIL,
    POST_SUBMISSION_START,
    POST_SUBMISSION_SUCCESS,
    POST_SUBMISSION_FAIL,
    UPDATE_SUBMISSION_START,
    UPDATE_SUBMISSION_SUCCESS,
    UPDATE_SUBMISSION_FAIL,
    DELETE_SUBMISSION_START,
    DELETE_SUBMISSION_SUCCESS,
    DELETE_SUBMISSION_FAIL,
    MAP_QUESTION_START,
    MAP_QUESTION_SUCCESS,
    MAP_QUESTION_FAIL,
} from './defined/submission';

export const fetchSubmissions = () => {
    return async (dispatch) => {
        dispatch({ type: GET_SUBMISSIONS_START });
        try {
            let response = await apiCall('/submission', 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { submissions } = response.data;
            dispatch({ type: GET_SUBMISSIONS_SUCCESS, submissions });
        } catch (e) {
            dispatch({ type: GET_SUBMISSIONS_FAIL });
        }
    };
};

export const fetchSubmissionsByTest = ({ id }) => {
    return async (dispatch) => {
        dispatch({ type: GET_SUBMISSIONS_START });
        try {
            let response = await apiCall(`/submission/group/${id}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { submissions } = response.data;
            dispatch({ type: GET_SUBMISSIONS_SUCCESS, submissions });
        } catch (e) {
            dispatch({ type: GET_SUBMISSIONS_FAIL });
        }
    };
};

export const createSubmission = ({ payload }) => {
    return async (dispatch) => {
        dispatch({ type: POST_SUBMISSION_START });
        try {
            let response = await apiCall('/submit/test', 'POST', payload);
            checkAPIResponse(response);
            // dispatch(fetchSubmissions())
            // dispatch(fetchSubmissionsByGroup({ id: groupId }));s
            dispatch({ type: POST_SUBMISSION_SUCCESS });
        } catch (e) {
            dispatch({ type: POST_SUBMISSION_FAIL });
        }
    };
};