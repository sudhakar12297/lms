import { apiCall, checkAPIResponse } from '../utils/helper';
import {
    GET_TESTS_START,
    GET_TESTS_SUCCESS,
    GET_TESTS_FAIL,
    POST_TEST_START,
    POST_TEST_SUCCESS,
    POST_TEST_FAIL,
    UPDATE_TEST_START,
    UPDATE_TEST_SUCCESS,
    UPDATE_TEST_FAIL,
    DELETE_TEST_START,
    DELETE_TEST_SUCCESS,
    DELETE_TEST_FAIL,
    MAP_QUESTION_START,
    MAP_QUESTION_SUCCESS,
    MAP_QUESTION_FAIL,
    SUBMITTED_RESULT_START,
    SUBMITTED_RESULT_SUCCESS,
    SUBMITTED_RESULT_FAIL,
    UPDATE_RESULT_START,
    UPDATE_RESULT_SUCCESS,
    UPDATE_RESULT_FAIL,
    REDIRECT_TEST,
} from './defined/test';

export const fetchTests = () => {
    return async (dispatch) => {
        dispatch({ type: GET_TESTS_START });
        try {
            let response = await apiCall('/test', 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { tests } = response.data;
            dispatch({ type: GET_TESTS_SUCCESS, tests });
        } catch (e) {
            dispatch({ type: GET_TESTS_FAIL });
        }
    };
};

export const fetchTestsByGroup = ({ id }) => {
    return async (dispatch) => {
        dispatch({ type: GET_TESTS_START });
        try {
            let response = await apiCall(`/test/group/${id}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { tests } = response.data;
            dispatch({ type: GET_TESTS_SUCCESS, tests });
        } catch (e) {
            dispatch({ type: GET_TESTS_FAIL });
        }
    };
};

export const fetchTestsByTopic = ({ id, isPractice = false }) => {
    return async (dispatch) => {
        dispatch({ type: GET_TESTS_START });
        try {
            let response = await apiCall(`/test/topic/${id}?isPractice=${isPractice}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { tests } = response.data;
            dispatch({ type: GET_TESTS_SUCCESS, tests });
        } catch (e) {
            dispatch({ type: GET_TESTS_FAIL });
        }
    };
};

export const createTest = (data) => {
    const { payload } = data;
    return async (dispatch) => {
        dispatch({ type: POST_TEST_START });
        try {
            let response = await apiCall('/test', 'POST', payload);
            checkAPIResponse(response);
            dispatch(fetchTests())
            response = await response.json();
            const { data = {} } = response;
            const { tests = [] } = data;
            const [firstNote = {}] = tests;
            // dispatch(fetchTestsByGroup({ id: groupId }));
            dispatch({ type: POST_TEST_SUCCESS });
            dispatch({ type: REDIRECT_TEST, payload: firstNote })
        } catch (e) {
            dispatch({ type: POST_TEST_FAIL });
        }
    };
};

export const updateTest = (data) => {
    const { id, payload } = data;
    return async (dispatch) => {
        dispatch({ type: UPDATE_TEST_START });
        try {
            let response = await apiCall(`/test/${id}`, 'PUT', payload);
            checkAPIResponse(response);
            dispatch(fetchTests());
            dispatch({ type: UPDATE_TEST_SUCCESS });
        } catch (e) {
            dispatch({ type: UPDATE_TEST_FAIL });
        }
    };
};

export const deleteTest = (data) => {
    const { id } = data;
    return async (dispatch) => {
        dispatch({ type: DELETE_TEST_START });
        try {
            let response = await apiCall(`/test/${id}`, 'DELETE');
            checkAPIResponse(response);
            dispatch(fetchTests());
            dispatch({ type: DELETE_TEST_SUCCESS });
        } catch (e) {
            dispatch({ type: DELETE_TEST_FAIL });
        }
    };
};

export const mapQuestions = ({ id, payload }) => {
    return async (dispatch) => {
        dispatch({ type: MAP_QUESTION_START });
        try {
            let response = await apiCall(`/test/questions/${id}`, 'PUT', payload);
            checkAPIResponse(response);
            response = await response.json();
            const { data = {} } = response;
            dispatch({ type: MAP_QUESTION_SUCCESS, payload: data });
        } catch (e) {
            dispatch({ type: MAP_QUESTION_FAIL });
        }
    };
}

export const getSubmissionByTestId = ({ testId }) => {
    return async (dispatch) => {
        dispatch({ type: SUBMITTED_RESULT_START });
        try {
            let response = await apiCall(`/submit/${testId}`, 'GET');
            checkAPIResponse(response);
            response = await response.json();
            const { data = {} } = response;
            dispatch({ type: SUBMITTED_RESULT_SUCCESS, data });
        } catch (e) {
            dispatch({ type: SUBMITTED_RESULT_FAIL });
        }
    };
}

export const updateTestResults = ({ testId, submitId, payload }) => {
    return async (dispatch) => {
        dispatch({ type: UPDATE_RESULT_START });
        try {
            let response = await apiCall(`/submit/${submitId}`, 'PUT', payload);
            checkAPIResponse(response);
            response = await response.json();
            const { data = {} } = response;
            dispatch(getSubmissionByTestId({ testId }));
            dispatch({ type: UPDATE_RESULT_SUCCESS, data });
        } catch (e) {
            dispatch({ type: UPDATE_RESULT_FAIL });
        }
    };
}