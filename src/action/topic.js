import moment from "moment";
import { apiCall, checkAPIResponse } from '../utils/helper';
import {
  GET_TOPIC_START,
  GET_TOPIC_SUCCESS,
  GET_TOPIC_FAIL,
  POST_TOPIC_START,
  POST_TOPIC_SUCCESS,
  POST_TOPIC_FAIL,
  UPDATE_TOPIC_START,
  UPDATE_TOPIC_SUCCESS,
  UPDATE_TOPIC_FAIL,
  DELETE_TOPIC_START,
  DELETE_TOPIC_SUCCESS,
  DELETE_TOPIC_FAIL,
} from './defined/topic';
import { fetchLessonsBySubject } from './lesson';

export const fetchTopicById = ({ subjectId, lessonId, topicId }) => {
  return async (dispatch) => {
    dispatch({ type: GET_TOPIC_START });
    try {
      let response = await apiCall(`/topic/${subjectId}/${lessonId}/${topicId}`, 'GET');
      checkAPIResponse(response);
      response = await response.json();
      const { topic } = response.data;
      dispatch({ type: GET_TOPIC_SUCCESS, topic: topic[0] });
    } catch (e) {
      dispatch({ type: GET_TOPIC_FAIL });
    }
  };
};

export const createTopic = (data) => {
  const { payload, subjectId, lessonId } = data;
  return async (dispatch) => {
    dispatch({ type: POST_TOPIC_START });
    try {
      let response = await apiCall(
        `/topic/${subjectId}/${lessonId}`,
        'POST',
        payload
      );
      checkAPIResponse(response);
      // dispatch(fetchLessonsBySubject({ id: subjectId }));
      dispatch({ type: POST_TOPIC_SUCCESS });
    } catch (e) {
      dispatch({ type: POST_TOPIC_FAIL });
    }
  };
};

export const updateTopic = (data) => {
  const { id, payload, lessonId, subjectId } = data;
  return async (dispatch) => {
    dispatch({ type: UPDATE_TOPIC_START });
    try {
      let response = await apiCall(
        `/topic/${subjectId}/${lessonId}/${id}`,
        'PUT',
        payload
      );
      checkAPIResponse(response);
      // dispatch(fetchLessonsBySubject({ id: subjectId }));
      dispatch({ type: UPDATE_TOPIC_SUCCESS });
    } catch (e) {
      dispatch({ type: UPDATE_TOPIC_FAIL });
    }
  };
};

export const deleteTopic = (data) => {
  const { id, subjectId, lessonId } = data;
  return async (dispatch) => {
    dispatch({ type: DELETE_TOPIC_START });
    try {
      let response = await apiCall(
        `/topic/${subjectId}/${lessonId}/${id}`,
        'DELETE'
      );
      checkAPIResponse(response);
      dispatch(fetchLessonsBySubject({ id: subjectId }));
      dispatch({ type: DELETE_TOPIC_SUCCESS });
    } catch (e) {
      dispatch({ type: DELETE_TOPIC_FAIL });
    }
  };
};


export const saveTrackTopicData = () => {
  return async (dispatch, getState) => {
    try {
      const { userOnboardTimeInStudyTopic, homeworks, subject, lesson, topic } = getState().common
      if (homeworks.length) {
        const payload = {
          subjectId: subject.id,
          chapterId: lesson.id,
          topicId: topic.id,
          homeworkIds: homeworks.map(homework => homework._id),
          timeSpent: moment().diff(userOnboardTimeInStudyTopic, 'minutes')
        }
        console.log(payload);
        let response = await apiCall(
          `/homework/submit`,
          'POST', payload
        );
        checkAPIResponse(response);
        dispatch({ type: 'UNSET_ONGOING_HOMEWORKS_CONTAINS_CURRENT_TOPIC' })
        dispatch({ type: 'UNSET_USER_ONBOARD_TIME' })
      }
    } catch (e) {
      dispatch({ type: "TRACK_TOPIC_FAIL" });
    }
  };
};