import React from 'react';
import '../../../assets/css/form-inputs.css';
import Loader from '../../../components/asset/loader';

export const Button = (props) => {
  const { name, title, onClick, className = "primary-button-", variant, disabled = false, loader = true, icon } = props;
  return (
    <div>
      <button className={className} title={title} onClick={onClick} variant={variant} disabled={disabled} style={{ opacity: disabled ? 0.3 : null }}>
        {icon}
        {disabled && loader ? '' : name}
        {loader && disabled && <Loader />}
      </button>
    </div>
  );
};
