import React from 'react';
import { Card } from '../cards'
import { Box, Flex, FlexWrap, Spacer } from '../wrapper'
import '../../../assets/css/announcement.css'
import { IconDelete, IconEdit } from '../../../components/asset/icons';

export const AnnounceHeaderCard = (props) => {
    const { content } = props;
  return (
      <Box className="header-card" bg="#F65164">
            {content}
      </Box>
  );
};

export const AnnounceContentCard = (props) => {
    const { content, date, time, edit, remove } = props;
  return (
    <Card className="content-card">
        <Box>
            {content}
        </Box>
        <Spacer m={3} />
        <Box>
            <FlexWrap>
                <span className="announce-date">{date}</span>
                <Spacer mr={2}/>
                <span className="announce-time">{time}</span>
            </FlexWrap>
        </Box>
        {edit !== undefined && remove !== undefined && (
        <Flex className="user-action-icon">
          <Box onClick={edit}>
            <IconEdit />
          </Box>
          <Spacer mr={2} />
          <Box onClick={remove}>
            <IconDelete />
          </Box>
        </Flex>
      )}
    </Card>
  );
};
