/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React from 'react';
import { Box } from '../wrapper';
import '../../../assets/css/card.css';

export default (props) => {
  const { children, bg = 'white', width = 'auto', className, onClick, ariaControls, ariaExpanded, p = "25px" } = props;
  return (
    <Box bg={bg} p={p} mr={4} mb={4} width={width} className={`main-card ${className}`} onClick={onClick} ariaControls={ariaControls} ariaExpanded={ariaExpanded} >
      {children}
    </Box>
  );
};
