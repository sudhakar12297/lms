import React from 'react';
import { Box, Flex } from '../wrapper';
import { Title } from '../cards';

const Row = (props) => {
  const { label, value } = props;
  return (
    <Flex mb={3} justifyContent="space-between">
      <Box className="card-key">{label}</Box>
      <Box>:</Box>
      <Box>{value}</Box>
    </Flex>
  );
};

export default (props) => {
  const { title, data } = props;
  return (
    <Box className="group-card-expand">
      <Title name={title} className="title-s4 white" />
      {data?.map((row, index) => {
        return <Row key={index} label={row.label} value={row.data} />;
      })}
    </Box>
  );
};
