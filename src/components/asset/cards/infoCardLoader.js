import React from 'react';
import { Box, Flex, FlexWrap } from '../wrapper';
import { Title } from '../cards';
import ContentLoader from 'react-content-loader';
import '../../../assets/css/login.css';

export default (props) => {
    const { title, } = props;
    return (
        <Box className="group-card-expand">
            <Title name={title} />
            <Flex mb={3} justifyContent="space-between">
                {[1].map(index => {
                    return (
                        <Flex p={2} className="" key={index}>
                            <ContentLoader
                                speed={2}
                                width={'100%'}
                                height={'100%'}
                                viewBox="0 0 100% 100%"
                                backgroundColor="#f3f3f3"
                                foregroundColor="#ecebeb"
                                {...props}
                            >
                                <rect x="0" y="8" rx="3" ry="3" width="100%" height="10%" />
                                <rect x="0" y="28" rx="3" ry="3" width="100%" height="10%" />
                                <rect x="0" y="48" rx="3" ry="3" width="100%" height="10%" />
                                <rect x="0" y="68" rx="3" ry="3" width="100%" height="10%" />
                            </ContentLoader>
                        </Flex>
                    )
                })}
            </Flex>
        </Box>
    );
};
