/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React from 'react';
import { Box, Flex } from '../wrapper';
import { color } from 'styled-system';

export const ScheduleCard = (props) => {
    const { title, detail, explanation, color = "bg-green", textColor = "navy-blue" } = props;
    return (
        <Box className={"schedule-card " + color} ml={3}>
            <Flex>
                <Box m={1} className="title-s4" >{title}</Box>
                <Box m={1} className={"title-s5 " + textColor}>{detail}</Box>
            </Flex>
            <Flex m={1} className="title-s5">
                {explanation}
            </Flex>
        </Box>
    );
};