import React from 'react';
import { Flex, Box } from '../../asset/wrapper';

export default (props) => {
  const { className } = props;
  return (
    <Flex mb="6px" justifyContent="space-between">
      <Box className={"card-title " + className}>{props.name}</Box>
    </Flex>
  );
};
