import React from 'react';
import { Box, Flex, FlexWrap } from '../wrapper';
import { Title } from '../cards';
import { Span } from '../elements'
import { ProfilePic } from '../../asset/icons/profilePic'

import '../../../assets/css/login.css';

const User = (props) => {
  const { name, subject } = props;
  return (
    <Flex p={2} className="staff-details">
      <Box m={1}>
        <ProfilePic />
      </Box>
      <Box>
        <Box m={1} mt={2} className="title-s6">
          {name}
        </Box>
        {subject && <Box m={1} mt={2}><Span text={subject} className="highliter" /></Box> }
      </Box>
    </Flex>
  );
};

export default (props) => {
  const { title, users } = props;
  return (
    <Box className="staff-group-list">
      <Box p={3} pb={2} className="title-s4">
        {title}
      </Box>
      <FlexWrap className="staff-row">
        {users?.length ? (
          users?.map((user, index) => {
            return<FlexWrap className="staff" title={user.firstName}> <User key={index} name={user.firstName} subject={user.lastName} /></FlexWrap>;
          })
        ) : (
          <Box className="p-grey">No teacher's available</Box>
        )}
      </FlexWrap>
    </Box>
  );
};
