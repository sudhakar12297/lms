import React from 'react';
import { Box, Flex, FlexWrap } from '../wrapper';
import { Title } from '../cards';
import ContentLoader from 'react-content-loader';
import '../../../assets/css/login.css';

export default (props) => {
    const { title, } = props;
    return (
        <Box className="staff-group-list">
            <Title name={title} />
            <FlexWrap className="staff-row">
                {[1, 2,].map(index => {
                    return (
                        <Flex p={2} className="staff-details" key={index}>
                            <ContentLoader
                                speed={2}
                                width={'100%'}
                                height={'100%'}
                                viewBox="0 0 100% 100%"
                                backgroundColor="#f3f3f3"
                                foregroundColor="#ecebeb"
                                {...props}
                            >
                                <rect x="15" y="0" rx="10" ry="10" width="25%" height="35%" />
                                <rect x="78" y="8" rx="3" ry="3" width="50%" height="10%" />
                                <rect x="78" y="28" rx="3" ry="3" width="50%" height="7%" />
                                <rect x="15" y="70" rx="10" ry="10" width="25%" height="35%" />
                                <rect x="78" y="78" rx="3" ry="3" width="50%" height="10%" />
                                <rect x="78" y="98" rx="3" ry="3" width="50%" height="7%" />
                            </ContentLoader>
                        </Flex>
                    )
                })}
            </FlexWrap>
        </Box>
    );
};
