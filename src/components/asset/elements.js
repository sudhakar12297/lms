import React from 'react';
import '../../assets/css/form-inputs.css';
import '../../assets/css/span.css'

export const Head = (props) => {
  return (
    <React.Fragment>
      <h1 className={props.className}>{props.name}</h1>
    </React.Fragment>
  );
};

export const Label = (props) => {
  const { className = 'form-label', required = false } = props;
  return (
    <div>
      <label className={className}>{props.name} {required && "  *"}</label>
    </div>
  );
};

export const FormTitle = (props) => {
  const { className = 'form-title-user' } = props;
  return (
    <h4 className={className}>{props.name}</h4>
  );
};


export const P = (props) => {
  return (
    <div>
      <p className={props.className}>{props.name}</p>
    </div>
  );
};

export const Span = (props) => {
  const { className, text } = props;
  return (
    <span className={className}>{text}</span>
  );
};