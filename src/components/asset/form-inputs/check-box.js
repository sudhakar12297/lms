import React from 'react';

export default (props) => {
  const { type, name, displayLabel = '', value, className, onChange, checked, errors = '' } = props;
  return (
    <div>
      <label className={"radio_btn_container " + className}>
        {displayLabel}
        <input type={type} name={name} onChange={onChange} checked={checked} />
        <span className="radio_btn_checkmark"></span>
      </label>
      {errors ? <p style={{ color: 'red' }}>{errors}</p> : ''}
    </div>
  );
};
