/* eslint-disable react/prop-types */
import React from 'react';
import { Box } from '../wrapper';

import '../../../assets/css/form-inputs.css';

export default (props) => {
  const { type, name, value, onChange, placeholder, className = "text-input-", errors = '' } = props;
  return (
    <Box>
      <Box pb={3} pt={1}>
        <input
          className={className}
          type={type}
          name={name}
          value={value}
          onChange={onChange}
          placeholder={placeholder}
        />
      </Box>
      {errors ? <p style={{ color: 'red' }}>{errors}</p> : ''}
    </Box>
  );
};
