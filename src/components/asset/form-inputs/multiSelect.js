/* eslint-disable react/prop-types */
import React from 'react';
import { Box } from '../wrapper';
import { Multiselect } from 'multiselect-react-dropdown';

export default React.forwardRef((props, ref) => {
    const { options, selectedValues,
        displayValue, placeholder,
        onSelect, onRemove,
        singleSelect, emptyRecordMsg, closeOnSelect,
        avoidHighlightFirstOption,
        style, errors = '' } = props;
    return (
        <Box>
            <Multiselect
                options={options}
                selectedValues={selectedValues}
                onSelect={onSelect}
                onRemove={onRemove}
                displayValue={displayValue}
                placeholder={placeholder}
                singleSelect={singleSelect}
                emptyRecordMsg={emptyRecordMsg}
                closeOnSelect={closeOnSelect}
                avoidHighlightFirstOption={avoidHighlightFirstOption}
                ref={ref}
                style={style}
            />
            {errors ? <p style={{ color: 'red' }}>{errors}</p> : ''}
        </Box>
    );
});
