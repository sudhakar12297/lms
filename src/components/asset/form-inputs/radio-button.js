import React from 'react';

export default (props) => {
  const { type, name, displayLabel = '', value, group, onChange, checked, className, disabled = false, errors = '' } = props;
  return (
    <div>
      <label className={"radio_btn_container " + className}>
        {displayLabel}
        <input
          type={type}
          group={group}
          name={name}
          value={value}
          onChange={onChange}
          checked={checked}
          className={className}
          disabled={disabled}
        />
        <span className="radio_btn_checkmark_radio"></span>
      </label>
      {errors ? <p style={{ color: 'red' }}>{errors}</p> : ''}
    </div>
  );
};
