/* eslint-disable react/prop-types */
import React from 'react';
import { Box } from '../wrapper';

import '../../../assets/css/form-inputs.css';

export default (props) => {
  const {
    id,
    name,
    value,
    onChange,
    placeholder,
    rows,
    className,
    disabled = false,
    errors = '',
  } = props;
  return (
    <Box>
      <textarea
        id={id}
        name={name}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        className={className}
        rows={rows}
        disabled={disabled}
      ></textarea>
      {errors ? <p style={{ color: 'red' }}>{errors}</p> : ''}
    </Box>
  );
};
