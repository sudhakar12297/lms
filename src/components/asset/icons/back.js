/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React from 'react';

import { BackIcon } from './backIcon';
import { Box, Flex } from '../wrapper';
// import { isMobile } from 'react-device-detect';

export const Back = (props) => {
    let { width = "24px", height = "24px", fill = "none", stroke = "none", text = "Back", textSize = "16px" } = props;

    // if (isMobile) {
    //     width = width / 2;
    //     height = height / 2;
    //     textSize = textSize / 2;
    // }

    let textStyle = {
        fontSize: textSize,
        fontFamily: "Product Sans !important",
        fontStyle: "normal",
        fontWeight: "bold",
        // color: "#2D2B2B",
        color: "#138BFC",
    }

    // let centerStyle = {
    //     marginTop: "50px",
    //     textAlign: "center",
    // }
    return (
        <Flex>
            <Box>
                <BackIcon />
            </Box>
            <Box style={textStyle} m={1}>
                {text}
            </Box>
        </Flex>
    );
};