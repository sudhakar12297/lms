/* eslint-disable react/prop-types */
import React from 'react';
export const IconLiveClass = (props) => {
    const { stoke, fill, width = "18", height = "18" } = props;
    return (
        <svg
            width={width}
            height={height}
            viewBox="0 0 18 18"
            fill={fill}
            stroke={stoke}
            xmlns="http://www.w3.org/2000/svg"
        >
            <g clip-path="url(#clip0)">
                <path d="M10.1816 3.7124H1.96937C0.886214 3.7124 0 4.59862 0 5.68177V12.3185C0 13.4017 0.886214 14.2879 1.96937 14.2879H10.1816C11.2648 14.2879 12.151 13.4017 12.151 12.3185V5.68177C12.151 4.57892 11.2648 3.7124 10.1816 3.7124Z" fill={fill} />
                <path d="M16.5427 4.79539C16.4246 4.81509 16.3064 4.87417 16.208 4.93325L13.1357 6.70568V11.2746L16.2276 13.047C16.7988 13.3818 17.5077 13.1849 17.8425 12.6138C17.941 12.4365 18.0001 12.2396 18.0001 12.023V5.93762C18.0001 5.20896 17.3108 4.61815 16.5427 4.79539Z" fill={fill} />
            </g>
            <defs>
                <clipPath id="clip0">
                    <rect width="18" height="18" fill="white" />
                </clipPath>
            </defs>
        </svg>
    );
};
