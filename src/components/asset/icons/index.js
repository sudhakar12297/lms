import IconEdit from './edit';
import IconDelete from './delete';
import IconDashboard from './dashboard';
import { IconGroup } from './group';
import { IconTeacher } from './teacher';
import { IconStudent } from './student';
import { IconAdmin } from './admin';
import { IconSubject } from './subject'
import { IconLiveClass } from './class';
import { IconNotes } from './notes';
import { IconDoubt } from './doubt'
import { IconTest } from './test'
import { IconHomework } from './homework'
import { IconAnnouncement } from './announcement';

export {
  IconEdit,
  IconDelete,
  IconDashboard,
  IconGroup,
  IconTeacher,
  IconStudent,
  IconAdmin,
  IconSubject,
  IconLiveClass,
  IconNotes,
  IconDoubt,
  IconTest,
  IconHomework,
  IconAnnouncement,
};
