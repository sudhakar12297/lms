/* eslint-disable react/prop-types */
import React from 'react';
export const Play = (props) => {
    const { width = "24", height = "24" } = props;
    return (
        <svg width={width} height={height} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="12" cy="12" r="11.5" stroke="#3D2F2F" />
            <rect x="7" y="6" width="3" height="12" rx="1" fill="#3D2F2F" />
            <rect x="14" y="6" width="3" height="12" rx="1" fill="#3D2F2F" />
        </svg>
    );
};