/* eslint-disable react/prop-types */
import React from 'react';
export const ProfilePic = (props) => {
    const { fill = "none", width = "38", height = "38" } = props;
    return (
        <svg width={width} height={height} viewBox="0 0 38 38" fill={fill} xmlns="http://www.w3.org/2000/svg">
            <circle cx="19" cy="19" r="19" fill="#C4C4C4" />
            <circle cx="19" cy="19" r="19" fill="url(#pattern0)" />
        </svg>
    );
};

