import React from 'react';
import Loader from 'react-loader-spinner';

const Loading = (props) => {
  const { type = 'ThreeDots', color = '#00BFFF', height = 35, width = 50, visible = true } = props;
  return (
    <Loader type={type} color={color} height={height} width={width} visible={visible} />
  );
};

export default Loading;
