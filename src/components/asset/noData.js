/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React from 'react';
import { Box } from './wrapper';
import { IconNoDataAvailable } from './icons/noDataIcon';
import { isMobile } from 'react-device-detect';

export const NoDataAvailable = (props) => {
    let { width = 200, height = 200, fill = "none", stroke = "none", text = "No Data Found", textSize = "25px" } = props;

    if (isMobile) {
        width = width / 2;
        height = height / 2;
        textSize = textSize / 2;
    }

    let textStyle = {
        fontSize: textSize,
        fontFamily: "Product Sans !important",
        fontStyle: "normal",
        fontWeight: "bold",
        textAlign: "center",
        color: "#2D2B2B",
    }

    let centerStyle = {
        marginTop: "50px",
        textAlign: "center",
    }
    return (
        <Box style={centerStyle}>
            <Box>
                <IconNoDataAvailable width={width} height={height} fill={fill} />
            </Box>
            <Box style={textStyle} mt={2} ml={4}>
                {text}
            </Box>
        </Box>
    );
};