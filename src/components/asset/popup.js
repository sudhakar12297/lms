import React from 'react';
import Modal from 'react-bootstrap/Modal';
import { Button } from '../asset/button';

function Popup(props) {
  const {
    show,
    size,
    confirm,
    cancel,
    title = 'Delete',
    body = 'Are you sure want to delete?',
    buttonName = 'Delete',
    children = null
  } = props;

  return (
    <Modal show={show} animation={true} style={{ opacity: 1 }} onHide={cancel} size={size} centered >
      <Modal.Header closeButton>
        <Modal.Title><h2>{title}</h2></Modal.Title>
      </Modal.Header>
      {!children && <Modal.Body > <h4>{body}</h4></Modal.Body>}
      <h4>{children}</h4>
      <Modal.Footer>
        <Button variant="secondary" name="Cancel" className={'primary-button-sm'} onClick={cancel} />
        <Button variant="primary" name={buttonName} className={'primary-button-sm button-red'} onClick={confirm} />
      </Modal.Footer>
    </Modal >
  );
}

export default Popup;
