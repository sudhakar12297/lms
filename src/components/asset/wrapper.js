import styled from '@emotion/styled';
import shouldForwardProp from '@styled-system/should-forward-prop';
import {
  background,
  border,
  color,
  flexbox,
  layout,
  position,
  shadow,
  space,
  system,
  typography,
} from 'styled-system';

const customProps = ['showFor'];

const forwardProp = (propName) =>
  shouldForwardProp(propName) && !customProps.includes(propName);

const fontFamily = `'Roboto', sans-serif`;
const fontSize = '14px';
const lineHeight = 1.15;

const Base = styled('div', { shouldForwardProp: forwardProp })(
  {
    fontFamily,
    fontSize,
    lineHeight,
  },
  space,
  layout,
  flexbox,
  color,
  typography,
  background,
  border,
  position,
  shadow
);

function showForProp(defaultValue) {
  system({
    showFor: {
      property: 'display',
      transform: (val) => (val ? defaultValue : 'none'),
    },
  });
}

export const Box = styled(Base)(showForProp('block'));
export const Flex = styled(Base)({ display: 'flex' }, showForProp('flex'));
export const FlexWrap = styled(Base)(
  { display: 'flex', flexWrap: 'wrap' },
  showForProp('flex')
);
export const Spacer = styled('div', { shouldForwardProp: forwardProp })(space);
