import React from 'react';
import { Box } from '../components/asset/wrapper';
import { AnnounceContentCard, AnnounceHeaderCard } from './asset/cards/announcementCards'

const Design = () => {
  return (
    <Box>
      Hi, I'm desin container
      <AnnounceHeaderCard content="10th Standard" />
      <AnnounceContentCard 
        content="When the war broke out in 1914, 
        Italy was, and since 1882 had been, 
        an ally of Germany and Austria-Hungary. 
        The alliance was something of a paradox."
        date="Jan,24,2020"
        time="12:30Pm"
      />
    </Box>
  );
};

export default Design;
