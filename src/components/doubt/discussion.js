import React from 'react';
import { useSelector } from 'react-redux';
import moment from 'moment';
import { Flex, Box, FlexWrap } from '../asset/wrapper';
import '../../assets/css/discussion.css'
import { Send } from '../../components/asset/icons/send'
import Accordion from 'react-bootstrap/Accordion'
import { Card } from 'react-bootstrap';
import { IconDelete, IconEdit } from '../asset/icons';
import { ProfilePic } from '../asset/icons/profilePic'
import { getUser } from '../../reducer/account';
import { NoDataAvailable } from '../asset/noData';

const Discussion = (props) => {
    const {
        doubt,
        showEdit,
        showRemove,
        edit,
        remove,
        onClick,
        value,
        onChange,
        onSubmit,
        hideAskAndReplyDoubt = false
    } = props;

    const user = useSelector(getUser)

    return (
        <Accordion defaultActiveKey="">
            <Card className="question-card">
                <Accordion.Toggle as={Card.Header} eventKey="0" className="accordion-header">
                    <Box className="discussion-box">
                        <Flex>
                            <FlexWrap m={1}><ProfilePic /> </FlexWrap>
                            <FlexWrap m={2}>
                                <FlexWrap ml={2} className="name" >
                                    {doubt?.userName || 'Anonymous'}
                                </FlexWrap>
                                <FlexWrap ml={2} mt={1} className="time">
                                    {moment(doubt?.updatedAt).fromNow()}
                                </FlexWrap>
                                <FlexWrap ml={2} mt={1} className="reply-button">
                                    Replies ({doubt?.replies?.length})
                                </FlexWrap>
                                {(showEdit || showRemove) &&
                                    <FlexWrap>
                                        {showEdit && <Box onClick={() => edit(true, doubt?.message)}><IconEdit /></Box>}
                                        {showRemove && <Box onClick={() => remove(true)}><IconDelete /></Box>}
                                    </FlexWrap>}
                            </FlexWrap>
                        </Flex>
                        <Flex m={1} ml={5} className="reply">
                            {doubt?.message}
                        </Flex>
                    </Box>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                    <Card.Body>
                        {
                            doubt?.replies?.length ?
                                (
                                    doubt?.replies?.map((reply, index) => {
                                        return (
                                            <Flex ml={4} key={index}>
                                                <FlexWrap m={1} className="color-line"></FlexWrap>
                                                <FlexWrap m={1}><ProfilePic /> </FlexWrap>
                                                <Box>
                                                    <Box>
                                                        <FlexWrap m={1}>
                                                            <FlexWrap m={1} className="name" >
                                                                {reply?.userName}
                                                            </FlexWrap>
                                                            <FlexWrap m={1} mt={2} className="time">
                                                                {moment(reply?.updatedAt).fromNow()}
                                                            </FlexWrap>
                                                        </FlexWrap>
                                                        {(showEdit || showRemove) &&
                                                            <FlexWrap ml={5} className="float-right" >
                                                                {showEdit && <Box onClick={() => edit(false, reply?.message, reply.id)}><IconEdit /></Box>}
                                                                {showRemove && <Box onClick={() => remove(false, reply.id)}><IconDelete /></Box>}
                                                            </FlexWrap>}
                                                    </Box>
                                                    <FlexWrap m={1} ml={2}>
                                                        {reply?.message}
                                                    </FlexWrap>
                                                </Box>
                                            </Flex>
                                        )
                                    })
                                )
                                : <NoDataAvailable height={75} width={75} textSize='20px' text='No replies received yet' />
                        }
                        {!hideAskAndReplyDoubt && <Flex m={2} ml={4}>
                            <input name="reply" value={value?.reply} onChange={onChange} placeholder="Reply" className="reply-input-box" />
                            <Box mt={"10px"} onClick={() => onSubmit()} className="reply-send-button"><Send /></Box>
                        </Flex>}
                    </Card.Body>
                </Accordion.Collapse>
            </Card>
        </Accordion>
    );
};

export default Discussion;