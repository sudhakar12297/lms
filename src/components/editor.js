import React from 'react';
import ReactQuill from 'react-quill';
import { Box } from '../components/asset/wrapper';

const Editor = (props) => {
  const { value = '', onChange, onBlur, limited, errors = '' } = props;

  const container = [
    [
      { header: '1' },
      { header: '2' },
      { header: [3, 4, 5, 6] },
      { font: [] },
    ],
    [{ size: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    ['link', 'video'],
    ['link', 'image', 'video'],
    ['clean'],
    ['code-block'],
  ]

  const limitedContainer = [
    [{ header: [1, 2, 3, 4, 5, 6] }, 'bold', 'italic', 'underline', 'blockquote', { list: 'ordered' }, { list: 'bullet' }],
  ]

  return (
    <Box width="90%" height="50vh" style={{ overflow: 'auto' }}>
      <ReactQuill
        placeholder="Write here....."
        value={value}
        onChange={(data) => onChange(data)}
        modules={{
          toolbar: {
            container: limited ? limitedContainer : container
            //   handlers: {
            //     image: this.imageHandler,
            //   },
          },
        }}
        onBlur={onBlur}
      />
      {errors ? <p style={{ color: 'red' }}>{errors}</p> : ''}
    </Box>
  );
};

export default Editor;
