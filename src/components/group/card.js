import React, { Fragment } from 'react';
import '../../assets/css/group.css';
import { Box, Flex, Spacer } from '../asset/wrapper';
import { Card } from '../asset/cards';
import { IconDelete, IconEdit } from '../../components/asset/icons';
import { Span } from '../asset/elements';

export const GroupCard = (props) => {
  const {
    standard,
    teachers,
    students,
    boys,
    girls,
    edit,
    remove,
    onClick,
    index,
    showDelete,
    showEdit,
  } = props;

  return (
    <Card width="370px" className="group-card-wrapper">
      <Fragment>
        {(showDelete || showEdit) && (
          <Flex className="user-action-icon">
            {showEdit && (
              <Box onClick={edit}>
                <IconEdit />
              </Box>
            )}
            <Spacer mr={2} />
            {showDelete && (
              <Box onClick={remove}>
                <IconDelete />
              </Box>
            )}
          </Flex>
        )}
        <Box onClick={onClick}>
          <Flex pb={2}>
            <Box
              pt={3}
              className={
                index % 2 === 0
                  ? `group-card-standard-`
                  : `group-card-standard-red`
              }
            >
              {/* <img src={subjectIcon} /> */}
              <Flex p={2} className="group-card-p">
                {standard[0]}
              </Flex>
            </Box>
            <Box ml={3} pr={1}>
              <Box mb={1} className="group-card-title">
                {standard}
              </Box>
              <Box className="group-card-content">
                <Box pt={2}>
                  No Of Teachers : <Span className="bold" text={teachers} />
                </Box>
                <Box pt={2}>
                  No Of Students : <Span className="bold" text={students} />
                </Box>
                <Box pt={2}>
                  Boys : <Span className="bold" text={boys} /> &nbsp;&nbsp;
                  Girls : <Span className="bold" text={girls} />
                </Box>
              </Box>
            </Box>
          </Flex>
        </Box>
      </Fragment>
    </Card>
  );
};
