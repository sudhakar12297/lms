import React from 'react';
import { GroupCard } from './card';
import { FlexWrap } from '../asset/wrapper';
import { NoDataAvailable } from '../asset/noData';

const Group = (props) => {
  const {
    groups,
    edit,
    remove,
    onClick,
    showEdit = false,
    showDelete = false,
  } = props;
  return (
    <FlexWrap className="group-card-main-card-wrapper">
      {
        groups?.length ?
          groups?.map((group, index) => {
            return (
              // eslint-disable-next-line react/jsx-key
              <GroupCard
                key={index}
                index={index}
                standard={group.name}
                teachers={group?.details?.teachersCount || '0'}
                students={group?.details?.studentsCount || '0'}
                boys={group?.details?.maleCount || '0'}
                girls={group?.details?.femaleCount || '0'}
                edit={() => {
                  edit(group);
                }}
                remove={() => remove(group._id)}
                onClick={(e) => onClick(group)}
                showEdit={showEdit}
                showDelete={showDelete}
              />
            );
          })
          : <NoDataAvailable text='No Group(s) created yet' />
      }
    </FlexWrap>
  );
};

export default Group;
