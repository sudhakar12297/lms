import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import DatePicker from 'react-datepicker';

import { isValidFormFieldData } from '../../utils/helper';
import { getIsFormSubmitted } from '../../reducer/group';
import { Box, Spacer } from '../asset/wrapper';
import { Input } from '../asset/form-inputs';
import { Button } from '../asset/button';
import { FormTitle, Label } from '../asset/elements';
import validateEngine from '../../utils/validator';

import 'react-datepicker/dist/react-datepicker.css';
import '../../assets/css/form-inputs.css';
import '../../assets/css/calendar.css';

const GroupForm = (props) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const { state, pathname } = location;
  const history = useHistory();
  const group = state ? state.group : null;
  const [validateOnChange, setValidateOnChange] = useState(false);
  const [errors, setErrors] = useState({});


  const isFormSubmitted = useSelector(getIsFormSubmitted);

  const [formData, setFormData] = useState({
    startYear: new Date(),
    endYear: new Date(),
  });
  const { isFormSubmitting = false } = props;
  useEffect(() => {
    group &&
      setFormData({
        ...formData,
        name: group?.name,
        startYear: new Date(group?.startYear),
        endYear: new Date(group?.endYear),
      });
    group && validateOnChange && doValidate({ ...formData, name: group?.name, startYear: new Date(group?.startYear), endYear: new Date(group?.endYear), });

    //reseting formData to initial state and remove state value from location
    if (isFormSubmitted) {
      setFormData({
        name: '',
        startYear: new Date(),
        endYear: new Date(),
      });
      history.replace({ pathname, state: {} });
      setErrors({});
      setValidateOnChange(false);
      dispatch({ type: 'UNSET_GROUP_IS_FORM_SUBMITTED' });
    }
  }, [dispatch, history, group, isFormSubmitted]);

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
    validateOnChange && doValidate({ ...formData, [e.target.name]: e.target.value });
  };

  const onCancel = () => {
    setFormData({
      name: '',
      startYear: new Date(),
      endYear: new Date(),
    });
    history.replace({ pathname, state: {} });
    setErrors({});
  }

  const doValidate = payload => {
    const { result = false, errors = {} } = validateEngine(payload, 'groupCreateJson');
    setErrors(errors);
    return result;
  }

  const onsubmit = (e) => {
    e.preventDefault();
    setValidateOnChange(true);
    if (doValidate(formData)) {
      let payload = {
        name: formData?.name,
        startYear: `${formData?.startYear.getFullYear()}`,
        endYear: `${formData?.endYear.getFullYear()}`,
        isActive: true,
      };
      props.onSubmit(
        group
          ? { id: group._id, payload, isCreate: false }
          : { payload, isCreate: true }
      );
    }
  };

  return (
    <Box>
      <FormTitle name={group ? 'Group updation' : 'Group creation'} />
      <Spacer mb={4} />
      <form>
        <Label name="Group Name" required={true} />
        <Input
          name="name"
          value={formData?.name}
          type="text"
          placeholder="Group Title"
          className="text-input-form"
          onChange={(e) => onChange(e)}
          errors={errors?.name || ''}
        />
        <Spacer mb={2} />
        <Label name="Start Year" />
        <DatePicker
          wrapperClassName="datepicker-wrapper"
          selected={formData?.startYear}
          onChange={(date) => setFormData({ ...formData, startYear: date })}
          showYearPicker
          dateFormat="yyyy"
          className="date-picker-style"
        />
        <Spacer mb={2} />
        <Label name="End Year" />
        <DatePicker
          wrapperClassName="datepicker-wrapper"
          selected={formData?.endYear}
          startDate={formData?.startYear}
          onChange={(date) => setFormData({ ...formData, endYear: date })}
          showYearPicker
          dateFormat="yyyy"
          className="date-picker-style"
        />
        <Spacer mb={3} />
        <Button
          className="primary-button-lg"
          name={group ? 'Update' : 'Create'}
          onClick={(e) => onsubmit(e)}
          disabled={isFormSubmitting}
          loader={true}
        />
        <Spacer mb={3} />
        {group && <Button
          className="primary-button-lg button-red"
          name={'Reset'}
          onClick={(e) => onCancel(e)}
        />}
      </form>
    </Box>
  );
};

export default GroupForm;
