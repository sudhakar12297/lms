import React from 'react';
import '../../assets/css/group.css';
import { FlexWrap } from '../asset/wrapper';
import { Card } from '../asset/cards';
import ContentLoader from 'react-content-loader';

const GroupLoader = (props) => {
  return (
    <FlexWrap>
      {[1, 2, 3, 4].map((index) => (
        <Card width="370px" className="group-card-wrapper" key={index}>
          <ContentLoader
            speed={2}
            width={'100%'}
            height={'100%'}
            viewBox="0 0 100% 100%"
            backgroundColor="#f3f3f3"
            foregroundColor="#ecebeb"
            {...props}
          >
            <rect x="0" y="0" rx="10" ry="10" width="94" height="80%" />
            <rect x="108" y="8" rx="3" ry="3" width="50%" height="10%" />
            <rect x="108" y="38" rx="3" ry="3" width="50%" height="7%" />
            <rect x="108" y="58" rx="3" ry="3" width="50%" height="7%" />
            <rect x="108" y="78" rx="3" ry="3" width="20%" height="7%" />
            <rect x="59%" y="78" rx="3" ry="3" width="25%" height="7%" />
          </ContentLoader>
        </Card>
      ))}
    </FlexWrap>
  );
};

export default GroupLoader;
