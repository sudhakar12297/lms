import React from 'react';
import moment from 'moment';

import { ProfilePic } from '../asset/icons/profilePic';
import { Box, Flex, FlexWrap } from '../asset/wrapper';
import { Span } from '../asset/elements';
import { Card } from '../../components/asset/cards';
import { TextArea } from '../asset/form-inputs';
import { IconEdit, IconDelete } from '../asset/icons';
import { topic } from '../../reducer/topic';

const HomeworkCard = (props) => {
    const { color, homework, showEdit = false, showRemove = false, edit, remove, onClickTopic } = props
    return (
        <Flex className="homework-cards">
            <Card width="100%">
                <FlexWrap m={1} mt={2}>
                    <Box m={1} mt={2}><Span text={homework?.subjectName} className={`span-lg-${color ? 'green' : 'red'}`} /></Box>
                    <Box m={1} mt={"2px"}><ProfilePic width="24" height="24" /></Box>
                    <Box m={1} p={1} className="title-s6-grey">{homework?.createdUser.split(' ')[0] || homework?.createdUser}</Box>
                    <Flex>
                        <Box ml={0} m={2} className="title-s5">Homework:</Box>
                        <Flex m={1}><Span text={homework?.name} className="span-sm-blue" /></Flex>
                    </Flex>
                    <Flex>
                        <Box ml={0} m={2} className="title-s5">Asigned date:</Box>
                        <Flex m={1}><Span text={moment(homework.assignedDate).format("dddd, MMM Do YYYY")} className="span-sm-violet" /></Flex>
                    </Flex>
                    <Flex>
                        <Box ml={0} m={2} className="title-s5">Due date:</Box>
                        <Flex m={1}><Span text={moment(homework.dueDate).format('dddd, MMM Do YYYY')} className="span-sm-green" /></Flex>
                    </Flex>
                    {
                        (showEdit || showRemove) &&
                        <Flex className="float-right">
                            {showEdit && <Box onClick={(e) => edit(e, homework)}><IconEdit /></Box>}
                            {showRemove && <Box onClick={(e) => remove(e, homework._id)}><IconDelete /></Box>}
                        </Flex>
                    }
                </FlexWrap>
                <Flex>
                    <Box m={2} className="title-s5">Topic's yet to complete:</Box>
                    <FlexWrap>
                        {
                            homework?.topic.map(_topic => {
                                return <Flex m={2} onClick={() => onClickTopic(_topic)}><Span text={_topic.name} className="span-sm-green" /></Flex>
                            })
                        }
                    </FlexWrap>
                </Flex>
                <Box width="98%" m={1}>
                    <TextArea rows={2} value={homework?.description || 'No description available'} disabled={true} className="text-input-textarea-form" />
                </Box>
            </Card>
        </Flex>
    );
};

export default HomeworkCard;