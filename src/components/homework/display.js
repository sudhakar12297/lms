import React from 'react';
import { Box } from '../asset/wrapper';
import { Card } from './';
import { NoDataAvailable } from '../asset/noData';

const Homework = (props) => {
    const { tab, color, homeworks, showEdit, showRemove, edit, remove, onClickTopic } = props
    const noUpcomingMsg = 'No homework(s) available currently', noCompletedMsg = `You haven't completed any homework(s)`
    return (
        <Box>
            {
                homeworks?.length ?
                    homeworks?.map((homework, index) => {
                        return <Card
                            color={color}
                            homework={homework}
                            key={index}
                            showEdit={showEdit}
                            showRemove={showRemove}
                            edit={edit}
                            remove={remove}
                            onClickTopic={(topic) => onClickTopic(topic, { id: homework.chapterId, name: homework.chapterName }, { id: homework.subjectId, name: homework.subjectName })}
                        />
                    })
                    : <NoDataAvailable text={tab === 'upcoming' ? noUpcomingMsg : noCompletedMsg} />
            }
        </Box>
    );
};

export default Homework;