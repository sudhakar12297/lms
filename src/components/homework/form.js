import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import _ from 'lodash';
import { Multiselect } from 'multiselect-react-dropdown';
import 'react-datepicker/dist/react-datepicker.css';

import { Box, Spacer } from '../../components/asset/wrapper';
import { Label, FormTitle } from '../../components/asset/elements';
import { CheckBox, TextArea, MultiSelect } from '../../components/asset/form-inputs';

import ProfilePicture from '../../assets/images/empty-profile-pic.svg';
import { Input } from '../asset/form-inputs';
import '../../assets/css/form.css';
import '../../assets/css/calendar.css';
import { Button } from '../asset/button';

import { isValidFormFieldData } from '../../utils/helper';

import { getSubjects, getMySubjects } from '../../reducer/subject';
import { getLessons } from '../../reducer/lesson';
import { getIsFormSubmitted, getIsFormSubmiting } from '../../reducer/homework';

import { fetchSubjectsByGroups, fetchMySubjects } from '../../action/subject';
import { fetchLessonsBySubject } from '../../action/lesson';
import validateEngine from '../../utils/validator';

const Form = (props) => {
    const history = useHistory();
    const location = useLocation();
    const dispatch = useDispatch();
    let multiselectTopicRef = React.createRef();

    const { onSubmit, groups } = props;
    const { state, pathname } = location;
    const { homework = {}, isUpdate = false } = state !== undefined && state;
    const [formData, setFormData] = useState({ assignedDate: new Date(), dueDate: new Date() });
    const [topicsList, setTopicsList] = useState([]);
    const [subjects, setSubjects] = useState({});
    const [validateOnChange, setValidateOnChange] = useState(false);
    const [errors, setErrors] = useState({});

    const mySubjects = useSelector(getMySubjects);
    const lessons = useSelector(getLessons);
    const isFormSubmiting = useSelector(getIsFormSubmiting);
    const isFormSubmitted = useSelector(getIsFormSubmitted);

    const preSelectedTopics = homework?.topic

    useEffect(() => {
        if (Object.keys(homework).length) {
            const { name, description, groupId, topicIds, assignedDate, dueDate, estimatedTime, subjectId, chapterId } = homework
            dispatch(fetchLessonsBySubject({ id: subjectId }))
            const newPayload = { ...formData, name, description, groupId, subjectId, chapterId, topicIds, assignedDate: assignedDate ? new Date(assignedDate) : new Date(), dueDate: dueDate ? new Date(dueDate) : new Date(), estimatedTime };
            setFormData(newPayload)
            validateOnChange && doValidate(newPayload);
        }
    }, [homework])

    useEffect(() => {
        if (mySubjects.length) {
            setSubjects(_.groupBy(mySubjects, 'groupId'))
        }
    }, [mySubjects])

    useEffect(() => {
        if (formData?.groupId) {
            dispatch(fetchMySubjects())
            // dispatch(fetchSubjectsByGroups({ groupIds: [formData?.groupId] }))
        }
    }, [formData?.groupId]);

    useEffect(() => {
        if (formData?.subjectId) {
            dispatch(fetchLessonsBySubject({ id: formData?.subjectId }))
        }
    }, [formData?.subjectId]);

    useEffect(() => {
        if (formData?.chapterId) {
            const topics = lessons?.filter(lesson => lesson.id === formData?.chapterId)[0]?.topics?.map(topic => topic)
            setTopicsList(topics)
        }
    }, [formData?.chapterId, lessons]);

    useEffect(() => {
        isFormSubmitted && setFormData({ assignedDate: new Date(), dueDate: new Date() });
        isFormSubmitted && setErrors({});
        isFormSubmitted && setValidateOnChange(false);
        multiselectTopicRef && multiselectTopicRef.current && multiselectTopicRef.current.resetSelectedValues();
        history.replace({ pathname, state: '' });
        dispatch({ type: 'UNSET_HOMEWORK_IS_FORM_SUBMITTED' });
    }, [isFormSubmitted, dispatch]);

    const onChange = (e, name) => {
        if (name) {
            setFormData({ ...formData, [name]: e });
            validateOnChange && doValidate({ ...formData, [name]: e });
        }
        else {
            setFormData({ ...formData, [e.target.name]: e.target.value });
            validateOnChange && doValidate({ ...formData, [e.target.name]: e.target.value });
        }
    };

    const doValidate = payload => {
        const { result = false, errors = {} } = validateEngine(payload, 'homewrokCreation');
        setErrors(errors);
        return result;
    }


    const onChangeTopic = (selectedDataArr) => {
        const topicIds = selectedDataArr.map((topic) => topic.id);
        setFormData({ ...formData, topicIds });
        validateOnChange && doValidate({ ...formData, topicIds });
    };

    const onsubmit = (e) => {
        setValidateOnChange(true);
        const { name, description, groupId, topicIds, assignedDate, dueDate, estimatedTime, subjectId, chapterId } = formData;
        const payload = {
            name,
            description,
            groupId,
            topicIds,
            dueDate,
            assignedDate,
            estimatedTime,
            subjectId,
            chapterId
        }

        if (doValidate(formData)) {
            onSubmit(
                isUpdate
                    ? {
                        isUpdate,
                        payload,
                        id: homework?._id
                    }
                    : {
                        isUpdate,
                        payload,
                    }
            )
        }
    };

    const onCancel = () => {
        setFormData({ assignedDate: new Date(), dueDate: new Date() });
        setErrors({});
        history.replace({ pathname, state: '' });
    }

    return (
        <Box>
            <FormTitle name={isUpdate ? 'Homework Updation' : 'Homework Creation'} />
            {/* <Box pb={2} className="profile-picture">
                <img src={ProfilePicture} alt="" />
            </Box> */}
            <Spacer mb={4} />
            <Label name="Homework Name" required={true} />
            <Input
                name="name"
                value={formData?.name || ''}
                onChange={(e) => onChange(e)}
                type="text"
                className="text-input-form"
                placeholder="Enter Homework Name"
                errors={errors?.name || ''}
            />
            <Spacer mb={2} />
            <Label name="Homework Description" required={true} />
            <TextArea
                name="description"
                value={formData?.description || ''}
                onChange={(e) => onChange(e)}
                // type="text"
                className="text-input-textarea-form"
                placeholder="Enter Homework Description"
                rows="3"
                errors={errors?.description || ''}
            />
            <Spacer mb={2} />
            <Label name="Select Group" required={true} />
            <select name='groupId' onChange={onChange}>
                <option value=''>Please Select Group</option>
                {
                    groups?.map((group, index) => <option value={group._id} selected={group._id === formData?.groupId}>{group.name}</option>)
                }
            </select>
            {errors?.groupId ? <p style={{ color: 'red' }}>{errors?.groupId}</p > : ''}
            {
                (formData?.groupId || isUpdate) &&
                <>
                    <Spacer mb={2} />
                    <Label name="Select Subject" required={true} />
                    <select name='subjectId' onChange={onChange}>
                        <option value=''>{subjects[formData?.groupId]?.length ? `Please Select Subject` : `No subjects available`}</option>
                        {
                            subjects[formData?.groupId]?.map((subject, index) => <option value={subject._id} selected={subject._id === homework?.subjectId}>{subject.name}</option>)
                        }
                    </select>
                    {errors?.subjectId ? <p style={{ color: 'red' }}>{errors?.subjectId}</p > : ''}
                </>
            }
            {
                (formData?.subjectId || isUpdate) &&
                <>
                    <Spacer mb={2} />
                    <Label name="Select Lesson" required={true} />
                    <select name='chapterId' onChange={onChange}>
                        <option value=''>{lessons?.length ? `Please Select Lesson` : `No lessons available`}</option>
                        {
                            lessons?.map((lesson, index) => <option value={lesson.id} selected={lesson.id === homework?.chapterId}>{lesson.name}</option>)
                        }
                    </select>
                    {errors?.chapterId ? <p style={{ color: 'red' }}>{errors?.chapterId}</p > : ''}
                </>
            }
            {
                (formData?.chapterId || isUpdate) &&
                <>
                    <Spacer mb={2} />
                    <Label name="Select Topic" required={true} />
                    <MultiSelect
                        options={topicsList}
                        selectedValues={preSelectedTopics}
                        onSelect={(selectedDataArr) => onChangeTopic(selectedDataArr)}
                        onRemove={(selectedDataArr) => onChangeTopic(selectedDataArr)}
                        displayValue="name"
                        placeholder="Please select Topic(s)"
                        singleSelect={false}
                        emptyRecordMsg="No Topic(s) available"
                        closeOnSelect={true}
                        avoidHighlightFirstOption={true}
                        ref={multiselectTopicRef}
                        style={{
                            searchBox: {
                                border: 'none',
                                background: '#E1E1E1',
                                borderRadius: '12px',
                            },
                        }}
                        errors={errors?.topicIds || ''}
                    />
                </>
            }
            <Spacer mb={3} />
            <Label name="Start Date" required={true} />
            <DatePicker
                calendarClassName="datepicker-calendar"
                // dayClassName="datepicker-day"
                popperClassName="datepicker-popper"
                wrapperClassName="datepicker-wrapper"
                className="text-input-form"
                selected={formData?.assignedDate}
                onChange={date => { onChange(date, 'assignedDate') }}
                dateFormat="MMMM d, yyyy"
            />
            <Spacer mb={3} />
            <Label name="Due Date" required={true} />
            <DatePicker
                className="text-input-form"
                wrapperClassName="datepicker-wrapper"
                selected={formData?.dueDate}
                onChange={date => { onChange(date, 'dueDate') }}
                dateFormat="MMMM d, yyyy"
            />
            <Spacer mb={3} />
            <Label name="Estimated time" required={true} />
            <Input
                name="estimatedTime"
                value={formData?.estimatedTime || ''}
                onChange={(e) => onChange(e)}
                type="number"
                className="text-input-form"
                placeholder="Enter estimated time in Minutes"
                errors={errors?.estimatedTime || ''}
            />
            <Spacer mb={2} />
            <Spacer mb={4} />
            <Button
                onClick={(e) => onsubmit(e)}
                name={isUpdate ? 'Update' : 'Create'}
                className="primary-button-lg"
                disabled={isFormSubmiting}
                loader={true}
            />
            <Spacer mb={3} />
            {
                isUpdate && <Button
                    onClick={onCancel}
                    name={'Reset'}
                    className="primary-button-lg button-red"
                />
            }
        </Box >
    );
};

export default Form;
