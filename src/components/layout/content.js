/* eslint-disable react/prop-types */
import React from 'react';
import { Box } from '../asset/wrapper';

import '../../assets/css/layout.css';

const ContentBox = (props) => {
  const {blur} = props
  return (
    <Box pr={4} py={4} className={`content ${blur&&"blur-content"}`}>
      {props.children}
    </Box>
  );
};
export default ContentBox;
