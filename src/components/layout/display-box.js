/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
import React from 'react';
import { Box } from '../asset/wrapper';

import '../../assets/css/layout.css';

export default (props) => {
  return (
    <Box pl={4} className="display-box">
      {props.children}
    </Box>
  );
};
