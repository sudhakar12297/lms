import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import InstituteLogo from '../../assets/images/e.jpeg';
import { Box, Flex, FlexWrap } from '../../components/asset/wrapper';
import '../../assets/css/header.css';
import { logout } from '../../action/account';
import { removeCookie } from '../../utils/helper';
import { Span, Label } from '../asset/elements';
import Popup from '../../components/asset/popup';
import { getUser } from '../../reducer/account';
import { ProfilePic } from '../asset/icons/profilePic';
const IconMenu = require('../../assets/images/menu.svg');

const Header = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const user = useSelector(getUser);
  const [show, setShow] = useState(false);

  const logoutConfirmed = () => {
    dispatch(logout());
    removeCookie('user');
    history.push('/login');
    setShow(false);
  };
  const { firstName = '', lastName = '', email = '', mobile = '' } = user || {};

  return (
    <Box ref={props.refs}>
      <FlexWrap
        pr={2}
        pb={1}
        pt={3}
        justifyContent="space-between"
        alignItems="center"
      >
        <Box className="nav-menu-bar" onClick={props.menuHandler}>
          <img src={IconMenu} />
        </Box>
        <FlexWrap
          className="institution-name-container"
          justifyContent="space-between"
        >
          <FlexWrap justifyContent="center">
            <Box>
              <img
                src={InstituteLogo}
                alt="logo"
                className="institution-logo"
              />
            </Box>
            <Box pt={2} mt={1} className="institution-name" title="view institute info">
              <Box className="text">{'EINSSTEIN MATRICULATION HIGHER SECONDARY SCHOOL' || props.institute_name}</Box>
              <Box pt={2} className="text">
                Nadaraja nagar, Komarapalayam
              </Box>
            </Box>
          </FlexWrap>

          <Box p={1} pr={3} mr={4} width="190px" height="55px" className="profile-card" title="Profile" onClick={() => setShow(true)}>
            <Flex>
              <Flex m={1} mr={2}><ProfilePic /></Flex>
              <Box>
                <Box pb={1} mt={2} className="person-name" >
                  {props.person_name}
                </Box>
                <Box>
                  <Span className="login-user-role" text={props.role} />
                </Box>
              </Box>
            </Flex>
          </Box>
          <Box className="mob-profile-card" title="Profile" onClick={() => setShow(true)}>
            <Span className="mob-profile-name" text={props?.person_name[0]} />
          </Box>
        </FlexWrap>
      </FlexWrap>
      <Box ml={3} mr={3}>
        <hr />
      </Box>
      <Popup
        // size="sm"
        show={show}
        title={`${firstName} ${lastName}`}
        buttonName='Logout'
        cancel={() => setShow(false)}
        confirm={logoutConfirmed}
      >

        <Box m={2} mt={3} mb={3}>
          <Flex m={3}>
            <Label name="Profile Picture" />
            <Box m={3} mb={0}>
              <ProfilePic width="66" height="66" />
            </Box>
          </Flex>
          <Flex m={3}>
            <Label name="Email" />
            <Span className="profile-details" text={email} />
          </Flex>
          <Flex m={3}>
            <Label name="Mobile" />
            <Span className="profile-details" text={mobile} />
          </Flex>
          {/* <Flex m={3}>
            <Label name="Institution" />
            <Span className="profile-details" text="School name" />
          </Flex>
          <Flex m={3}>
            <Label name="Group" />
            <Span className="profile-details" text="Group" />
          </Flex>
          <Flex m={3}>
            <Label name="Address" />
            <Span className="profile-details" text="Address" />
          </Flex> */}
        </Box>
      </Popup>
    </Box >
  );
};

export default Header;
