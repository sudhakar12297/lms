import Header from './header';
import LeftNav from './leftNav';
import RightPanel from './right-panel';
import MainBox from './main-box';
import DisplayBox from './display-box';
import ContentBox from './content';

export { Header, LeftNav, RightPanel, MainBox, DisplayBox, ContentBox };
