import React from 'react';
import { Link } from 'react-router-dom';

import { Box, Spacer, Flex } from '../asset/wrapper';
import '../../assets/css/left-nav.css';

const LeftNav = (props) => {
  const { LEFT_NAV_LIST, role, pathName } = props;
  return (
    <Box>
      {LEFT_NAV_LIST?.[role]?.map((leftNav, index) => {
        const { Icon, path, name } = leftNav;
        return (
          <Box key={index} className="nav-content-row" onClick={props.closeNavBar}>
            <Flex py="5px">
              <Box
                pt="6px"
                pr="10px"
                pb="5px"
                pl="20px"
                className={pathName === path ? 'nav-icon' : ''}
              >
                <Icon fill={pathName === path ? 'white' : '#ACACAC'} stroke={pathName === path ? 'white' : '#ACACAC'} />
              </Box>
              <Spacer mr="15px" />
              <Box pt={2}>
                <Link to={path} key={index} style={{ textDecoration: 'none' }}>
                  <Box
                    style={{
                      color: pathName === path ? '#3D2F2F' : '#ACACAC',
                    }}
                  >
                    {name}
                  </Box>
                </Link>
              </Box>
            </Flex>
          </Box>
        );
      })}
    </Box>
  );
};

export default LeftNav;
