import React from 'react';
import { Flex } from '../asset/wrapper';

export default (props) => {
  return <Flex className="main-box">{props.children}</Flex>;
};
