/* eslint-disable react/prop-types */
import React from 'react';
import { Box } from '../../components/asset/wrapper';
import '../../assets/css/layout.css';

export default (props) => {
  const { show } = props;
  return (
    <Box className={`right-panel-mobile ${show}`}>
      <Box className="form-title">{props.title}</Box>
      {props.children}
    </Box>
  );
};
