import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Collapse from 'react-bootstrap/Collapse';
import { Box, Flex, Spacer } from '../asset/wrapper';
import { IconDelete, IconEdit } from '../../components/asset/icons';
import '../../assets/css/chapter.css';
import { ROLE } from '../../utils/common';
import { setLesson } from '../../action/common';
import { getRole } from '../../reducer/account';
import { Span } from '../asset/elements';
import { Card } from '../asset/cards';
import { Button } from '../asset/button';

const ChapterCard = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const loggedInUserRole = useSelector(getRole);

  const [open, setOpen] = useState(false);

  const {
    lesson,
    collapse,
    index,
    edit,
    remove,
    className,
    editTopic,
    removeTopic,
    showDelete,
    showEdit,
    onClickTopic,
  } = props;
  const { topics } = lesson;

  const setLessonData = () => {
    dispatch(setLesson(lesson.id, lesson.name));
  };

  const onClickLesson = () => {
    setLessonData();
    return collapse === false ? null : setOpen(!open);
  };

  const onClickCreateTopic = () => {
    setLessonData();
    history.push({ pathname: '/topic/create', state: { lesson } });
  };


  return (
    <Box className={className}>
      <Card
        ariaControls="collapse-text-1"
        ariaExpanded={open}
        className={'chapter-card'}
        width="100%"
      >
        <Box p={2} onClick={onClickLesson}>
          <Flex>
            <Box>
              <Box
                m={2}
                className={
                  index % 2 === 1
                    ? `chapter-card-chapter-box-violet`
                    : `chapter-card-chapter-box-red`
                }
                onClick={onClickLesson}
              >
                <h4>
                  Chapter
                <Spacer mb={2} />
                  <Span className="bold" text={index} />
                </h4>
              </Box>
            </Box>
            <Box onClick={onClickLesson}>
              <Flex m={2} className="chapter-card-title">
                {lesson.name}
              </Flex>
              <Flex m={2} className="chapter-card-content">
                {lesson.description}
              </Flex>
            </Box>
          </Flex>
          {/* {collapse !== false && loggedInUserRole === ROLE.student && (
            <Flex m={2}>
              <span className="span-sm-red">
                {lesson?.completed
                  ? `${lesson.completed}% completed`
                  : `Not yet started`}
              </span>
            </Flex>
          )} */}
          {(showDelete || showEdit) && (
            <Flex className="user-action-icon">
              {showEdit && (
                <Box onClick={edit}>
                  <IconEdit />
                </Box>
              )}
              <Spacer mr={2} />
              {showDelete && (
                <Box onClick={remove}>
                  <IconDelete />
                </Box>
              )}
            </Flex>
          )}
        </Box>
      </Card>
      <Collapse in={open} appear={true}>
        <Box>
          {showEdit && showDelete && (
            <Flex mr={3} mb={1} onClick={onClickCreateTopic}>
              <Box ml={2} className="float-right">
                <Button
                  title="add new lesson"
                  className="primary-button-circle-sm button-green"
                  name="+"
                />
              </Box>
              <Box p={1} ml={1} mt={1} className="title-lable">
                Create Topic
              </Box>
            </Flex>

          )}
          {topics?.map((topic, index) => {
            return (
              <Flex key={index} p={1} m={2} mt={1} mb={1} id="collapse-text-1">
                <Box className="chapter-card-topic-box">{index + 1}</Box>
                <Box
                  p={2}
                  className="chapter-card-topic"
                  onClick={() => onClickTopic(topic)}
                >
                  {topic.name}
                </Box>
                {(showEdit || showDelete) &&
                  (
                    <Flex className="user-action-icon" mt={2} ml={3}>
                      {showEdit && (
                        <Box onClick={() => editTopic(topic)}>
                          <IconEdit />
                        </Box>
                      )}
                      <Spacer mr={2} />
                      {showDelete && (
                        <Box onClick={() => removeTopic(topic.id)}>
                          <IconDelete />
                        </Box>
                      )}
                    </Flex>
                  )}
                {/* {loggedInUserRole === ROLE.student && (
                  <Box p={2} className="chapter-card-topic">
                    <span className="span-sm-red">
                      Completed - 0%
                    </span>
                  </Box>
                )} */}
              </Flex>
            );
          })}
        </Box>
      </Collapse>
    </Box>
  );
};

export default ChapterCard;
