import React from 'react';
import { Box } from '../asset/wrapper';
import LessonCard from './card';
import { NoDataAvailable } from '../asset/noData';

const Display = (props) => {
  const {
    lessons,
    edit,
    remove,
    editTopic,
    removeTopic,
    showEdit = false,
    showDelete = false,
    onClickTopic
  } = props;
  return (
    <Box>
      {lessons?.map((lesson, index) => {
        return (
          <LessonCard
            key={index}
            index={index + 1}
            lesson={lesson}
            edit={(e) => { e.stopPropagation(); edit(lesson) }}
            remove={(e) => { e.stopPropagation(); remove(lesson.id) }}
            editTopic={(topic) => editTopic(topic, lesson)}
            removeTopic={(topicId) => removeTopic(topicId)}
            showEdit={showEdit}
            showDelete={showDelete}
            onClickTopic={(topic) => onClickTopic(topic)}
          />
        );
      })}
      {!lessons.length && <NoDataAvailable text='No Lesson(s) Available' />}
    </Box>
  );
};

export default Display;
