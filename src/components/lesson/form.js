import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { Box, Spacer } from '../../components/asset/wrapper';
import { Label, FormTitle } from '../../components/asset/elements';
import ProfilePicture from '../../assets/images/empty-profile-pic.svg';
import { Input, TextArea } from '../asset/form-inputs';
import '../../assets/css/form.css';
import { Button } from '../asset/button';

import { isValidFormFieldData } from '../../utils/helper';

import { getIsFormSubmitted } from '../../reducer/lesson';
import { getSelectedSubject } from '../../reducer/common';
import validateEngine from '../../utils/validator';

const Form = (props) => {
  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();

  const { onSubmit, isFormSubmitting } = props;
  const { state, pathname } = location;
  const subjectId = useSelector(getSelectedSubject).id;
  const { lesson = {}, isUpdate = false } = state !== undefined && state;
  const [formData, setFormData] = useState({});
  const [validateOnChange, setValidateOnChange] = useState(false);
  const [errors, setErrors] = useState({});

  const isFormSubmitted = useSelector(getIsFormSubmitted);

  useEffect(() => {
    if (isUpdate) {
      const { name, description } = lesson;
      setFormData({ name, desc: description });
      validateOnChange && doValidate({ name, desc: description });
    }
  }, [isUpdate, lesson]);

  useEffect(() => {
    isFormSubmitted && setFormData({});
    isFormSubmitted && setErrors({});
    isFormSubmitted && setValidateOnChange(false);
    history.replace({ pathname, state: '' });
    dispatch({ type: 'UNSET_LESSON_IS_FORM_SUBMITTED' });
  }, [isFormSubmitted, dispatch]);

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
    validateOnChange && doValidate({ ...formData, [e.target.name]: e.target.value });
  };

  const doValidate = payload => {
    const { result = false, errors = {} } = validateEngine(payload, 'lessonCreateJson');
    setErrors(errors);
    return result;
  }

  const onCancel = () => {
    setFormData({});
    setErrors({});
    history.replace({ pathname, state: '' });
  }

  const onsubmit = (e) => {
    const { name, desc } = formData;
    setValidateOnChange(true);
    if (doValidate(formData)) {
      onSubmit(
        isUpdate
          ? {
            isUpdate,
            payload: {
              name,
              description: desc,
            },
            id: lesson?.id,
            subjectId,
          }
          : {
            isUpdate,
            payload: { name, description: desc },
            subjectId,
          }
      )
    }
  };

  return (
    <Box>
      <FormTitle name={isUpdate ? 'Lesson Updation' : 'Lesson Creation'} />
      {/* <Box pb={2} className="profile-picture">
        <img src={ProfilePicture} alt="" />
      </Box> */}
      <Spacer mb={4} />
      <Label name="Lesson Name" required={true} />
      <Input
        name="name"
        value={formData?.name || ''}
        onChange={(e) => onChange(e)}
        type="text"
        className="text-input-form"
        placeholder="Enter Lesson Name"
        errors={errors?.name || ''}
      />
      <Spacer mb={2} />
      <Label name="Lesson Description" required={true} />
      <TextArea
        name="desc"
        value={formData?.desc || ''}
        onChange={(e) => onChange(e)}
        // type="text"
        className="text-input-textarea-form"
        placeholder="Enter Lesson Description"
        rows="10"
        errors={errors?.desc || ''}
      />
      <Spacer mb={4} />
      <Button
        onClick={(e) => onsubmit(e)}
        name={isUpdate ? 'Update' : 'Create'}
        className="primary-button-lg"
        disabled={isFormSubmitting}
      />
      <Spacer mb={3} />
      {isUpdate && <Button
        onClick={onCancel}
        name={'Reset'}
        className="primary-button-lg button-red"
      />}
    </Box>
  );
};

export default Form;
