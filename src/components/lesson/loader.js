import React from 'react';
import { Box, FlexWrap, Flex } from '../asset/wrapper';
import ContentLoader from 'react-content-loader';
import Card from 'react-bootstrap/Card';

const LessonLoader = (props) => {
    return (
        <Box>
            {[1, 2, 3].map(index => {
                return (
                    <Box key={index}>
                        <Card
                            aria-controls="collapse-text-1"
                            aria-expanded={false}
                            className={'chapter-card'}
                        >
                            <Box p={2}>
                                <ContentLoader
                                    speed={2}
                                    width={'100%'}
                                    height={'100%'}
                                    viewBox="0 0 100% 100%"
                                    backgroundColor="#f3f3f3"
                                    foregroundColor="#ecebeb"
                                    {...props}
                                >
                                    <rect x="15" y="15" rx="10" ry="10" width="10%" height="70%" />
                                    <rect x="110" y="20" rx="3" ry="3" width="50%" height="10%" />
                                    <rect x="110" y="50" rx="3" ry="3" width="50%" height="7%" />

                                </ContentLoader>
                            </Box>
                        </Card>
                    </Box>
                );
            })}
        </Box>
    );
};

export default LessonLoader;
