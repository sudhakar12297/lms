import React from 'react';
import moment from 'moment';

import '../../assets/css/test.css';
import { IconDelete, IconEdit } from '../../components/asset/icons';
import { Card } from 'react-bootstrap';
import { Box, Flex, FlexWrap, Spacer } from '../../components/asset/wrapper';
import { Span } from '../../components/asset/elements';
import { ProfilePic } from '../../components/asset/icons/profilePic'
import { VideoCall } from '../../components/asset/icons/videoCall'
import { Button } from '../../components/asset/button'

import '../../assets/css/live-session.css'
import { ROLE } from '../../utils/common';
import { useSelector } from 'react-redux';
import { getRole } from '../../reducer/account';
import { getGroupNameById } from '../../utils/helper';

const LiveCard = (props) => {
    const {
        color,
        live,
        groups,
        edit,
        remove,
        onClick,
        showEdit,
        showRemove,
        tag = `Today's Session`
    } = props;

    const loggedInUserRole = useSelector(getRole)

    const endTime = moment(live?.date).add(live.duration, 'minutes')

    const isCurrentTimeInBetweenStartAndEndOfLive = () => {
        return (moment(new Date()).isAfter(live.date) && moment(new Date()).isBefore(endTime))
    }


    return (
        <Box>
            <Card className="live-session-card">
                <FlexWrap mb={0}>
                    <Box m={3}>
                        <Span text={tag} className={`span-lg-${color ? 'green' : 'red'}`} />
                    </Box>
                    {loggedInUserRole !== ROLE.teacher &&
                        < Flex >
                            <Box m={"12px"}>
                                <ProfilePic width="28" height="28" />
                            </Box>
                            <Box m={3} ml={0}>
                                <Span text="Ria Wilson" className="title-s6-grey" />
                            </Box>
                        </Flex>
                    }
                    <Flex className="float-right" mr={2}>
                        <Box m={3} mr={2} onClick={(e) => edit(e, live)}>
                            {showEdit && <IconEdit />}
                        </Box>
                        <Box m={3} ml={2} onClick={(e) => remove(e, live._id)}>
                            {showRemove && <IconDelete />}
                        </Box>
                    </Flex>
                </FlexWrap>
                <Card.Body className="live-session-card-body">
                    <FlexWrap>
                        <Box mb={2} mr={3} ml={3}>
                            <Box className="title-s5">Title</Box>
                            <Flex mt={3}><Span text={live.name} className="title-s6-grey" /></Flex>
                        </Box>
                        <Box mb={2} mr={3} ml={3}>
                            <Box className="title-s5">Group</Box>
                            <Flex mt={3}><Span text={live?.groupIds?.length ? live.groupIds.map(groupId => getGroupNameById({ id: groupId, groups })).join(', ') : 'NA'} className="title-s6-grey" /></Flex>
                        </Box>
                        <Box mb={2} mr={3} ml={3}>
                            <Box className="title-s5">{`Date & Time`}</Box>
                            <Flex mt={3}><Span text={moment(live.date).format('llll')} className="span-sm-violet" /></Flex>
                        </Box>
                        <Box mb={2} mr={3} ml={3}>
                            <Box className="title-s5">Duration</Box>
                            <Flex mt={3}><Span text={live.duration} className="span-sm-green" /></Flex>
                        </Box>
                        <Box mb={2} mr={3} ml={3}>
                            <Box mt={3}>
                                <Button
                                    icon={<VideoCall />}
                                    name="Join"
                                    disabled={!isCurrentTimeInBetweenStartAndEndOfLive()}
                                    loader={false} className="primary-button-sm"
                                    onClick={(e) => onClick(e, live)}
                                />
                            </Box>
                        </Box>
                    </FlexWrap>
                </Card.Body>
            </Card >
            <Spacer mb={3} />
        </Box>
    );
};

export default LiveCard