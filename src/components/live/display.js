import React from 'react';
import { isToday } from './../../utils/helper';

import Card from './card';
import { Box } from '../asset/wrapper';
import { NoDataAvailable } from '../asset/noData';

const Display = (props) => {
    const { tab, color, lives, groups, onClick, edit, remove, showEdit, showRemove, tag = '' } = props;
    const noUpcomingMsg = 'No Upcoming Live classes', noCompletedMsg = `You haven't attend any live class`

    return (
        lives.length ? lives?.map((live, index) => {
            const _tag = isToday(live.date) ? "Today's Session" : "Upcoming's Session";
            return <Card
                color={color}
                key={index}
                live={live}
                groups={groups}
                onClick={onClick}
                edit={edit}
                remove={remove}
                showEdit={showEdit}
                showRemove={showRemove}
                tag={tag ? tag : _tag}
            />
        })
            : <NoDataAvailable text={tab === 'upcoming' ? noUpcomingMsg : noCompletedMsg} />

    );
};

export default Display;