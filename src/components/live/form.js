import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import Moment from 'moment';
import { Multiselect } from 'multiselect-react-dropdown';

import 'react-datepicker/dist/react-datepicker.css';

import { Box, Spacer } from '../../components/asset/wrapper';
import { Label, FormTitle } from '../../components/asset/elements';
import { CheckBox } from '../../components/asset/form-inputs';

import ProfilePicture from '../../assets/images/empty-profile-pic.svg';
import { Input } from '../asset/form-inputs';
import '../../assets/css/form.css';
import { Button } from '../asset/button';

import { isValidFormFieldData } from '../../utils/helper';

import { getSubjects } from '../../reducer/subject';
import { getIsFormSubmitted, getIsFormSubmitting } from '../../reducer/live';

import { fetchSubjectsByGroups } from '../../action/subject';
import '../../assets/css/test.css'
import { getUser } from '../../reducer/account';

const Form = (props) => {
    const history = useHistory();
    const location = useLocation();
    const dispatch = useDispatch();

    let multiselectGroupRef = React.createRef();

    const { onSubmit, groups } = props;
    const { state, pathname } = location;
    const { live = {}, isUpdate = false } = state !== undefined && state;
    const [formData, setFormData] = useState({ start: new Date() });
    const [errMsg, setErrMsg] = useState('');
    const [subjects, setSubjects] = useState([]);

    const subjectsFromStore = useSelector(getSubjects);
    const user = useSelector(getUser);
    const isFormSubmiting = useSelector(getIsFormSubmitting);
    const isFormSubmitted = useSelector(getIsFormSubmitted);
    const preSelectedGroups = groups && groups.filter((group) =>
        live?.groupIds?.includes(group._id)
    );

    useEffect(() => {
        if (Object.keys(live).length) {
            const { name, groupIds, subjectId, date, duration } = live
            setFormData({ ...formData, name, groupIds, subjectId, start: date ? new Date(date) : new Date(), timing: duration })
        }
    }, [live])

    useEffect(() => {
        if (subjectsFromStore.length) {
            setSubjects(subjectsFromStore.filter(subject => user.subjectId.includes(subject._id)))
        }
    }, [subjectsFromStore])

    useEffect(() => {
        if (isFormSubmitted) {
            setFormData({ start: new Date() });
            multiselectGroupRef && multiselectGroupRef.current.resetSelectedValues();
            history.replace({ pathname, state: '' });
            dispatch({ type: 'UNSET_LIVE_IS_FORM_SUBMITTED' });
        }
    }, [isFormSubmitted, dispatch]);

    const onChange = (e, name) => {
        if (name) setFormData({ ...formData, [name]: e });
        else if (e.target.name === 'isPractice') setFormData({ ...formData, [e.target.name]: e.target.checked });
        else setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const onChangeGroup = (selectedDataArr) => {
        const groupIds = selectedDataArr.map((group) => group._id);
        groupIds.length && dispatch(fetchSubjectsByGroups({ groupIds }))
        setFormData({ ...formData, groupIds });
    };

    const onCancel = () => {
        setFormData({ start: new Date() });
        multiselectGroupRef && multiselectGroupRef.current.resetSelectedValues();
        history.replace({ pathname, state: '' });
    }

    const onsubmit = (e) => {
        const { name, groupIds, subjectId, start, timing } = formData;
        const payload = {
            name,
            subjectId,
            groupIds,
            date: start,
            duration: timing,
            conductedBy: user.userId
        }
        isValidFormFieldData([name, groupIds, subjectId, start, timing])
            ? (
                Math.sign(timing) ?
                    onSubmit(
                        isUpdate
                            ? {
                                isUpdate,
                                payload,
                                id: live?._id
                            }
                            : {
                                isUpdate,
                                payload,
                            }
                    )
                    : setErrMsg('Please enter proper duration'))
            : setErrMsg('Please fill all the fields');
    };

    return (
        <Box>
            <FormTitle name={isUpdate ? 'Live Class Updation' : 'Live Class Creation'} />
            {/* <Box pb={2} className="profile-picture">
                <img src={ProfilePicture} alt="" />
            </Box> */}
            <Spacer mb={4} />
            <Label name="Live Name" />
            <Input
                name="name"
                value={formData?.name || ''}
                onChange={(e) => onChange(e)}
                type="text"
                className="text-input-form"
                placeholder="Enter Live Name"
            />
            <Spacer mb={2} />
            <Label name="Select Group" />
            <Multiselect
                options={groups}
                selectedValues={preSelectedGroups}
                onSelect={(selectedDataArr) => onChangeGroup(selectedDataArr)}
                onRemove={(selectedDataArr) => onChangeGroup(selectedDataArr)}
                displayValue="name"
                placeholder="Please select Group(s)"
                singleSelect={false}
                emptyRecordMsg="No Groups available"
                closeOnSelect={true}
                avoidHighlightFirstOption={true}
                ref={multiselectGroupRef}
                style={{
                    searchBox: {
                        border: 'none',
                        background: '#E1E1E1',
                        borderRadius: '12px',
                    },
                }}
            />
            {formData?.groupIds &&
                <>
                    <Spacer mb={2} />
                    <Label name="Select Subject" />
                    <select name='subjectId' onChange={onChange}>
                        <option value=''>{subjects?.length ? `Please Select Subject` : `No subjects available`}</option>
                        {
                            subjects?.map((subject, index) => <option value={subject._id} selected={subject._id === formData?.subjectId}>{subject.name}</option>)
                        }
                    </select>
                </>
            }
            <Spacer mb={3} />
            <Label name="Live Time" />
            <DatePicker
                className="text-input-form"
                selected={formData?.start}
                onChange={date => { onChange(date, 'start') }}
                showTimeSelect
                dateFormat="MMMM d, yyyy h:mm aa"
                minTime={
                    Moment(formData?.start).isSame(Date.now(), 'day')
                        ? new Date().setHours(new Date().getHours())
                        : new Date().setHours(0, 0, 0, 0)
                }
                maxTime={new Date().setHours(23, 59, 59, 999)}

            />
            <Spacer mb={3} />
            <Label name="Test time in minutes" />
            <Input
                name="timing"
                value={formData?.timing || ''}
                onChange={(e) => onChange(e)}
                type="number"
                className="text-input-form"
                placeholder="Enter Timing in Minutes"
            />
            <Spacer mb={2} />
            <Spacer mb={4} />
            {errMsg && <p>{errMsg}</p>}
            <Button
                onClick={(e) => onsubmit(e)}
                name={isUpdate ? 'Update' : 'Create'}
                className="primary-button-lg"
                disabled={isFormSubmiting}
                loader={true}
            />
            <Spacer mb={3} />
            {isUpdate && <Button
                onClick={onCancel}
                name={'Reset'}
                className="primary-button-lg button-red"
            />}
        </Box >
    );
};

export default Form;
