import React from 'react';
import { Box, FlexWrap, Spacer } from '../../components/asset/wrapper';
import { NoDataAvailable } from '../../components/asset/noData';
import SubjectCard from './card';

import '../../assets/css/group.css';

export default (props) => {
  const {
    subjects,
    edit,
    remove,
    groupName,
    onClick,
    showEdit = false,
    showDelete = false,
  } = props;
  return (
    <FlexWrap p={1}>
      <Box pb={3} className="title-s4">
        {`${groupName} Standard Subjects`}
        <Spacer mb={3} />
        <FlexWrap>
          {subjects?.length ? (
            subjects?.map((subject, index) => {
              return (
                <SubjectCard
                  name={subject.name}
                  code={subject.code}
                  chapterCount={subject?.chapters?.length}
                  edit={(e) => edit(subject)}
                  remove={(e) => remove(subject._id)}
                  onClick={(e) => onClick(subject)}
                  showEdit={showEdit}
                  showDelete={showDelete}
                />
              );
            })
          ) : (
              <NoDataAvailable text={`No Subject's available`} />
            )}
        </FlexWrap>
      </Box>
    </FlexWrap>
  );
};
