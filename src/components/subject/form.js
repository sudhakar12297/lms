import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { Box, Spacer } from '../../components/asset/wrapper';
import { Label, FormTitle } from '../../components/asset/elements';
import ProfilePicture from '../../assets/images/empty-profile-pic.svg';
import { Input } from '../asset/form-inputs';
import '../../assets/css/form.css';
import { Button } from '../asset/button';

import { isValidFormFieldData } from '../../utils/helper';

import { getIsFormSubmitted } from '../../reducer/subject';
import { getSelectedGroup } from '../../reducer/common';
import validateEngine from '../../utils/validator';

const Form = (props) => {
  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();

  const { onSubmit, isFormSubmitting = false } = props;
  const { state, pathname } = location;
  const { subject = {}, isUpdate = false } = state !== undefined && state;
  const [formData, setFormData] = useState({});
  const [validateOnChange, setValidateOnChange] = useState(false);
  const [errors, setErrors] = useState({});


  const selectedGroup = useSelector(getSelectedGroup);
  const isFormSubmitted = useSelector(getIsFormSubmitted);

  useEffect(() => {
    if (isUpdate) {
      const { name, code, groupId } = subject;
      setFormData({ name: name, code: code, groupId });
      validateOnChange && doValidate({ name: name, code: code, groupId });
    }
  }, [isUpdate, subject]);

  useEffect(() => {
    isFormSubmitted && setFormData({});
    isFormSubmitted && setErrors({});
    isFormSubmitted && setValidateOnChange(false);
    history.replace({ pathname, state: '' });
    dispatch({ type: 'UNSET_SUBJECT_IS_FORM_SUBMITTED' });
  }, [isFormSubmitted, dispatch]);

  const doValidate = payload => {
    const { result = false, errors = {} } = validateEngine(payload, 'subjectCreateJson');
    setErrors(errors);
    return result;
  }

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
    validateOnChange && doValidate({ ...formData, [e.target.name]: e.target.value });
  };

  const onCancel = () => {
    setFormData({});
    setErrors({});
    history.replace({ pathname, state: '' });
  }

  const onsubmit = (e) => {
    const { name, code } = formData;
    setValidateOnChange(true);
    if (doValidate(formData)) {
      onSubmit(
        isUpdate
          ? {
            isUpdate,
            payload: { name: name, code: code, groupId: selectedGroup?.id },
            id: subject?._id,
          }
          : {
            isUpdate,
            payload: { name: name, code: code, groupId: selectedGroup?.id },
          }
      )
    }
  };

  return (
    <Box>
      <FormTitle name={isUpdate ? 'Subject Updation' : 'Subject Creation'} />
      {/* <Box pb={2} className="profile-picture">
        <img src={ProfilePicture} alt="" />
      </Box> */}
      <Spacer mb={4} />
      <Label name="Subject Name" required={true} />
      <Input
        name="name"
        value={formData?.name || ''}
        onChange={(e) => onChange(e)}
        type="text"
        className="text-input-form"
        placeholder="Enter Subject Name"
        errors={errors?.name || ''}
      />
      <Spacer mb={2} />
      <Label name="Subject Code" required={true} />
      <Input
        name="code"
        value={formData?.code || ''}
        onChange={(e) => onChange(e)}
        type="text"
        className="text-input-form"
        placeholder="Enter Subject Code"
        errors={errors?.code || ''}
      />
      <Spacer mb={2} />
      <Spacer mb={4} />
      <Button
        onClick={(e) => onsubmit(e)}
        name={isUpdate ? 'Update' : 'Create'}
        className="primary-button-lg"
        disabled={isFormSubmitting}
      />
      <Spacer mb={3} />
      {isUpdate && <Button
        onClick={onCancel}
        name={'Reset'}
        className="primary-button-lg button-red"
      />}
    </Box>
  );
};

export default Form;
