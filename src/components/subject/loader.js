import React from 'react';
import '../../assets/css/group.css';
import { Box, FlexWrap, Spacer } from '../asset/wrapper';
import ContentLoader from 'react-content-loader';

const SubjectLoader = (props) => {
  const { groupName } = props;
  return (
    <FlexWrap className="subject-section">
      <Box pb={3} className="subject-section-title">
        {groupName ? `${groupName} Standard Subjects` : ''}
        <Spacer mb={3} />
        <FlexWrap>
          {[1, 2, 3, 4, 5].map((index) => (
            <Box p={3} mr={4} mb={4} className="subject-card">
              <ContentLoader
                speed={2}
                width={165}
                height={70}
                viewBox="0 0 100% 100%"
                backgroundColor="#f3f3f3"
                foregroundColor="#ecebeb"
                {...props}
              >
                <rect x="0" y="0" rx="10" ry="10" width="50" height="80%" />
                <rect x="58" y="8" rx="3" ry="3" width="50%" height="10%" />
                <rect x="58" y="28" rx="3" ry="3" width="50%" height="7%" />
                <rect x="58" y="38" rx="3" ry="3" width="50%" height="7%" />
              </ContentLoader>
            </Box>
          ))}
        </FlexWrap>
      </Box>
    </FlexWrap>
  );
};

export default SubjectLoader;
