import React from 'react';
import { Flex, Box, Spacer } from '../asset/wrapper';
import subjectIcon from '../../assets/images/subject-icon.svg';
import { IconDelete, IconEdit } from '../../components/asset/icons';

export default (props) => {
  const {
    name,
    code,
    chapterCount,
    edit,
    remove,
    onClick,
    showDelete,
    showEdit,
  } = props;
  return (
    <Box p={3} mr={4} mb={4} className="subject-card">
      {(showDelete || showEdit) && (
        <Flex className="user-action-icon">
          {showEdit && (
            <Box onClick={edit}>
              <IconEdit />
            </Box>
          )}
          <Spacer mr={2} />
          {showDelete && (
            <Box onClick={remove}>
              <IconDelete />
            </Box>
          )}
        </Flex>
      )}
      <Flex onClick={onClick}>
        <Box pr={2}>
          <img src={subjectIcon} alt="" />
        </Box>
        <Box>
          <Box pb={2} title={name} className="title">
            {name}
          </Box>
          <Box pb={2} className="code">
            {`Subject code: ${code}`}
          </Box>
          <Box className="chapter-count">{`No of Chapters: ${chapterCount}`}</Box>
        </Box>
      </Flex>
    </Box>
  );
};
