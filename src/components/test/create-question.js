import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Papaparse from 'papaparse';
import { Box, Flex, Spacer } from '../../components/asset/wrapper';
import { TextArea, RadioButton } from '../../components/asset/form-inputs';
import { Label } from '../../components/asset/elements';
import { Button } from '../../components/asset/button';
import { Span } from '../../components/asset/elements';
import { fetchLessonsBySubject } from '../../action/lesson';
import MyCard from '../../components/asset/cards/card';
import { getLessons } from '../../reducer/lesson';
import 'react-quill/dist/quill.snow.css';
import '../../assets/css/editor.css';

const headerMapping = {
    'Question Type': 'type',
    'Question': 'question',
    'Option 1': 'option1',
    'Option 2': 'option2',
    'Option 3': 'option3',
    'Option 4': 'option4',
    'Correct Answer': 'correctAnswer',
    'Weightage': 'weightage',
}

const typeMapper = {
    'SINGLE_CHOICE': 'single_choice',
    'TEXT': 'text',
    'MULTIPLE_CHOICE': 'mul_choice'
}

const Form = (props) => {
    const { test = {} } = props;
    const { subjectId, lessonId = '', topicId = '' } = test;
    const dispatch = useDispatch();
    const lessons = useSelector(getLessons);
    const [formData, setFormData] = useState({ lessonId, topicId });
    const [questions, setQuestions] = useState([]);

    const onChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    useEffect(() => {
        dispatch(fetchLessonsBySubject({ id: subjectId }))
    }, [subjectId]);

    const getLessonOptions = () => {
        if (lessonId) {
            const [firstLesson = {}] = lessons.filter(lesson => lesson.id == lessonId);
            const { id = '', name = '' } = firstLesson;
            return <option value={id} selected={true}>{name}</option>
        } else if (!lessons.length) {
            return <option value={''} selected={true}>{'No option'}</option>
        } else {
            let _options = [<option value='' selected>Select Lesson</option>]
            return [..._options, ...lessons.map(lesson => <option value={lesson.id} selected={lesson.id == formData.lessonId}>{lesson.name}</option>)];
        }
    }

    const getTopicOptions = () => {
        let _lessonId = lessonId || formData.lessonId || '';
        if (_lessonId) {
            const [firstLesson = {}] = lessons.filter(lesson => lesson.id == _lessonId);
            const { topics = [] } = firstLesson;
            if (topicId) {
                const [firstTopic = {}] = topics.filter(topic => topic.id == topicId);
                const { id = '', name = '' } = firstTopic;
                return <option value={id} selected={true}>{name}</option>
            } else if (topics.length) {
                let _options = [<option value='' selected>Select Topic</option>]
                return [..._options, ...topics.map(topic => <option value={topic.id} selected={topic.id == formData.topicId}>{topic.name}</option>)];
            } else {
                return <option value={''} selected={true}>{'No option'}</option>
            }
        } else {
            return <option value={''} selected={true}>{'No option'}</option>
        }
    }

    const formPayload = (questions) => {
        return questions.map(question => {
            const { type = '', question: name, option1, option2, option3, option4, correctAnswer, weightage } = question;
            let _question = { question: name, type: typeMapper[type], correctAnswer, weightage, subjectId, lessonId: formData?.lessonId, topicId: formData?.topicId };
            switch (type) {
                case "MULTIPLE_CHOICE":
                case "SINGLE_CHOICE":
                    _question['options'] = [option1, option2, option3, option4].map((option, index) => ({ id: `${index + 1}`, option }));
            }
            return _question;
        });
    }

    const onChangeUpload = (e) => {
        e.preventDefault()
        const file = e.target.files[0];
        Papaparse.parse(file, {
            download: true,
            complete: (data) => {
                const rows = data.data
                const [head = [], ...bodyData] = rows;
                const _questions = bodyData.map((questions) => questions.reduce((accum, question, index) => {
                    return {
                        ...accum,
                        [headerMapping[head[index]]]: question
                    }
                }, {}));
                const payload = formPayload(_questions);
                setQuestions(payload);
            }
        })
    }

    const handleSubmit = () => {
        props.onSubmit({ questions })
    }

    return (
        <Box>
            <Label name="Select Lesson" />
            <select name='lessonId' onChange={onChange}>
                {getLessonOptions()}
            </select>
            <Spacer mb={3} />
            {formData?.lessonId &&
                <>
                    <Spacer mb={2} />
                    <Label name="Select Topic" />
                    <select name='topicId' onChange={onChange}>
                        {getTopicOptions()}
                    </select>
                </>
            }
            <Spacer mb={5} />
            <input type="file" name="file" onChange={onChangeUpload} accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
            <Spacer mb={5} />

            {questions.length ? <MyCard className="test-evaluation-card">
                <Spacer mb={3} />

                {questions.map((question, index) => {
                    const { type = '', options = [], question: name = '', weightage = '', correctAnswer = '', _id: questionId = '' } = question;
                    switch (type) {
                        case "single_choice":
                            return (
                                <Flex className="question" m={2} mb={1} mt={1}>
                                    <Box width="80%" mr={2}>
                                        <Box mb={2}>
                                            {index + 1}. {name}
                                        </Box>
                                        <Box>
                                            {options.map((option, optionIndex) => {
                                                const color = optionIndex + 1 == correctAnswer ? 'green' : '';
                                                return <RadioButton
                                                    name={questionId}
                                                    group={questionId}
                                                    value={option.id}
                                                    disabled={true}
                                                    displayLabel={option.option}
                                                    type="checkbox"
                                                    className={`choice ${color}`}
                                                    checked={color != ''}
                                                />
                                            })}
                                        </Box>
                                    </Box>
                                    <Flex m={1}>
                                        <Box width="35px" height="35px" p={"12px"} className="mark-box float-right">
                                            <Span text={weightage} className="red bold" />
                                        </Box>
                                    </Flex>
                                </Flex>
                            );
                        case "mul_choice":
                            return (<Flex className="question" m={2} mb={1} mt={1}>
                                <Box mb={2} mt={2} width="80%" mr={2}>
                                    <Box mb={2}>
                                        {index + 1}. {name}
                                    </Box>
                                    <Box>
                                        {options.map((option, optionIndex) => {
                                            const color = correctAnswer.split(',').includes(`${optionIndex + 1}`) ? 'green' : '';
                                            return <RadioButton
                                                name={questionId}
                                                group={questionId}
                                                value={option.id}
                                                disabled={true}
                                                displayLabel={option.option}
                                                type="checkbox"
                                                className={`choice ${color}`}
                                                checked={color != ''}
                                            />
                                        })}
                                    </Box>
                                </Box>
                                <Flex m={1}>
                                    <Box width="35px" height="35px" p={"12px"} className="mark-box float-right">
                                        <Span text={weightage} className="red bold" />
                                    </Box>
                                </Flex>
                            </Flex>);
                        case "text":
                            return (<Flex className="question" m={2} mb={1} mt={1}>
                                <Box mb={2} mt={2} width="80%" mr={2}>
                                    <Box mb={2}>
                                        {index + 1}. {name}
                                    </Box>
                                    <Box>
                                        <TextArea className="text-input-textarea-form" rows={6} value={''} disabled={true} />
                                    </Box>
                                </Box>
                                <Flex m={1}>
                                    <Box width="35px" height="35px" p={"12px"} className="mark-box float-right">
                                        <Span text={weightage} className="red bold" />
                                    </Box>
                                </Flex>
                            </Flex>);
                    }
                })}
                <Box m={2} width="100px" className="float-right">
                    <Button className="primary-button-sm" name="submit" onClick={handleSubmit} />
                </Box>
            </MyCard> : ''}
        </Box>
    );

}



export default Form;
