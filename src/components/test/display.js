import React from "react";
import moment from 'moment';
import { Box, FlexWrap, Spacer, Flex } from '../../components/asset/wrapper';
import { IconEdit, IconDelete } from '../asset/icons';
import '../../assets/css/group.css';
import '../../assets/css/test.css';
import { Span } from '../asset/elements';
import Accordion from 'react-bootstrap/Accordion'
import { Card } from 'react-bootstrap';
import { Button } from '../asset/button';
import { NoDataAvailable } from '../asset/noData';

export default (props) => {
  let {
    tab,
    color,
    tests,
    edit,
    remove,
    onClick,
    onClickAttend,
    onClickEvaluate,
    showAttend,
    showEdit,
    showRemove,
    groups,
    showEvaluate,
    showNoQuestions,
    disableAttend,
    isLive,
  } = props;

  const noUpcomingMsg = 'No Upcoming Exam(s)', noCompletedMsg = `You haven't attend any exam(s) still`,
    liveMsg = 'No Live Exam(s)';

  const getGroupNameById = (id) => {
    const filteredGroup = groups?.filter(group => group._id === id)
    return filteredGroup[0]?.name
  }

  tab == 'upcoming' || tab == 'live' ? tests.sort((a, b) => moment(a.start).diff(moment(b.start))) : tests.sort((a, b) => moment(b.start).diff(moment(a.start)));

  return (
    <Box>
      {
        tests.length ?
          tests?.map((test, index) => {
            return (
              <Accordion defaultActiveKey="">
                <Card className="test-card">
                  <Accordion.Toggle as={Card.Header} eventKey="0">
                    <FlexWrap>
                      <Box m={2}>
                        <Span text={test.isPractice ? "Practice" : "Exam"} className={`span-lg-${color ? 'green' : 'red'}`} />
                      </Box>
                      <Box m={2} className="title-s4">
                        {test.name}
                      </Box>
                      <Box m={2} mt={0}>
                        {showNoQuestions && !test.questions?.length ? <Button
                          // icon={<VideoCall />}
                          name="No Questions"
                          // disabled={!isCurrentTimeInBetweenStartAndEndOfLive()}
                          disabled={true} loader={false} className="primary-button-sm button-red"
                        /> : ''}
                      </Box>
                      <Flex className="float-right">
                        <Box m={2} onClick={(e) => edit(e, test)}>
                          {!isLive && showEdit && <IconEdit />}
                        </Box>
                        <Box m={2} onClick={(e) => remove(e, test._id)}>
                          {!isLive && showRemove && <IconDelete />}
                        </Box>
                      </Flex>
                    </FlexWrap>
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body className="test-card-body">
                      <FlexWrap>
                        <Box m={3}>
                          <Box m={2} className="title-s5">Group</Box>
                          <Box m={2} mt={3}><Span text={getGroupNameById(test.groupId)} className="title-s6-grey" /></Box>
                        </Box>
                        <Box m={3}>
                          <Box m={2} className="title-s5">Test Starting Time</Box>
                          <Box m={2} mt={3}><Span text={test.isPractice ? 'NA' : moment(test.start).format('llll')} className="span-sm-violet" /></Box>
                        </Box>
                        <Box m={3}>
                          <Box m={2} className="title-s5">Test Duration</Box>
                          <Box m={2} mt={3}><Span text={test.isPractice ? 'NA' : `${test.timing} mins`} className="span-sm-blue" /></Box>
                        </Box>
                        <Box m={3}>
                          <Box mt={3}>
                            {!isLive && showEvaluate && test.questions?.length ?
                              <Button
                                name="Evaluate"
                                // disabled={!isCurrentTimeInBetweenStartAndEndOfLive()}
                                loader={false} className="primary-button-sm"
                                onClick={(e) => onClickEvaluate(e, test)}
                              /> : ''
                            }
                            {isLive && showAttend && test.questions?.length ? <Button
                              // icon={<VideoCall />}
                              name="Attend"
                              // disabled={!isCurrentTimeInBetweenStartAndEndOfLive()}
                              loader={false} className="primary-button-sm"
                              onClick={(e) => onClickAttend(e, test)}
                            /> : ''}
                            {disableAttend && !(isLive && showAttend && test.questions?.length) ?
                              <Button
                                // icon={<VideoCall />}
                                name="Attend"
                                // disabled={!isCurrentTimeInBetweenStartAndEndOfLive()}
                                disabled={true} loader={false} className="primary-button-sm"
                              /> : ''}

                          </Box>
                        </Box>
                        <Box m={4}>
                          {!isLive && showEdit && <Box><Button name="+" title="Add questions" onClick={(e) => { onClick(e, test) }} className="primary-button-circle-lg" /></Box>}
                        </Box>
                      </FlexWrap>
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Spacer mb={3} />
              </Accordion>
            )
          })
          :
          <NoDataAvailable text={tab === 'upcoming' ? noUpcomingMsg : tab == 'live' ? liveMsg : noCompletedMsg} />
      }
    </Box>
  );
};
