/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';

//action
import { fetchTests, deleteTest } from '../../action/test';
//reducer
import { getRole } from '../../reducer/account';
import { getTests } from '../../reducer/test';

import Test from '../../components/test/display';

//assets
import { Flex, FlexWrap, Box, Spacer } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import Popup from '../../components/asset/popup';

import Form from './form';
import { ROLE } from '../../utils/common';

import '../../assets/css/group.css';
import { getMyGroups } from '../../reducer/group';
import { fetchMyGroups } from '../../action/group';
import { isStudent, isTeacher } from '../../utils/helper';
import { LiveIcon } from '../../components/asset/icons/live';
import '../../assets/css/test.css'
import { Span } from '../../components/asset/elements';
import { Card } from '../../components/asset/cards';
import { CheckBox, TextArea, RadioButton } from '../../components/asset/form-inputs';
import { ProfilePic } from '../../components/asset/icons/profilePic';

const Evaluate = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();
    const { state } = location

    return (
        <Flex className="test-evaluation">
            <ContentBox>
                <FlexWrap>
                    <Flex ml={2}>
                        <LiveIcon widht="40px" height="40px" />
                    </Flex>
                    <Flex m={2} className="page-title">
                        Test
                    </Flex>
                    <Flex mt={2} className="title-lable float-right">
                        Total Marks Scored -<Span text=" 25" className="red" />
                    </Flex>
                </FlexWrap>
                <Card className="test-evaluation-card">
                    <Box m={2} className="title-lable">
                        <Span text="Choose the best" className="blue" /> -1 Mark
                    </Box>
                    <Spacer mb={3} />
                    <Box className="question" m={2} mb={1} mt={1}>
                        <Flex mb={2} mt={2}>
                            <Box>
                                1. Which of the general groups below would be considered a 'social institution' by most sociologists?
                            </Box>
                            <Box ml={3} width="35px" height="35px" p={"12px"} className="mark-box">
                                <Span text="1" className="red bold" />
                            </Box>
                        </Flex>

                        <RadioButton
                            name="choice 1"
                            value="choice"
                            displayLabel="choice 1"
                            type="checkbox"
                            className="choice red"
                        // onChange={() => onChange()}
                        />
                        <RadioButton
                            name="choice 1"
                            value="choice"
                            displayLabel="choice 1"
                            type="checkbox"
                            className="choice"
                        // onChange={() => onChange()}
                        />
                        <RadioButton
                            name="choice 1"
                            value="choice"
                            displayLabel="choice 1"
                            type="checkbox"
                            className="choice green"
                        // onChange={() => onChange()}
                        />
                        <RadioButton
                            name="choice 1"
                            value="choice"
                            displayLabel="choice 1"
                            type="checkbox"
                            className="choice"
                        // onChange={() => onChange()}
                        />
                    </Box>
                    <Box className="question" className="question" m={2} mb={1} mt={1}>
                        <Flex mb={2} mt={2} >
                            <Box>
                                2. Select the answer that contains the original meaning of the word 'manufacturing.'
                            </Box>
                            <Box ml={3} width="35px" height="35px" p={"12px"} className="mark-box">
                                <Span text="5" className="red bold" />
                            </Box>
                        </Flex>
                        <TextArea className="text-input-textarea-form" rows={6} />
                    </Box>
                    <Box className="question" className="question" m={2} mb={1} mt={1}>
                        <Flex mb={2} mt={2}>
                            <Box>
                                3. Select the answer that contains the original meaning of the word 'manufacturing.'
                            </Box>
                            <Box ml={3} width="35px" height="35px" p={"12px"} className="mark-box">
                                <Span text="5" className="red bold" />
                            </Box>
                        </Flex>
                        <TextArea className="text-input-textarea-form" rows={6} />
                    </Box>
                </Card>
            </ContentBox>
            <RightPanel>
                <Box className="title-lable" mb={3}>
                    Test details
                </Box>
                <Box className="right-first-part">
                    <Box m={2}>
                        <Span text="Practice test" className="test-type-completed" />
                    </Box>
                    <Flex>
                        <Box m={2}>
                            <Box m={2} className="test-card-topics">Test Name</Box>
                            <Box m={2}><Span text="Test Name" className="test-group" /></Box>
                        </Box>
                        <Box m={2}>
                            <Box m={2} className="test-card-topics">Subject</Box>
                            <Box m={2}><Span text="Subject" className="test-group" /></Box>
                        </Box>
                    </Flex>
                    <Box m={2}>
                        <Box m={2} className="test-card-topics">Lesson</Box>
                        <Box m={2}><Span text="Topic of the chapter" className="test-group" /></Box>
                    </Box>
                    <Flex>
                        <Box m={2}>
                            <Box m={2} className="test-card-topics">Test Starting Time</Box>
                            <Box m={2}><Span text="12:30 pm" className="test-time" /></Box>
                        </Box>
                        <Box m={2}>
                            <Box m={2} className="test-card-topics">Test Duration</Box>
                            <Box m={2}><Span text="60 mins" className="test-duration" /></Box>
                        </Box>
                    </Flex>
                    <Flex>
                        <Box m={2}>
                            <Box m={2} className="test-card-topics">Total Questions</Box>
                            <Box m={2}><Span text="06" className="questions-added" /></Box>
                        </Box>
                        <Box m={2}>
                            <Box m={2} className="test-card-topics">Staff Name</Box>
                            <Flex>
                                <ProfilePic width="24" height="24" />
                                <Box m={2}><Span text="Ria Wilson" className="test-group" /></Box>
                            </Flex>
                        </Box>
                    </Flex>
                </Box>
                <Box m={2} className="title-lable">
                    Students
                </Box>
                <Box className="right-second-part">
                    <Box m={2} mb={3}>
                        <Flex p={1}>
                            <ProfilePic width="42" height="42" />
                            <Box>
                                <Box className="test-group" m={1} >Student 1</Box>
                                <Box m={1} ><Span className="roll-no" text="20F2352" /></Box>
                            </Box>
                        </Flex>
                        <Flex p={1} className="selected-student">
                            <ProfilePic width="42" height="42" />
                            <Box>
                                <Box className="test-group" m={1} >Student 1</Box>
                                <Box m={1} ><Span className="roll-no" text="20F2352" /></Box>
                            </Box>
                        </Flex>
                        <Flex p={1}>
                            <ProfilePic width="42" height="42" />
                            <Box>
                                <Box className="test-group" m={1} >Student 1</Box>
                                <Box m={1} ><Span className="roll-no" text="20F2352" /></Box>
                            </Box>
                        </Flex>
                        <Flex p={1}>
                            <ProfilePic width="42" height="42" />
                            <Box>
                                <Box className="test-group" m={1} >Student 1</Box>
                                <Box m={1} ><Span className="roll-no" text="20F2352" /></Box>
                            </Box>
                        </Flex>
                    </Box>
                </Box>
            </RightPanel>
        </Flex >
    );
};

export default Evaluate;
