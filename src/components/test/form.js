import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import Moment from 'moment';
import _ from 'lodash';
import 'react-datepicker/dist/react-datepicker.css';

import { Box, Spacer } from '../../components/asset/wrapper';
import { Label, FormTitle } from '../../components/asset/elements';
import { CheckBox } from '../../components/asset/form-inputs';

import { Input } from '../asset/form-inputs';
import '../../assets/css/form.css';
import { Button } from '../asset/button';

import { isValidFormFieldData } from '../../utils/helper';

import { getMySubjects } from '../../reducer/subject';
import { getLessons } from '../../reducer/lesson';
import { getIsFormSubmitted, getIsFormSubmiting } from '../../reducer/test';

import { fetchMySubjects } from '../../action/subject';
import { fetchLessonsBySubject } from '../../action/lesson';
import validateEngine from '../../utils/validator';
import '../../assets/css/test.css'

const Form = (props) => {
  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();

  const { onSubmit, groups } = props;
  const { state, pathname } = location;
  const { test = {}, isUpdate = false } = state !== undefined && state;
  const [formData, setFormData] = useState({ start: new Date(), isPractice: false });
  const [errMsg, setErrMsg] = useState('');
  const [subjects, setSubjects] = useState({});
  const [validateOnChange, setValidateOnChange] = useState(false);
  const [errors, setErrors] = useState({});

  const mySubjects = useSelector(getMySubjects);
  const lessons = useSelector(getLessons);
  const isFormSubmiting = useSelector(getIsFormSubmiting);
  const isFormSubmitted = useSelector(getIsFormSubmitted);

  useEffect(() => {
    if (Object.keys(test).length) {
      const { name, groupId, subjectId, lessonId, topicId, isPractice, start, timing } = test
      const newPayload = { ...formData, name, groupId, subjectId, lessonId, topicId, isPractice, start: start ? new Date(start) : new Date(), timing };
      setFormData(newPayload)
      validateOnChange && doValidate(newPayload);
    }
  }, [test])

  useEffect(() => {
    if (mySubjects.length) {
      setSubjects(_.groupBy(mySubjects, 'groupId'))
    }
  }, [mySubjects])

  useEffect(() => {
    if (formData?.groupId) {
      dispatch(fetchMySubjects())
    }
  }, [formData?.groupId]);

  useEffect(() => {
    if (formData?.subjectId) {
      dispatch(fetchLessonsBySubject({ id: formData?.subjectId }))
    }
  }, [formData?.subjectId]);

  useEffect(() => {
    if (formData?.lessonId) {

    }
  }, [formData?.lessonId]);

  useEffect(() => {
    isFormSubmitted && setFormData({ start: new Date() });
    isFormSubmitted && setErrors({});
    isFormSubmitted && setValidateOnChange(false);
    history.replace({ pathname, state: '' });
    dispatch({ type: 'UNSET_TEST_IS_FORM_SUBMITTED' });
  }, [isFormSubmitted, dispatch]);

  const onChange = (e, name) => {
    if (name) {
      setFormData({ ...formData, [name]: e })
      validateOnChange && doValidate({ ...formData, [name]: e });
    } else if (e.target.name === 'isPractice') {
      setFormData({ ...formData, [e.target.name]: e.target.checked })
      validateOnChange && doValidate({ ...formData, [e.target.name]: e.target.checked });
    } else {
      setFormData({ ...formData, [e.target.name]: e.target.value });
      validateOnChange && doValidate({ ...formData, [e.target.name]: e.target.value });
    }
  };

  const doValidate = payload => {
    const { result = false, errors = {} } = validateEngine(payload, 'testCreation');
    setErrors(errors);
    return result;
  }

  const onsubmit = (e) => {
    setValidateOnChange(true);
    const { name, groupId, subjectId, lessonId, topicId, isPractice, start, timing } = formData;
    const payload = {
      name,
      subjectId,
      groupId,
      lessonId,
      topicId,
      isPractice,
      start,
      timing
    }

    if (formData?.isPractice) {
      payload['start'] = ''
      payload['timing'] = ''
    }
    //  else {
    //   if (!Math.sign(timing)) {
    //     return setErrMsg('Please enter proper duration');
    //   }
    // }

    if (doValidate(payload)) {
      onSubmit(
        isUpdate
          ? {
            isUpdate,
            payload,
            id: test?._id
          }
          : {
            isUpdate,
            payload,
          }
      )
    }
  };

  const onCancel = () => {
    setFormData({ start: new Date() });
    setErrors({});
    history.replace({ pathname, state: '' });
  }

  return (
    <Box>
      <FormTitle name={isUpdate ? 'Test Updation' : 'Test Creation'} />
      {/* <Box pb={2} className="profile-picture">
        <img src={ProfilePicture} alt="" />
      </Box> */}
      <Spacer mb={4} />
      <Label name="Test Name" required={true} />
      <Input
        name="name"
        value={formData?.name || ''}
        onChange={(e) => onChange(e)}
        type="text"
        className="text-input-form"
        placeholder="Enter Test Name"
        errors={errors?.name || ''}
      />
      <Spacer mb={2} />
      <Label name="Select Group" required={true} />
      <select name='groupId' onChange={onChange}>
        <option value=''>Please Select Group</option>
        {
          groups?.map((group, index) => <option value={group._id} selected={group._id === formData?.groupId}>{group.name}</option>)
        }
      </select>
      {errors?.groupId ? <p style={{ color: 'red' }}>{errors?.groupId}</p > : ''}
      {formData?.groupId &&
        <>
          <Spacer mb={2} />
          <Label name="Select Subject" required={true} />
          <select name='subjectId' onChange={onChange} class="input-text" className="select-field">
            <option value=''>{subjects[formData?.groupId]?.length ? `Please Select Subject` : `No subjects available`}</option>
            {
              subjects[formData?.groupId]?.map((subject, index) => <option value={subject._id} selected={subject._id === formData?.subjectId}>{subject.name}</option>)
            }
          </select>
          {errors?.subjectId ? <p style={{ color: 'red' }}>{errors?.subjectId}</p > : ''}
        </>
      }
      {formData?.subjectId &&
        <>
          <Spacer mb={2} />
          <Label name="Select Lesson" required={true} />
          <select name='lessonId' onChange={onChange}>
            <option value=''>{lessons?.length ? `Please Select Lesson` : `No lessons available`}</option>
            {
              lessons?.map((lesson, index) => <option value={lesson.id} selected={lesson.id === formData?.lessonId}>{lesson.name}</option>)
            }
          </select>
          {errors?.lessonId ? <p style={{ color: 'red' }}>{errors?.lessonId}</p > : ''}
        </>
      }
      {formData?.lessonId &&
        <>
          <Spacer mb={2} />
          <Label name="Select Topic" />
          <select name='topicId' onChange={onChange}>
            <option value=''>{lessons?.filter(lesson => lesson.id === formData?.lessonId)[0]?.topics?.length ? `Please Select Topic` : `No topics available`}</option>
            {
              lessons?.filter(lesson => lesson.id === formData?.lessonId)[0]?.topics?.map((topic, index) => <option value={topic.id} selected={topic.id === formData?.topicId}>{topic.name}</option>)
            }
          </select>
          {errors?.topicId ? <p style={{ color: 'red' }}>{errors?.topicId}</p > : ''}
        </>
      }
      <Spacer mb={3} />
      <Box pb={1}>
        <CheckBox type='checkbox' name='isPractice' displayLabel='Is this Practice test?' checked={formData?.isPractice} onChange={e => onChange(e)} className="" />
      </Box>
      {!formData?.isPractice &&
        <>
          <Spacer mb={2} />
          <Label name="Test start time" required={true} />
          <DatePicker
            calendarClassName="datepicker-calendar"
            // dayClassName="datepicker-day"
            // timeClassName="datepicker-timer"
            popperClassName="datepicker-popper"
            wrapperClassName="datepicker-wrapper"
            className="text-input-form"
            selected={formData?.start}
            onChange={date => { onChange(date, 'start') }}
            showTimeSelect
            dateFormat="MMMM d, yyyy h:mm aa"
            minTime={
              Moment(formData?.start).isSame(Date.now(), 'day')
                ? new Date().setHours(new Date().getHours())
                : new Date().setHours(0, 0, 0, 0)
            }
            maxTime={new Date().setHours(23, 59, 59, 999)}

          />
          <Spacer mb={2} />
          <Label name="Test time in minutes" required={true} />
          <Input
            name="timing"
            value={formData?.timing || ''}
            onChange={(e) => onChange(e)}
            type="number"
            className="text-input-form"
            placeholder="Enter Timing in Minutes"
            errors={errors?.timing || ''}
          />
        </>
      }
      <Spacer mb={2} />
      <Spacer mb={4} />
      <Button
        onClick={(e) => onsubmit(e)}
        name={isUpdate ? 'Update' : 'Create'}
        className="primary-button-lg"
        disabled={isFormSubmiting}
        loader={true}
      />
      <Spacer mb={3} />
      {isUpdate && <Button
        onClick={onCancel}
        name={'Reset'}
        className="primary-button-lg button-red"
      />}
    </Box >
  );
};

export default Form;
