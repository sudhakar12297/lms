import React from 'react';
import { Box, Flex, Spacer } from '../asset/wrapper';
import { Card } from '../asset/cards';
import { VideoCall } from '../asset/icons/videoCall';
import { Chat } from '../asset/icons/chat'
import { Span } from '../asset/elements';
import { ProfilePic } from '../asset/icons/profilePic';
import { IconEdit, IconDelete } from '../asset/icons';

const GroupList = (props) => {
    const { test, showEdit = false, showRemove = false, edit, remove } = props
    return (
        <Box>
            <table>
                <thead>
                    <tr className="list-table-header">
                        <th> </th>
                        <th>Name</th>
                        <th>Group</th>
                        <th>Start time</th>
                        <th>Timing (minutes)</th>
                        {showEdit && showRemove && <th>Edit</th>}
                        {showEdit && showRemove && <th>Delete</th>}
                    </tr>
                </thead>
                <Spacer mb={2} />
                <tbody>
                    <tr className="list-table-body">
                        <td><ProfilePic width="32" height="32" /></td>
                        <td className="name-style">{test.name}</td>
                        <td>{test.groupId}</td>
                        <td>{test.start}</td>
                        <td>{test.timing}</td>
                        {showEdit && showRemove && <td><IconEdit /></td>}
                        {showEdit && showRemove && <td><IconDelete /></td>}
                    </tr>
                    <Spacer mb={2} />
                </tbody>
            </table>
        </Box >
    );
};

export default GroupList;