import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import Editor from '../editor';
import { Box, Flex, FlexWrap, Spacer } from '../../components/asset/wrapper';

import 'react-quill/dist/quill.snow.css';

import '../../assets/css/editor.css';
import { Card } from '../../components/asset/cards';
import { Label } from '../../components/asset/elements';
import { Button } from '../../components/asset/button';
import { Input } from '../../components/asset/form-inputs';
import AttachIcon from '../../assets/images/attach.svg';

import { isValidFormFieldData } from '../../utils/helper';
import { getSelectedSubject, getSelectedLesson } from '../../reducer/common';
import { getIsFormSubmitted, getTopic } from '../../reducer/topic';
import { fetchTopicById } from '../../action/topic';
import validateEngine from '../../utils/validator';

const Form = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const { state = {} } = location;

  const { isUpdate = false, topicId = '' } = state

  const { onSubmit, onChangeParent } = props;
  const [formData, setFormData] = useState('');
  const [editorData, setEditorData] = useState('');
  const [validateOnChange, setValidateOnChange] = useState(false);
  const [errors, setErrors] = useState({});

  const selectedSubject = useSelector(getSelectedSubject);
  const selectedLesson = useSelector(getSelectedLesson);
  const isFormSubmitted = useSelector(getIsFormSubmitted);
  const topic = useSelector(getTopic);

  useEffect(() => {
    if (isUpdate) {
      dispatch(fetchTopicById({ topicId, lessonId: selectedLesson.id, subjectId: selectedSubject.id }))
    }
  }, [isUpdate]);

  useEffect(() => {
    if (Object.keys(topic).length) {
      const { name, content } = topic;
      setFormData({ name });
      validateOnChange && doValidate({ name, editorData });
      setEditorData(content);
    }
  }, [topic]);

  useEffect(() => {
    if (isFormSubmitted) {
      setFormData('');
      setErrors({});
      setValidateOnChange(false);
      dispatch({ type: 'UNSET_TOPIC_IS_FORM_SUBMITTED' });
      history.push('/lesson');
    }
  }, [isFormSubmitted]);

  const doValidate = payload => {
    const { result = false, errors = {} } = validateEngine(payload, 'topicCreateJson');
    setErrors(errors);
    return result;
  }

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
    validateOnChange && doValidate({ ...formData, [e.target.name]: e.target.value, editorData });
  };

  const onChangeEditor = (value) => {
    setEditorData(value);
    return onChangeParent(value);
  };

  const onsubmit = () => {
    setValidateOnChange(true);
    if (doValidate({ ...formData, editorData })) {
      const payload = {
        name: formData.name,
        content: editorData,
      };
      onSubmit(
        isUpdate
          ? {
            isUpdate,
            payload,
            subjectId: selectedSubject.id,
            lessonId: selectedLesson.id,
            id: topic.id,
          }
          : {
            isUpdate,
            payload,
            subjectId: selectedSubject.id,
            lessonId: selectedLesson.id,
          }
      );
    }
  };

  const onCancel = () => {
    setFormData('');
    setErrors({});
    history.push('/lesson');
  }

  return (
    <Box>
      <Label name="Topic Name" className="editor-card-label" required={true} />
      <Spacer mb={2} />
      <Input
        type="text"
        name="name"
        value={formData?.name || ''}
        placeholder="Topic Name"
        className="text-input-topic-input"
        onChange={onChange}
        errors={errors?.name || ''}
      />
      <Spacer mb={2} />
      <FlexWrap mr={3}>
        <Label name="Topic Content" className="editor-card-label" />
        <Flex className="float-right">
          <Box mt={1}>
            <Button
              name={<img src={AttachIcon} alt="attach" />}
              className="primary-button-sm button-green"
              title="attach file"
            />
          </Box>
          <Input
            placeholder="copy the generrated url"
            className="text-input-topic-url-input"
          />
        </Flex>
      </FlexWrap>
      <Card className={'editor-card'} width="100%">
        <Editor value={editorData} onChange={onChangeEditor} errors={errors?.editorData || ''} />
      </Card>
      <FlexWrap justifyContent="space-between">

        <Flex className="float-right">
          <Button
            name={'Reset'}
            className="primary-button-sm button-red"
            onClick={onCancel}
          />
          <Spacer mr={2} />
          <Button
            name={isUpdate ? 'Update' : 'Create'}
            className="primary-button-sm"
            onClick={onsubmit}
          />
        </Flex>
      </FlexWrap>
    </Box >
  );
};

export default Form;