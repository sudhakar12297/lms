import React from 'react';
import Card from 'react-bootstrap/Card';
import Nav from 'react-bootstrap/Nav'
import { Box, Flex } from '../components/asset/wrapper';
import TopicImage from '../assets/images/topic_image.svg'

import '../assets/css/topic.css';
import { NoDataAvailable } from './asset/noData';

const TopicCard = () => {

    return (
        <Box>
            <NoDataAvailable height={300} width={300} text='We are working on this....' />
        </Box>
    );
};

export default TopicCard;