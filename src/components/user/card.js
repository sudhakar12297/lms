/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
import React from 'react';
import { FlexWrap, Box, Flex, Spacer } from '../asset/wrapper';
import { Card } from '../asset/cards';
import '../../assets/css/user.css';
import ProfilePic from '../../assets/images/profilepic.svg';
import VideoIcon from '../../assets/images/video_icon.svg';
import ChatIcon from '../../assets/images/chat_icon.svg';

import { IconDelete, IconEdit } from '../../components/asset/icons';
import { Span } from '../asset/elements';

export default (props) => {
  const {
    name,
    role,
    handlingSections,
    mobile,
    mail,
    edit,
    remove,
    showEdit,
    showDelete,
  } = props;
  return (
    <Card width="360px" className="user-card-wrapper">
      {(showEdit || showDelete) && (
        <Flex className="user-action-icon">
          {showEdit && (
            <Box onClick={edit}>
              <IconEdit />
            </Box>
          )}
          <Spacer mr={2} />
          {showDelete && (
            <Box onClick={remove}>
              <IconDelete />
            </Box>
          )}
        </Flex>
      )}
      <Box className="user-card">
        <Flex>
          <Box pr="20px">
            <img src={ProfilePic} width="90px" height="100px" alt="" />
            <FlexWrap pt={1} pl={2}>
              <Box pt={2} pl={'5px'}>
                <img src={VideoIcon} alt="" />
              </Box>
              <Box pt={2} pl={2}>
                <img src={ChatIcon} alt="" />
              </Box>
            </FlexWrap>
          </Box>
          <Box>
            {/*  */}
            <Flex pb={2} className="user-card-title">
              {name}
            </Flex>
            <Box pb={1}>
              <Span className="user-card-role" text={role} />
            </Box>
            <Box className="user-card-content">
              <Box pt={2}>{handlingSections}</Box>
              <Box pt={2}>{mobile}</Box>
              <Box pt={2} className="mail">
                {mail}
              </Box>
            </Box>
            {/* <Box className="chapter-count">{`No of Chapters: ${chapterCount}`}</Box> */}
          </Box>
        </Flex>
      </Box>
    </Card>
  );
};

// export default (props) => {
//   return (
//     <div className="profile_card_row">
//       <div className="profile_card">
//         <div className="additional">
//           <div className="user_card">
//             <div>
//               <img className="" src={ProfilePic} alt="profile picture" />
//             </div>
//             <div className="profile_grp">
//               <img className="profile_icon" src={ChatIcon} alt="chat icon" />
//               <img className="profile_icon" src={VideoIcon} alt="video icon" />
//             </div>
//           </div>
//         </div>
//         <div className="general">
//           <div className="identity">
//             <h1>{props.name}</h1>
//             <img
//               className="profile_menu"
//               src={ProfileMenu}
//               alt="profile menu"
//             />
//           </div>
//           <div>
//             <p className="profile_role">{props.role}</p>
//             <p>
//               {props.class}
//               <br />
//               {props.number}
//               <br />
//               {props.email}
//             </p>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };
