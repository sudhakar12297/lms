/* eslint-disable react/jsx-key */
import React from 'react';
import { useSelector } from 'react-redux';
import { FlexWrap, Spacer } from '../asset/wrapper';
import UserCard from './card';
import { getGroups } from '../../reducer/group';
import List from './list';

const User = (props) => {
  const groups = useSelector(getGroups);
  const { users, edit, remove, showEdit = false, showDelete = false, isLoading = false, emptyMessage = '' } = props;

  const getGroupNameById = (id) => {
    const filtered = groups?.filter((group) => group._id === id);
    return filtered[0]?.name || '';
  };

  return (
    <FlexWrap className="user-container">
      {/* {users?.map((user, findIndex) => {
        return (
          <UserCard
            key={findIndex}
            name={user.firstName}
            role={'Teacher'}
            handlingSections={user.groupId
              .map((id) => getGroupNameById(id))
              .join(', ')}
            mobile={user.mobile}
            mail={user.email}
            edit={(e) => edit(user)}
            remove={(e) => remove(user._id)}
            showEdit={showEdit}
            showDelete={showDelete}
          />
        );
      })} */}
      <List
        users={users}
        edit={edit}
        remove={remove}
        showEdit={showEdit}
        showDelete={showDelete}
        groups={groups}
        isLoading={isLoading}
        emptyMessage={emptyMessage}
      />

    </FlexWrap>
  );
};

export default User;
