import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useHistory } from 'react-router-dom';
import { Multiselect } from 'multiselect-react-dropdown';

//helper
import {
  isValidFormFieldData,
  convertToStringIfUndefined,
} from '../../utils/helper';
import { Box, Spacer, Flex } from '../asset/wrapper';
import { Input, MultiSelect } from '../asset/form-inputs';
import { Label, FormTitle } from '../asset/elements';
import { RadioButton } from '../asset/form-inputs';
import { Button } from '../asset/button';

import { getIsFormSubmitted } from '../../reducer/account';
import { getSubjects } from '../../reducer/subject';
import { fetchSubjectsByGroups } from '../../action/subject';
import validateEngine from '../../utils/validator';

const Form = (props) => {
  const { isFormSubmitting = false } = props;
  const dispatch = useDispatch();
  const location = useLocation();
  const history = useHistory();
  const { state, pathname } = location;
  const [validateOnChange, setValidateOnChange] = useState(false);
  const [errors, setErrors] = useState({});

  const page = pathname.split('/').pop();

  const userObj = state ? state.userObj : null;

  const { groups = [], teachers, onsubmit } = props;

  const isFormSubmitted = useSelector(getIsFormSubmitted);
  const subjectsByGroupsIds = useSelector(getSubjects);
  const [subjectOptions, setSubjectOptions] = useState([]);
  const preSelectedGroups = groups?.filter((group) =>
    userObj?.groupId?.includes(group._id)
  );

  const preSelectedSubjects = subjectOptions?.filter((subject) =>
    userObj?.subjectId?.includes(subject._id)
  );

  //local state
  const [formData, setFormData] = useState({ role: page });

  let multiselectGroupRef = React.createRef();
  let multiselectSubjectRef = React.createRef();
  let multiselectTutorRef = React.createRef();

  useEffect(() => {
    if (userObj) {
      const {
        email,
        fatherEmail,
        fatherMobile,
        firstName,
        gender,
        groupId,
        lastName,
        mobile,
        motherEmail,
        motherMobile,
        role,
        subjectId,
        userId,
        tutor,
      } = userObj;
      dispatch(fetchSubjectsByGroups({ groupIds: groupId }))
      const newPayload = {
        ...formData, email, fatherMobile, fatherEmail, firstName, lastName,
        gender, mobile, motherEmail, motherMobile, role, tutor,
        group: groupId, id: userId, subjectIds: subjectId,
      }
      setFormData(newPayload);
      validateOnChange && doValidate(newPayload);
    }
  }, [userObj]);

  //if doing update : set user groups into formData

  useEffect(() => {
    if (isFormSubmitted) {
      setFormData({ role: page });
      multiselectGroupRef && multiselectGroupRef.current && multiselectGroupRef.current.resetSelectedValues();
      multiselectSubjectRef && multiselectSubjectRef.current && multiselectSubjectRef.current.resetSelectedValues();
      multiselectTutorRef && multiselectTutorRef.current && multiselectTutorRef.current.resetSelectedValues();
      history.replace({ pathname, state: {} });
      setErrors({});
      setValidateOnChange(false);
      dispatch({ type: 'UNSET_USER_IS_FORM_SUBMITTED' });
    }
  }, [isFormSubmitted]);

  useEffect(() => {
    setSubjectOptions(subjectsByGroupsIds.map(subject => {
      const { groupId } = subject;
      const selectedSubject = groups?.filter(group => group._id === groupId);
      const [{ name = '' } = {}] = selectedSubject;
      return {
        ...subject,
        name: name ? `${subject.name} (${name})` : subject.name
      }
    }))
  }, [subjectsByGroupsIds]);

  const onCancel = () => {
    setFormData({ role: page });
    setErrors({});
    history.replace({ pathname, state: {} });
  }

  const doValidate = payload => {
    const { result = false, errors = {} } = validateEngine(payload, isStudentSelected() ? 'studentCreateJson' : 'userCreateJson');
    setErrors(errors);
    return result;
  }

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
    validateOnChange && doValidate({ ...formData, [e.target.name]: e.target.value });
  };

  const onChangeGroup = (selectedDataArr) => {
    const groupIds = selectedDataArr.map((group) => group._id);
    multiselectSubjectRef && multiselectSubjectRef.current.resetSelectedValues();
    groupIds.length && dispatch(fetchSubjectsByGroups({ groupIds }))
    setFormData({ ...formData, group: groupIds });
    validateOnChange && doValidate({ ...formData, group: groupIds });
  };

  const onChangeSubject = (selectedDataArr) => {
    const subjectIds = selectedDataArr.map((subject) => subject._id);
    setFormData({ ...formData, subjectIds: subjectIds });
    validateOnChange && doValidate({ ...formData, subjectIds: subjectIds });
  };

  const onChangeTutor = (selectedData) => {
    setFormData({ ...formData, tutor: selectedData[0]._id });
    validateOnChange && doValidate({ ...formData, tutor: selectedData[0]._id });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    setValidateOnChange(true);
    if (doValidate(formData)) {
      let payload = {
        role: formData?.role,
        firstName: formData?.firstName,
        lastName: convertToStringIfUndefined(formData?.lastName),
        email: formData?.email,
        mobile: formData?.mobile,
        fatherEmail: convertToStringIfUndefined(formData?.fatherEmail),
        fatherMobile: convertToStringIfUndefined(formData?.fatherMobile),
        motherEmail: convertToStringIfUndefined(formData?.motherEmail),
        motherMobile: convertToStringIfUndefined(formData?.motherMobile),
        userId: formData?.id,
        groupId: formData?.group,
        subjectId: formData?.subjectIds,
        gender: formData?.gender,
        tutor: formData?.tutor || '',
      };

      onsubmit(
        userObj
          ? { id: userObj._id, payload, isCreate: false }
          : { payload, isCreate: true }
      );
    }
  };

  const isStudentSelected = () => {
    return formData?.role === 'student';
  };

  return (
    <div>
      <FormTitle name={userObj ? 'User updation' : 'User creation'} />
      <Spacer mb={4} />
      <form>
        <Flex>
          <Label name="First Name" required={true} />
        </Flex>
        <Input
          name="firstName"
          value={formData?.firstName || ''}
          type="text"
          placeholder="First Name"
          className="text-input-form"
          onChange={(e) => onChange(e)}
          errors={errors?.firstName || ''}
        />
        <Spacer mb={2} />
        <Label name="Last Name" />
        <Input
          name="lastName"
          value={formData?.lastName || ''}
          type="text"
          placeholder="Last Name"
          className="text-input-form"
          onChange={(e) => onChange(e)}
          errors={errors?.lastName || ''}
        />
        <Spacer mb={2} />
        <Label name="Select Group(s)" required={true} />
        <MultiSelect
          options={groups}
          selectedValues={preSelectedGroups}
          onSelect={(selectedDataArr) => onChangeGroup(selectedDataArr)}
          onRemove={(selectedDataArr) => onChangeGroup(selectedDataArr)}
          displayValue="name"
          placeholder="Please select Group(s)"
          singleSelect={isStudentSelected() ? true : false}
          emptyRecordMsg="No Groups available"
          closeOnSelect={true}
          avoidHighlightFirstOption={true}
          ref={multiselectGroupRef}
          style={{
            searchBox: {
              border: 'none',
              background: '#E1E1E1',
              borderRadius: '12px',
            },
          }}
          errors={errors?.group || ''}
        />
        <Spacer mb={3} />
        <Label name="Select Subjects(s)" required={true} />
        <MultiSelect
          options={subjectOptions}
          selectedValues={preSelectedSubjects}
          onSelect={(selectedDataArr) => onChangeSubject(selectedDataArr)}
          onRemove={(selectedDataArr) => onChangeSubject(selectedDataArr)}
          displayValue="name"
          placeholder="Please select Subject(s)"
          singleSelect={false}
          emptyRecordMsg="No Subjects available"
          closeOnSelect={true}
          avoidHighlightFirstOption={true}
          ref={multiselectSubjectRef}
          style={{
            searchBox: {
              border: 'none',
              background: '#E1E1E1',
              borderRadius: '12px',
            },
          }}
          errors={errors?.subjectIds || ''}
        />
        <Spacer mb={3} />
        <Label name="Id" required={true} />
        <Input
          name="id"
          value={formData?.id || ''}
          type="text"
          placeholder="ID"
          className="text-input-form"
          onChange={(e) => onChange(e)}
          errors={errors?.id || ''}
        />
        <Spacer mb={2} />
        <Label name="Email" required={true} />
        <Input
          name="email"
          value={formData?.email || ''}
          type="email"
          placeholder="Email"
          className="text-input-form"
          onChange={(e) => onChange(e)}
          errors={errors?.email || ''}
        />
        <Spacer mb={2} />
        <Label name="Mobile" required={true} />
        <Input
          name="mobile"
          value={formData?.mobile || ''}
          type="tel"
          pattern="[0-9]{10}"
          placeholder="Mobile"
          className="text-input-form"
          onChange={(e) => onChange(e)}
          errors={errors?.mobile || ''}
        />
        <Spacer mb={2} />
        <Label name="Gender" required={true} />
        <RadioButton
          name="gender"
          group="gender"
          value="Male"
          displayLabel="Male"
          type="radio"
          checked={
            formData?.gender === 'Male' || formData?.gender === 'male'
              ? true
              : false
          }
          onChange={(e) => onChange(e)}
        />
        <RadioButton
          name="gender"
          group="gender"
          value="Female"
          displayLabel="Female"
          type="radio"
          checked={
            formData?.gender === 'Female' || formData?.gender === 'female'
              ? true
              : false
          }
          onChange={(e) => onChange(e)}
          errors={errors?.gender || ''}
        />
        {isStudentSelected() && (
          <Box>
            <Spacer mb={2} />
            <Label name="Select Tutor" required={true} />
            <MultiSelect
              options={teachers}
              onSelect={(selectedData) => onChangeTutor(selectedData)}
              onRemove={(selectedData) => onChangeTutor(selectedData)}
              displayValue="firstName"
              placeholder="Please select Tutor"
              singleSelect={true}
              emptyRecordMsg="No teachers available"
              closeOnSelect={false}
              avoidHighlightFirstOption={true}
              ref={multiselectTutorRef}
              style={{
                searchBox: {
                  border: 'none',
                  background: '#E1E1E1',
                  // border-radius : "12px",
                },
              }}
              errors={errors?.tutor || ''}
            />
            <Spacer mb={2} />
            <Label name="Father's Email" />
            <Input
              name="fatherEmail"
              value={formData?.fatherEmail || ''}
              type="email"
              placeholder="Father's Email"
              className="text-input-form"
              onChange={(e) => onChange(e)}
              errors={errors?.fatherEmail || ''}
            />
            <Spacer mb={2} />
            <Label name="Father's Mobile" />
            <Input
              name="fatherMobile"
              value={formData?.fatherMobile || ''}
              type="tel"
              pattern="[0-9]{10}"
              placeholder="Father's Mobile"
              className="text-input-form"
              onChange={(e) => onChange(e)}
              errors={errors?.fatherMobile || ''}
            />
            <Spacer mb={2} />
            <Label name="Mother's Email" />
            <Input
              name="motherEmail"
              value={formData?.motherEmail || ''}
              type="email"
              placeholder="Mother's Email"
              className="text-input-form"
              onChange={(e) => onChange(e)}
              errors={errors?.motherEmail || ''}
            />
            <Spacer mb={2} />

            <Label name="Mother's Mobile" />
            <Input
              name="motherMobile"
              value={formData?.motherMobile || ''}
              type="tel"
              pattern="[0-9]{10}"
              placeholder="Mother's Mobile"
              className="text-input-form"
              onChange={(e) => onChange(e)}
              errors={errors?.motherMobile || ''}
            />
          </Box>
        )}
        <Spacer mb={3} />
        <Button
          onClick={(e) => onSubmit(e)}
          name={userObj ? 'Update' : 'Create'}
          className="primary-button-lg"
          disabled={isFormSubmitting}
        />
        <Spacer mb={3} />
        {userObj && <Button
          className="primary-button-lg button-red"
          name={'Reset'}
          onClick={(e) => onCancel(e)}
        />}
      </form>
    </div>
  );
};

export default Form;
