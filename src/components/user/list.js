import React from 'react';
import { Box, Flex, Spacer } from '../asset/wrapper';
import { Card } from '../asset/cards';
import { ProfilePic } from '../asset/icons/profilePic';
import { VideoCall } from '../asset/icons/videoCall';
import { Chat } from '../asset/icons/chat'
import { Span } from '../asset/elements';
import { NoDataAvailable } from '../asset/noData';
import { IconEdit, IconDelete } from '../asset/icons';
import UserLoader from './loader';
import { toTitleCase } from '../../utils/helper';

const getGroupNameById = (groups, id) => {
    const filtered = groups?.filter((group) => group._id === id);
    return filtered[0]?.name || '';
};


const getBody = (options) => {
    const { users = [], showEdit = false, showDelete = false, edit, remove, groups } = options;
    let body = [];
    users.forEach(user => {
        const { firstName = '', mobile = '', email = '', _id = '', role = '', groupId = [] } = user;
        body.push(<tr className="list-table-body">
            <td><ProfilePic width="32" height="32" /></td>
            <td className="name-style">{firstName}</td>
            <td><Span text={toTitleCase(role)} className="desc-style" /></td>
            <td>{groupId.map(id => getGroupNameById(groups, id)).join(', ')}</td>
            <td>{mobile}</td>
            <td className="email-style">{email}</td>
            {/* <td><VideoCall /></td>
            <td><Chat /></td> */}
            {showEdit && <td><Box onClick={() => edit(user)}><IconEdit /></Box></td>}
            {showDelete && <td><Box onClick={() => remove(_id)}><IconDelete /></Box></td>}
        </tr>);
        body.push(<Spacer mb={2} />);
    });
    return body;
}

const List = (props) => {
    const { showEdit = false, showDelete = false, isLoading = false, emptyMessage = '' } = props;
    let userBody = getBody(props);
    return userBody.length || isLoading ?
        (
            <Box width="100%">
                <table>
                    <thead>
                        <tr className="list-table-header">
                            <th> </th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>Standards</th>
                            <th>Phone Number</th>
                            <th>Email Id</th>
                            {/* <th>Call</th>
                            <th>Chat</th> */}
                            {showEdit && <th></th>}
                            {showDelete && <th></th>}
                        </tr>
                    </thead>
                    <Spacer mb={2} />
                    {!isLoading ? <tbody>{userBody} </tbody> : <UserLoader type='list' cols={8} />}
                </table>
            </Box >
        ) : (
            <NoDataAvailable height={300} width={500} text={emptyMessage} />
        );
};

export default List;