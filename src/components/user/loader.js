import React from 'react';
import '../../assets/css/group.css';
import { FlexWrap } from '../asset/wrapper';
import { Card } from '../asset/cards';
import ContentLoader from 'react-content-loader';
import Loader from '../../components/asset/loader';

const UserLoader = (props) => {
  const { type = 'card', cols = 0 } = props;
  return (
    type == 'card' ? [1, 2].map((index) => (
      <FlexWrap>
        <Card width="370px" className="group-card-wrapper">
          <ContentLoader
            speed={2}
            width={'100%'}
            height={'100%'}
            viewBox="0 0 100% 100%"
            backgroundColor="#f3f3f3"
            foregroundColor="#ecebeb"
            {...props}
          >
            <rect x="0" y="0" rx="10" ry="10" width="94" height="80%" />
            <rect x="108" y="8" rx="3" ry="3" width="50%" height="10%" />
            <rect x="108" y="48" rx="3" ry="3" width="50%" height="7%" />
            <rect x="108" y="68" rx="3" ry="3" width="50%" height="7%" />
            <rect x="108" y="88" rx="3" ry="3" width="50%" height="7%" />
          </ContentLoader>
        </Card>
      </FlexWrap>
    )) :
      <FlexWrap>
        <Loader />
      </FlexWrap>
    // [1].map(index => (
    //   <tr className="list-table-body" >
    //     <FlexWrap>
    //       <td colspan={10}>
    //         <Card width="370px" className="group-card-wrapper">
    //           <ContentLoader
    //             speed={2}
    //             width={'100%'}
    //             height={'100%'}
    //             viewBox="0 0 100% 100%"
    //             backgroundColor="#f3f3f3"
    //             foregroundColor="#ecebeb"
    //             {...props}
    //           >
    //             <rect x="0" y="0" rx="10" ry="10" width="94" height="80%" />
    //             {/* <rect x="0" y="0" rx="10" ry="10" width="94" height="80%" />
    //           <rect x="108" y="8" rx="3" ry="3" width="50%" height="10%" />
    //           <rect x="108" y="48" rx="3" ry="3" width="50%" height="7%" />
    //           <rect x="108" y="68" rx="3" ry="3" width="50%" height="7%" />
    //           <rect x="108" y="88" rx="3" ry="3" width="50%" height="7%" /> */}
    //           </ContentLoader>
    //         </Card>
    //       </td>
    //     </FlexWrap>
    //   </tr>
    // ))
  );
};

export default UserLoader;
