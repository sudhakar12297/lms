import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../middleware/configureStore';
import 'bootstrap/dist/css/bootstrap.min.css';
import { PersistGate } from 'redux-persist/integration/react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Router from '../routes';

let { store, persistor } = configureStore();
function App() {
  return (
    <Provider store={store}>
      <ToastContainer closeOnClick={false} draggable={false} autoClose={5000} hideProgressBar={true} />
      <PersistGate loading={null} persistor={persistor}>
        <Router />
      </PersistGate>
    </Provider>
  );
}

export default App;
