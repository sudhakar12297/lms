import React from 'react';
import { useSelector } from 'react-redux';

//reducer
import { getRole } from '../../reducer/account';

import SuperAdmin from './superAdmin';
import Admin from './admin';
import Teacher from './teacher';
import Student from './student';
import { NoDataAvailable } from '../../components/asset/noData';

const Dashboard = () => {
  const role = useSelector(getRole);
  switch (role) {
    case 'super-admin':
      return <SuperAdmin />;
    case 'admin':
      return <Admin />;
    case 'teacher':
      return <Teacher />;
    case 'student':
      return <Student />;
    default:
      return <NoDataAvailable height={300} width={300} text='We are working on this....' />;
  }
};

export default Dashboard;
