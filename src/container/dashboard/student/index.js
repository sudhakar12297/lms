import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setGroup } from '../../../action/common';
import { getUser } from '../../../reducer/account';

import Homework from "../../homework";

const Student = () => {
  const dispatch = useDispatch();
  const loggedInUser = useSelector(getUser);

  useEffect(() => {
    dispatch(setGroup(loggedInUser.groupId[0]));
  }, [dispatch]);

  //for temp solution, included homework to student dashboard 
  return (
    <Homework />
  );
};

export default Student;
