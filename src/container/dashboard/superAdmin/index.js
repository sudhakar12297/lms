import React, { useState } from 'react';
import { Chart } from "react-google-charts";
import Calendar from 'react-calendar';
import { Box, Flex, FlexWrap } from '../../../components/asset/wrapper';
import { ContentBox } from '../../../components/layout';
import RightPanel from '../../../container/layout/rightPanel'
import MyCard from '../../../components/asset/cards/card';
import { Card } from 'react-bootstrap';
import 'react-calendar/dist/Calendar.css';
import '../../../assets/css/calendar.css'
import '../../../assets/css/dashboard.css'
import { Bar } from 'react-chartjs-2';
import { Pie, Doughnut } from 'react-chartjs-2';
import { MobileRightPanel } from '../../layout/mobile';
import { Button } from '../../../components/asset/button';
import { CalendarIcon } from '../../../components/asset/icons/calendar';
import { Close } from '../../../components/asset/icons/close';
import { IconDashboard } from '../../../components/asset/icons';
import Accordion from 'react-bootstrap/Accordion';
import { ScheduleCard } from '../../../components/asset/cards/scheduleCard';


const SuperAdmin = () => {


  const doughnut_state_1 = {
    labels: ['Students Passed', 'Students Above 90%', 'Students Failed',
      'Students Below 50%'],
    datasets: [
      {
        label: 'Students',
        backgroundColor: [
          '#138BFC',
          '#9ACDFE',
          '#4FA8FD',
          '#B3DAFE'
        ],
        hoverBackgroundColor: [
          '#094a87',
          '#53708c',
          '#28537d',
          '#5d7285'
        ],
        data: [80, 45, 30, 20]
      }
    ]
  }

  const doughnut_state_2 = {
    labels: ['Students Passed', 'Students Above 90%'],
    datasets: [
      {
        label: 'Students',
        backgroundColor: [
          '#138BFC',
          '#9ACDFE'
        ],
        hoverBackgroundColor: [
          '#094a87',
          '#53708c'
        ],
        data: [80, 45]
      }
    ]
  }

  const bar_state = {
    labels: ['Tamil', 'English', 'Science',
      'Social', 'Maths'],
    datasets: [
      {
        label: 'Completed percentage',
        backgroundColor: [
          '#3D73DD',
          '#FF9066',
          '#138BFC',
          '#6160B9',
          '#F65164'
        ],
        borderWidth: 1,
        data: [52, 42, 67, 48, 78]
      }
    ]
  }
  const [value, onChange] = useState(new Date());

  const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
  const onClickToggleRightPanel = () => {
    setShowRightPanelOnMobile(!showRightPanelOnMobile);
  };

  return (
    <Flex className="dashboard">
      <ContentBox blur={showRightPanelOnMobile}>
        {/* <Box class="app" mb={2}>
          <ul class="hs full no-scrollbar">
            <li class="item">test</li>
            <li class="item">test</li>
            <li class="item">test</li>
            <li class="item">test</li>
          </ul>
        </Box> */}
        <Flex mb={3}>
          <IconDashboard fill={"#138BFC"} height="28" width="28" />
          <Box ml={3} className="page-title">
            Dashboard
          </Box>
        </Flex>
        <FlexWrap>
          <MyCard>
            <Doughnut
              data={doughnut_state_1}
              options={{
                title: {
                  display: true,
                  text: 'Exam Statistics',
                  fontSize: 16
                },
                legend: {
                  display: true,
                  position: 'right'
                }
              }}
            />
          </MyCard>
          <MyCard>
            <Doughnut
              data={doughnut_state_2}
              options={{
                title: {
                  display: true,
                  text: 'Pass percentage',
                  fontSize: 16
                },
                legend: {
                  display: true,
                  position: 'right'
                }
              }}
            />
          </MyCard>
        </FlexWrap>
        <Box>
          <MyCard width="60%">
            <Bar
              data={bar_state}
              options={{
                title: {
                  display: true,
                  text: 'Syllabus Completion',
                  fontSize: 16
                },
                legend: {
                  display: false,
                  position: 'right'
                }
              }}
            />
          </MyCard>
        </Box>
      </ContentBox>
      <RightPanel>
        <Calendar
          onChange={onChange}
          value={value}
          className=" react-calendar-custom-style"
        />
        <Box mt={2}>
          <Accordion defaultActiveKey="">
            <Card className="rightpanel-cards">
              <Accordion.Toggle as={Card.Header} eventKey="0">
                <Box className="title-lable" m={2}>
                  Today's Schedule
              </Box>
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  <Flex mt={1}>
                    <Box mt={3} className="title-s5">
                      08.00 am
                    </Box>
                    <ScheduleCard title="Maths" detail="Online Class" explanation="Chapter" color="bg-green" textColor="red" />
                  </Flex>
                  <Flex mt={1}>
                    <Box mt={3} className="title-s5">
                      09.00 am
                    </Box>
                    <ScheduleCard title="Maths" detail="Online Class" explanation="Chapter" color="bg-red" textColor="navy-blue" />
                  </Flex>
                  <Flex mt={1}>
                    <Box mt={3} className="title-s5">
                      10.00 am
                    </Box>
                    <ScheduleCard title="Maths" detail="Online Class" explanation="Chapter" color="bg-blue" textColor="red" />
                  </Flex>
                  <Flex mt={1}>
                    <Box mt={3} className="title-s5">
                      11.00 am
                    </Box>
                    <ScheduleCard title="Maths" detail="Online Class" explanation="Chapter" color="bg-grey" textColor="navy-blue" />
                  </Flex>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </Box>
      </RightPanel>
      <MobileRightPanel
        title="Group Creation"
        show={showRightPanelOnMobile ? 'visible' : 'hide'}
      >
        <Calendar
          onChange={onChange}
          value={value}
          className=" react-calendar-custom-style"
        />
      </MobileRightPanel>
      {!showRightPanelOnMobile && (
        <Button
          name={<CalendarIcon />}
          title={'Create'}
          className={'mob-primary-button-form-'}
          onClick={onClickToggleRightPanel}
        />
      )}
      {showRightPanelOnMobile && (
        <Button
          name={<Close />}
          title={'Close'}
          className={'mob-primary-button-form-close'}
          onClick={onClickToggleRightPanel}
        />
      )}
    </Flex >
  );
};

export default SuperAdmin;
