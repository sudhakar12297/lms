import React from 'react';
import { Box } from '../../../components/asset/wrapper';
import { NoDataAvailable } from '../../../components/asset/noData';


const Teacher = () => {
    return (
        <Box>
            <NoDataAvailable height={300} width={300} text='We are working on this...' />
        </Box>
    );
};

export default Teacher;
