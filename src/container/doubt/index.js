import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';

import { Box, Flex } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import Discussion from '../../components/doubt/discussion';

import { getIsLoading, getIsError, getMyDoubts } from '../../reducer/doubt';
import { fetchMyDoubts } from '../../action/doubt';
import { Span } from '../../components/asset/elements';
import { NoDataAvailable } from '../../components/asset/noData';


const MyDoubts = () => {
    const dispatch = useDispatch();

    const isLoading = useSelector(getIsLoading);
    const isError = useSelector(getIsError);
    const myDoubts = useSelector(getMyDoubts)

    const [doubtsGroupByTopic, setDoubtsGroupByTopic] = useState({})

    useEffect(() => {
        dispatch(fetchMyDoubts());
    }, [dispatch]);

    useEffect(() => {
        setDoubtsGroupByTopic(_.groupBy(myDoubts, 'topicId'))
    }, [myDoubts]);

    const getTopicNameById = (id) => {
        const filtered = myDoubts.filter(doubt => doubt.topicId === id)
        return filtered[0].topicName
    }

    return (
        <Flex>
            <ContentBox>
                {
                    (myDoubts && myDoubts?.length) ?
                        (
                            Object.keys(doubtsGroupByTopic)?.map(topicId => {
                                return (
                                    <>
                                        <Box>
                                            <Box m={2} p={2} pl={0}><Span text={getTopicNameById(topicId) || '---'} className="topic-header" /></Box>
                                            {doubtsGroupByTopic[topicId]?.map(doubt => {
                                                return <Box m={2}><Discussion doubt={doubt} hideAskAndReplyDoubt={true} /></Box>
                                            })}
                                        </Box>
                                    </>
                                )
                            })

                        )
                        : <NoDataAvailable text={`No Doubt(s) asked yet`} />
                }
            </ContentBox>
            <RightPanel>
                coming soon
            </RightPanel>
        </Flex>
    );
};

export default MyDoubts;
