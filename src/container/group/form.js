import React from 'react';
import { useDispatch } from 'react-redux';
import { updateGroup, createGroup } from '../../action/group';
import Form from '../../components/group/form';
import 'react-datepicker/dist/react-datepicker.css';

const GroupForm = (props) => {
  const { isFormSubmitting = false } = props;
  const dispatch = useDispatch();

  const submit = (data) => {
    data.isCreate
      ? dispatch(createGroup({ payload: data.payload }))
      : dispatch(updateGroup({ id: data.id, payload: data.payload }));
  };

  return <Form onSubmit={(data) => submit(data)} isFormSubmitting={isFormSubmitting} />;
};

export default GroupForm;
