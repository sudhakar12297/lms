import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import Form from './form';

import GroupList from '../../components/group/display';
import GroupLoader from '../../components/group/loader';
import { Flex, FlexWrap, Box } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import { MobileRightPanel } from '../layout/mobile';
import Popup from '../../components/asset/popup';
//action
import { fetchGroups, deleteGroup, fetchMyGroups } from '../../action/group';
import { setGroup } from '../../action/common';

//reducer
import {
  getGroups,
  getIsLoading,
  getIsFormSubmitted,
  getIsFormSubmitting,
  getMyGroups,
} from '../../reducer/group';
import { getRole } from '../../reducer/account';

import '../../assets/css/group.css';
import { Button } from '../../components/asset/button';
import { ROLE } from '../../utils/common';
import { IconStudent, IconGroup } from '../../components/asset/icons';
import { Close } from '../../components/asset/icons/close';
import { AddIcon } from '../../components/asset/icons/add';

const Group = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [show, setShow] = useState(false);
  const [groups, setGroups] = useState([]);
  const [deleteGroupId, setDeleteGroupId] = useState();

  //selector
  const myGroups = useSelector(getMyGroups);
  const allGroups = useSelector(getGroups)
  const isLoading = useSelector(getIsLoading);
  const isFormSubmitted = useSelector(getIsFormSubmitted);
  const isFormSubmitting = useSelector(getIsFormSubmitting);
  const role = useSelector(getRole);

  useEffect(() => {
    role !== 'teacher' ? dispatch(fetchGroups()) : dispatch(fetchMyGroups())
  }, [dispatch]);

  useEffect(() => {
    allGroups?.length && setGroups(allGroups)
  }, [allGroups]);

  useEffect(() => {
    myGroups?.length && setGroups(myGroups)
  }, [myGroups]);

  //closing group create/update form once form successfully submitted in mobile view
  useEffect(() => {
    isFormSubmitted && setShowRightPanelOnMobile(false);
  }, [isFormSubmitted]);

  //onclick handler
  const onEdit = (group) => {
    history.push({ pathname: '/group', state: { group } });
  };

  const deleteConfirmed = () => {
    dispatch(deleteGroup({ id: deleteGroupId }));
    setShow(false);
  };

  const onClickGroup = (group) => {
    dispatch(setGroup(group._id, group.name));
    history.push('/subject');
  };

  const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
  const onClickToggleRightPanel = () => {
    setShowRightPanelOnMobile(!showRightPanelOnMobile);
  };

  return (
    <Flex justifyContent="space-between">
      <ContentBox blur={showRightPanelOnMobile}>
        <Flex mb={3}>
          <IconGroup stroke={"#138BFC"} fill={"#138BFC"} height="28" width="28" />
          <Box ml={3} className="page-title">
            Groups
          </Box>
        </Flex>
        <FlexWrap>
          {isLoading ? (
            <GroupLoader />
          ) : (
              <GroupList
                groups={groups}
                edit={(group) => {
                  onEdit(group);
                }}
                remove={(id) => {
                  setDeleteGroupId(id);
                  setShow(true);
                }}
                onClick={(group) => onClickGroup(group)}
                showEdit={role !== 'student' && role !== 'teacher'}
                showDelete={role !== 'student' && role !== 'teacher'}
              />
            )}
          <Popup
            show={show}
            cancel={() => setShow(false)}
            confirm={deleteConfirmed}
          />
        </FlexWrap>
      </ContentBox>
      {/* <Box> */}
      <RightPanel>
        {
          role === ROLE.teacher ? 'Coming soon' :
            <Form isFormSubmitting={isFormSubmitting} />}
      </RightPanel>
      <MobileRightPanel
        title="Group Creation"
        show={showRightPanelOnMobile ? 'visible' : 'hide'}
      >
        {
          role === ROLE.teacher ? 'Coming soon' :
            <Form isFormSubmitting={isFormSubmitting} />}
      </MobileRightPanel>
      {/* </Box> */}
      {!showRightPanelOnMobile && (
        <Button
          name={<AddIcon />}
          title={'Create'}
          className={'mob-primary-button-form-'}
          onClick={onClickToggleRightPanel}
        />
      )}
      {showRightPanelOnMobile && (
        <Button
          name={<Close />}
          title={'Close'}
          className={'mob-primary-button-form-close'}
          onClick={onClickToggleRightPanel}
        />
      )}
    </Flex>
  );
};

export default Group;
