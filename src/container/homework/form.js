import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateGroup, createGroup, fetchMyGroups } from '../../action/group';
import { Form } from '../../components/homework';
import 'react-datepicker/dist/react-datepicker.css';
import { getMyGroups } from '../../reducer/group';
import { createHomework, updateHomework } from '../../action/homework';

const GroupForm = () => {
  const dispatch = useDispatch();

  const myGroups = useSelector(getMyGroups)

  useEffect(() => {
    dispatch(fetchMyGroups())
  }, [])

  const submit = (data) => {
    data.isUpdate
      ? dispatch(updateHomework({ id: data.id, payload: data.payload }))
      : dispatch(createHomework({ payload: data.payload }))
  };

  return <Form groups={myGroups} onSubmit={(data) => submit(data)} />;
};

export default GroupForm;
