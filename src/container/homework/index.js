import React, { useEffect, useState, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import moment from 'moment';
import { Nav, Card } from 'react-bootstrap';

import Form from './form';

import { Flex, Box } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import { MobileRightPanel } from '../layout/mobile';
import Popup from '../../components/asset/popup';
//action
import { fetchMyHomeworks, deleteHomework } from '../../action/homework';
import { setGroup, setSubject, setLesson, setTopic } from '../../action/common';

//reducer
import {
  getHomeworks,
  getIsHomeworkLoading,
  getIsError,
  getIsFormSubmiting, getIsFormSubmitted, getIsFormFailed,
} from '../../reducer/homework';
import { getRole, getUser } from '../../reducer/account';

import '../../assets/css/group.css';
import { Button } from '../../components/asset/button';
import { ROLE } from '../../utils/common';
import { Homework } from '../../components/homework';
import { isStudent, isTeacher } from '../../utils/helper';
import { Span } from '../../components/asset/elements';
import { LiveIcon } from '../../components/asset/icons/live';
import { AddIcon } from '../../components/asset/icons/add';
import { Close } from '../../components/asset/icons/close';
import { Back } from '../../components/asset/icons/back';

const Homeworks = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation()

  const [page = '', setPage] = useState('upcoming');
  const [show, setShow] = useState(false);
  const [homeworks, setHomeworks] = useState([]);
  const [deleteHomeworkId, setDeleteHomeworkId] = useState('');

  // //selector
  const loggedInUserRole = useSelector(getRole);
  const user = useSelector(getUser);
  const homeworksFromStore = useSelector(getHomeworks);
  const isLoading = useSelector(getIsHomeworkLoading);
  const isError = useSelector(getIsError);
  const isFormSubmitted = useSelector(getIsFormSubmitted);

  useEffect(() => {
    dispatch(fetchMyHomeworks())
  }, [dispatch]);

  useEffect(() => {
    if (isFormSubmitted) {
      dispatch(fetchMyHomeworks())
    }
  }, [isFormSubmitted])

  const isCurrentTimeInBetweenStartAndEndOfLive = (homework) => {
    return (moment(new Date()).isAfter(homework.assignedDate) && moment(new Date()).isBefore(homework.dueDate))
  }

  useEffect(() => {
    setHomeworks({
      upcoming: homeworksFromStore?.filter(homework => (moment(homework.assignedDate, 'YYYY-MM-DD').isSameOrAfter(moment().format('YYYY-MM-DD')) || isCurrentTimeInBetweenStartAndEndOfLive(homework))),
      previous: homeworksFromStore?.filter(homework => moment(homework.dueDate, 'YYYY-MM-DD').isBefore(moment().format('YYYY-MM-DD')))
    })
  }, [homeworksFromStore]);

  // //closing group create/update form once form successfully submitted in mobile view
  // useEffect(() => {
  //   isFormSubmitted && setShowRightPanelOnMobile(false);
  // }, [isFormSubmitted]);

  const onEdit = (e, homework) => {
    e.stopPropagation()
    history.push({ pathname: '/homework', state: { homework, isUpdate: true } });
  };

  const deleteConfirmed = () => {
    dispatch(deleteHomework({ id: deleteHomeworkId }));
    setShow(false);
  };

  const onClickTopic = (topic, lesson, subject) => {
    dispatch(setSubject(subject.id, subject.name));
    dispatch(setLesson(lesson.id, lesson.name));
    dispatch(setTopic(topic.id, topic.name));
    history.push('/topic/study');
  };

  // const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
  // const onClickToggleRightPanel = () => {
  //   setShowRightPanelOnMobile(!showRightPanelOnMobile);
  // };


  const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
  const onClickToggleRightPanel = () => {
    setShowRightPanelOnMobile(!showRightPanelOnMobile);
  };

  const back = () => {
    console.log(location);
  }

  return (
    <Flex justifyContent="space-between">
      <ContentBox blur={showRightPanelOnMobile}>
        <Flex mb={2}>
          <Box>
            <LiveIcon widht="34px" height="34px" />
          </Box>
          <Box m={1} ml={2} className="page-title">
            Homework
          </Box>
          <Box mt={1} className={"float-right"} onClick={back}>
            <Back />
          </Box>
        </Flex>
        <Card className="live-session-nav-card">
          <Card.Header className="card-header">
            <Nav variant="tabs" defaultActiveKey="#first">
              <Nav.Item className="nav-item">
                <Flex>
                  <Nav.Link
                    onClick={() => setPage('upcoming')}
                    className={page === 'upcoming' ? 'nav-link-active ' : ''}
                  >
                    {' '}
                    Upcoming Homework{' '}
                    <Span
                      text={homeworks?.upcoming?.length}
                      className="notification-counts"
                    />
                  </Nav.Link>
                  <Box m={1} pl={2}></Box>
                </Flex>
              </Nav.Item>
              <Nav.Item className="nav-item">
                <Nav.Link
                  onClick={() => setPage('completed')}
                  className={page === 'completed' ? 'nav-link-active ' : ''}
                >
                  {' '}
                  Completed Homeworks
                  <Span
                    text={homeworks?.previous?.length}
                    className="notification-counts"
                  />
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </Card.Header>
          <Card.Body>
            {page === 'upcoming' && (
              <Fragment>
                <Card.Text>
                  <Box className="nav-card-content">
                    <Homework
                      tab={page}
                      color={true}
                      homeworks={homeworks?.upcoming}
                      showEdit={isTeacher(loggedInUserRole)}
                      showRemove={isTeacher(loggedInUserRole)}
                      edit={onEdit}
                      remove={(e, id) => {
                        e.stopPropagation()
                        setDeleteHomeworkId(id);
                        setShow(true);
                      }}
                      onClickTopic={onClickTopic}
                    />
                  </Box>
                </Card.Text>
              </Fragment>
            )}
            {page === 'completed' && (
              <Fragment>
                <Card.Text>
                  <Box className="nav-card-content">
                    <Homework
                      tab={page}
                      color={false}
                      homeworks={homeworks?.previous}
                    />
                  </Box>
                </Card.Text>
              </Fragment>
            )}
          </Card.Body>
        </Card>
        <Popup
          show={show}
          cancel={() => setShow(false)}
          confirm={deleteConfirmed}
        />
      </ContentBox>
      <RightPanel>
        {
          isStudent(loggedInUserRole) ? 'Coming soon'
            : <Form />
        }
      </RightPanel>
      <MobileRightPanel
        title="Homework Creation"
        show={showRightPanelOnMobile ? 'visible' : 'hide'}
      >
        {!isStudent(loggedInUserRole) && <Form />}
      </MobileRightPanel>
      {!showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
        <Button
          name={<AddIcon />}
          title={'Create'}
          className={'mob-primary-button-form-'}
          onClick={onClickToggleRightPanel}
        />
      )}
      {showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
        <Button
          name={<Close />}
          title={'Close'}
          className={'mob-primary-button-form-close'}
          onClick={onClickToggleRightPanel}
        />
      )}
    </Flex>
  );
};

export default Homeworks;
