import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { LeftNav } from '../../components/layout';
import { Box, Spacer } from '../../components/asset/wrapper';
import { LEFT_NAV_LIST } from '../../utils/common';
import { getRole } from '../../reducer/account';
import { Eduzy } from '../../components/asset/icons/eduzy'
import '../../assets/css/left-nav.css';

const LeftNavigation = () => {
  const role = useSelector(getRole);
  const history = useHistory();
  const pathName = history.location.pathname;

  return (
    <Box className="nav-container">
      <Box>
        <Box p={3} title="Eduzy">
          <Eduzy width="160" height="63" />
          <Spacer pb={1} />
          <hr />
        </Box>
        <Box className="nav-content">
          <LeftNav
            LEFT_NAV_LIST={LEFT_NAV_LIST}
            role={role}
            pathName={pathName}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default LeftNavigation;
