/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { LeftNav } from '../../../components/layout';
import { Box, Spacer, Flex } from '../../../components/asset/wrapper';
import { LEFT_NAV_LIST } from '../../../utils/common';
import { getRole } from '../../../reducer/account';
import OurLogo from '../../../assets/images/logo.svg';
import '../../../assets/css/mobile.css';
import { Eduzy } from '../../../components/asset/icons/eduzy';
import { Close } from '../../../components/asset/icons/close';

function useOutsideAlerter(handleNavBar, commentRef) {
  function handleClickOutside(event) {
    if (
      commentRef &&
      !!commentRef.current &&
      !commentRef.current.contains(event.target)
    ) {
      handleNavBar();
    }
  }

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => document.removeEventListener('mousedown', handleClickOutside);
  });
}

const MobileLeftNav = (props) => {
  const role = useSelector(getRole);
  const history = useHistory();
  const pathName = history.location.pathname;
  const ref = React.createRef();

  useOutsideAlerter(props.closeNavBar, ref);

  return (
    <Box ref={ref} className={`mobile-nav-container ${props.className}`}>
      <Box>
        <Box p={3}>
          <Flex>
            {/* <img src={OurLogo} alt="logo" width="140px" height="58px" /> */}
            <Eduzy width="160" height="55" />
            <Box onClick={props.closeNavBar}><Close fill="#000000" /></Box>
          </Flex>
          <Spacer pb={1} />
          <hr />
        </Box>
        <Box className="nav-content">
          <LeftNav
            LEFT_NAV_LIST={LEFT_NAV_LIST}
            role={role}
            pathName={pathName}
            closeNavBar={props.closeNavBar}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default MobileLeftNav;
