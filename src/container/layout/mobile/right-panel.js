import React from 'react';
import { Box } from '../../../components/asset/wrapper';
import '../../../assets/css/mobile.css';

export default (props) => {
  const { children, show } = props;
  return (
    <Box p={4} width="75%" className={`mobile-right-panel ${show}`}>
      {children}
    </Box>
  );
};
