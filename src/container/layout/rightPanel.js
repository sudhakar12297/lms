import React from 'react';
import { Box } from '../../components/asset/wrapper';
import { RightPanel } from '../../components/layout';
import '../../assets/css/layout.css';

export default (props) => {
  const { title, children, bg, className } = props;
  return (
    <Box p={4} width="25%" className={"right-panel float-right " + className} bg={bg}>
      <RightPanel title={title}>{children}</RightPanel>
    </Box>
  );
};
