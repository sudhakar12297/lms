import React from 'react';
import { useDispatch } from 'react-redux';
import { createLesson, updateLesson } from '../../action/lesson';
import { Form } from '../../components/lesson';
import 'react-datepicker/dist/react-datepicker.css';

const LessonForm = (props) => {
  const { isFormSubmitting = false } = props;
  const dispatch = useDispatch();

  const submit = (data) => {
    const { isUpdate, payload, subjectId, id } = data;
    !isUpdate
      ? dispatch(createLesson({ payload, subjectId }))
      : dispatch(updateLesson({ id, payload, subjectId }));
  };

  return <Form onSubmit={(data) => submit(data)} isFormSubmitting={isFormSubmitting} />;
};

export default LessonForm;
