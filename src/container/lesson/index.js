import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import { Box, Flex } from '../../components/asset/wrapper';
import Form from './form';
import '../../assets/css/chapter.css';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import Popup from '../../components/asset/popup';

import { Lesson, LessonLoader } from '../../components/lesson';

import { ROLE } from '../../utils/common';

import { fetchLessonsBySubject, deleteLesson } from '../../action/lesson';
import { deleteTopic } from '../../action/topic';
import { setTopic, setSubject } from '../../action/common';

import { getRole } from '../../reducer/account';
import { getSelectedSubject, getSelectedLesson, getSelectedGroup } from '../../reducer/common';
import { getLessons, getIsLoading, getIsFormSubmitting } from '../../reducer/lesson';
import { isStudent, isTeacher } from '../../utils/helper';
import { getMySubjects } from '../../reducer/subject';
import { Button } from '../../components/asset/button';
import { MobileRightPanel } from '../layout/mobile';
import { IconSubject } from '../../components/asset/icons';


const LessonContainer = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const loggedInUserRole = useSelector(getRole);
  const selectedGroup = useSelector(getSelectedGroup);
  const subjectId = useSelector(getSelectedSubject).id;
  const selectedSubject = useSelector(getSelectedSubject);
  const lessonId = useSelector(getSelectedLesson).id;
  const lessons = useSelector(getLessons);
  const isLoading = useSelector(getIsLoading);
  const isFormSubmitting = useSelector(getIsFormSubmitting);
  const mySubjects = useSelector(getMySubjects);

  const [subjects, setSubjects] = useState([])

  useEffect(() => {
    dispatch(fetchLessonsBySubject({ id: subjectId }));
  }, [dispatch]);

  //grouping subjects by groupid to list on the rightpanel for quick switch
  useEffect(() => {
    if (mySubjects?.length) {
      const subjectsGroupByGroupId = _.groupBy(mySubjects, 'groupId')
      setSubjects(subjectsGroupByGroupId[selectedGroup.id])
    }
  }, [mySubjects])

  const onEditLesson = (lesson) => {
    history.push({
      pathname: '/lesson',
      state: { isUpdate: true, lesson },
    });
  };

  const [showLessonDeletePopup, setShowLessonDeletePopup] = useState(false);
  const [deleteLessonId, setDeleteLessonId] = useState();

  const onDeleteLesson = (id) => {
    setDeleteLessonId(id);
    setShowLessonDeletePopup(true);
  };

  const lessonDeleteConfirmed = () => {
    dispatch(deleteLesson({ id: deleteLessonId, subjectId }));
    setShowLessonDeletePopup(false);
  };

  const onEditTopic = (topic, lesson) => {
    history.push({
      pathname: '/topic/create',
      state: { isUpdate: true, topicId: topic.id, lesson },
    });
  };

  const [showTopicDeletePopup, setShowTopicDeletePopup] = useState(false);
  const [deleteTopicId, setDeleteTopicId] = useState();

  const onDeleteTopic = (id) => {
    setDeleteTopicId(id);
    setShowTopicDeletePopup(true);
  };

  const topicDeleteConfirmed = () => {
    dispatch(deleteTopic({ id: deleteTopicId, subjectId, lessonId }));
    setShowTopicDeletePopup(false);
  };

  const onClickTopic = (topic) => {
    dispatch(setTopic(topic.id, topic.name))
    history.push({ pathname: '/topic/study', state: { topic } })
  }

  const onClickSwitchSubject = (subject) => {
    dispatch(setSubject(subject._id, subject.name))
    dispatch(fetchLessonsBySubject({ id: subject._id }));
  }

  const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
  const onClickToggleRightPanel = () => {
    setShowRightPanelOnMobile(!showRightPanelOnMobile);
  };

  return (
    <Flex>
      <ContentBox blur={showRightPanelOnMobile}>
        <Flex mb={2}>
          <Box>
            <IconSubject widht="34px" height="34px" fill="#138BFC" />
          </Box>
          <Box m={1} ml={2} className="page-title">
            {`Subject - ${selectedSubject.name}`}
          </Box>
        </Flex>
        {isLoading ?
          <LessonLoader />
          :
          <Lesson
            lessons={lessons}
            edit={(lesson) => {
              onEditLesson(lesson);
            }}
            remove={(id) => onDeleteLesson(id)}
            editTopic={(topic, lesson) => {
              onEditTopic(topic, lesson);
            }}
            removeTopic={(id) => onDeleteTopic(id)}
            showEdit={
              !isStudent(loggedInUserRole)
            }
            showDelete={
              !isStudent(loggedInUserRole)
            }
            onClickTopic={(topic) => onClickTopic(topic)}
          />
        }
      </ContentBox>
      <RightPanel>
        {!isStudent(loggedInUserRole) ? (
          <Form isFormSubmitting={isFormSubmitting} />
        ) :
          (
            <Box>
              <Box pl={3} m={1} className="study-topics-total-info">
                Subjects: {subjects?.length} (Quick Switch)
              </Box>
              <Box m={2}>
                {
                  subjects?.map((subject, index) => {
                    return (
                      <Flex onClick={() => { onClickSwitchSubject(subject) }} p={3} m={1} mb={2} className={`study-topics ${subjectId === subject._id && 'topic-active'}`}>
                        <Box mr={3} className="topic-card-topic-box">{index + 1}</Box>
                        {subject.name}
                        {/* <Box className={"timer"}> 16.00 </Box> */}
                      </Flex>
                    )
                  })
                }
              </Box>
            </Box>
          )}
      </RightPanel>
      <MobileRightPanel
        title="Lesson Creation"
        show={showRightPanelOnMobile ? 'visible' : 'hide'}
      >
        {!isStudent(loggedInUserRole) &&
          <Form isFormSubmitting={isFormSubmitting} />}
      </MobileRightPanel>
      <Popup
        show={showLessonDeletePopup}
        cancel={() => setShowLessonDeletePopup(false)}
        confirm={lessonDeleteConfirmed}
      />
      <Popup
        show={showTopicDeletePopup}
        cancel={() => setShowTopicDeletePopup(false)}
        confirm={topicDeleteConfirmed}
      />
      {!showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
        <Button
          name={'+'}
          title={'Create'}
          className={'mob-primary-button-form-'}
          onClick={onClickToggleRightPanel}
        />
      )}
      {showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
        <Button
          name={'x'}
          title={'Close'}
          className={'mob-primary-button-form-close'}
          onClick={onClickToggleRightPanel}
        />
      )}
    </Flex>
  );
};

export default LessonContainer;
