import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Form } from '../../components/live';
import 'react-datepicker/dist/react-datepicker.css';

import { createLive, updateLive } from '../../action/live';

const LiveForm = (props) => {
    const { groups } = props
    const dispatch = useDispatch()

    const submit = (data) => {
        data.isUpdate
            ? dispatch(
                updateLive({
                    id: data.id,
                    payload: data.payload,
                })
            )
            : dispatch(
                createLive({ payload: data.payload })
            );
    };

    return <Form groups={groups} onSubmit={(data) => submit(data)} />;
};

export default LiveForm;
