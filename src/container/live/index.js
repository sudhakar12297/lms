import React, { useState, useEffect, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import moment from 'moment';
import { Box, Flex } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import { Card, Nav } from 'react-bootstrap';
import '../../assets/css/nav-card.css';
import '../../assets/css/live-session.css';
import { Span } from '../../components/asset/elements';
import { LiveIcon } from '../../components/asset/icons/live';
import Popup from '../../components/asset/popup';

import Form from './form';
import { Live } from '../../components/live';

import { getRole, getUser } from '../../reducer/account';
import { getLives } from '../../reducer/live';
import { fetchLivesByUserId, deleteLive, fetchLivesByGroups } from '../../action/live';
import { ROLE } from '../../utils/common';
import { fetchMyGroups } from '../../action/group';
import { getMyGroups } from '../../reducer/group';
import { MobileRightPanel } from '../layout/mobile';
import { isStudent } from '../../utils/helper';
import { Button } from '../../components/asset/button';
import { Close } from '../../components/asset/icons/close';
import { AddIcon } from '../../components/asset/icons/add';

const Lives = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const loggedInUserRole = useSelector(getRole);
  const user = useSelector(getUser);
  const myGroups = useSelector(getMyGroups);
  const liveSessions = useSelector(getLives);

  const [lives, setLives] = useState({
    upcoming: [],
    previous: []
  })

  const [page = '', setPage] = React.useState('upcoming');
  const [show, setShow] = useState(false);
  const [deleteLiveId, setDeleteLiveId] = useState();

  const isCurrentTimeInBetweenStartAndEndOfLive = (live) => {
    const endTime = moment(live?.date).add(live.duration, 'minutes')
    return (moment(new Date()).isAfter(live?.date) && moment(new Date()).isBefore(endTime))
  }

  useEffect(() => {
    loggedInUserRole === ROLE.teacher && dispatch(fetchLivesByUserId());
    loggedInUserRole === ROLE.student && dispatch(fetchLivesByGroups({ groupId: user.groupId[0] }))
    dispatch(fetchMyGroups());
  }, [dispatch]);

  useEffect(() => {
    setLives({
      upcoming: liveSessions.filter(live => (moment(live.date).isSameOrAfter(new Date()) || isCurrentTimeInBetweenStartAndEndOfLive(live))),
      previous: liveSessions.filter(live => moment(moment(live?.date).add(live.duration, 'minutes')).isBefore(new Date()))
    })
  }, [liveSessions]);

  const onEdit = (e, live) => {
    e.stopPropagation();
    history.push({ pathname: '/live', state: { live, isUpdate: true } });
  };

  const deleteConfirmed = (e) => {
    dispatch(deleteLive({ id: deleteLiveId }));
    setShow(false);
  };

  const onClickLive = (e, live) => {
    e.stopPropagation();
    history.push({ pathname: '/live/video', state: { live } });
  };

  const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
  const onClickToggleRightPanel = () => {
    setShowRightPanelOnMobile(!showRightPanelOnMobile);
  };

  return (
    <Flex className="group-container">
      <ContentBox blur={showRightPanelOnMobile}>
        <Flex mb={2}>
          <Box>
            <LiveIcon widht="34px" height="34px" />
          </Box>
          <Box m={1} ml={2} className="page-title">
            Live
          </Box>
        </Flex>
        <Card className="live-session-nav-card">
          <Card.Header className="card-header">
            <Nav variant="tabs" defaultActiveKey="#first">
              <Nav.Item className="nav-item">
                <Flex>
                  <Nav.Link
                    onClick={() => setPage('upcoming')}
                    className={page === 'upcoming' ? 'nav-link-active ' : ''}
                  >
                    {' '}
                    Upcoming Sessions{' '}
                    <Span
                      text={lives.upcoming?.length}
                      className="notification-counts"
                    />
                  </Nav.Link>
                  <Box m={1} pl={2}></Box>
                </Flex>
              </Nav.Item>
              <Nav.Item className="nav-item">
                <Nav.Link
                  onClick={() => setPage('completed')}
                  className={page === 'completed' ? 'nav-link-active ' : ''}
                >
                  {' '}
                  Completed Sessions
                  <Span
                    text={lives.previous?.length}
                    className="notification-counts"
                  />
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </Card.Header>
          <Card.Body>
            {page === 'upcoming' && (
              <Fragment>
                <Card.Text>
                  <Box className="nav-card-content">
                    <Live
                      tab={page}
                      color={true}
                      lives={lives?.upcoming}
                      groups={myGroups}
                      onClick={(e, live) => {
                        onClickLive(e, live);
                      }}
                      edit={onEdit}
                      remove={(e, id) => {
                        e.stopPropagation();
                        setShow(true);
                        setDeleteLiveId(id);
                      }}
                      showEdit={loggedInUserRole === ROLE.teacher}
                      showRemove={loggedInUserRole === ROLE.teacher}
                    />
                  </Box>
                </Card.Text>
              </Fragment>
            )}
            {page === 'completed' && (
              <Fragment>
                <Card.Text>
                  <Box className="nav-card-content">
                    <Live
                      tab={page}
                      color={false}
                      lives={lives?.previous}
                      groups={myGroups}
                      onClick={(e, live) => {
                        onClickLive(e, live);
                      }}
                      edit={onEdit}
                      remove={(e, id) => {
                        e.stopPropagation();
                        setShow(true);
                        setDeleteLiveId(id);
                      }}
                      showEdit={false}
                      showRemove={loggedInUserRole === ROLE.teacher}
                      tag={'Completed Session'}
                    />
                  </Box>
                </Card.Text>
              </Fragment>
            )}
          </Card.Body>
        </Card>
      </ContentBox>
      <RightPanel>
        {!isStudent(loggedInUserRole) ? (
          <Form groups={myGroups} />
        ) : (
            'Will we show you something else soon'
          )}
      </RightPanel>
      <MobileRightPanel
        title="Live Creation"
        show={showRightPanelOnMobile ? 'visible' : 'hide'}
      >
        {!isStudent(loggedInUserRole) && <Form groups={myGroups} />}
      </MobileRightPanel>
      <Popup
        show={show}
        cancel={() => setShow(false)}
        confirm={deleteConfirmed}
      />
      {!showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
        <Button
          name={<AddIcon />}
          title={'Create'}
          className={'mob-primary-button-form-'}
          onClick={onClickToggleRightPanel}
        />
      )}
      {showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
        <Button
          name={<Close />}
          title={'Close'}
          className={'mob-primary-button-form-close'}
          onClick={onClickToggleRightPanel}
        />
      )}
    </Flex>
  );
};

export default Lives;
