import React, { useState, useEffect, Fragment } from 'react';
import { Box, Flex } from '../../components/asset/wrapper';
import { Button } from '../../components/asset/button';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import { Card, Nav } from 'react-bootstrap';
import { Span } from '../../components/asset/elements';
import Editor from '../../components/editor';
import '../../assets/css/live-session.css';

let api;

export default function VideoConference(props) {
  const [page = '', setPage] = React.useState('notes');
  const [loading, setLoading] = useState(true);
  const containerStyle = {
    width: '100%',
    height: '400px',
  };

  const jitsiContainerStyle = {
    display: loading ? 'none' : 'block',
    width: '100%',
    height: '400px',
  };

  function startConference() {
    try {
      const domain = 'meet.eduzy.co';
      const options = {
        roomName: 'roomName',
        height: 400,
        parentNode: document.getElementById('jitsi-container'),
        interfaceConfigOverwrite: {
          filmStripOnly: false,
          SHOW_BRAND_WATERMARK: false,
          SHOW_JITSI_WATERMARK: false,
          SHOW_WATERMARK_FOR_GUESTS: false,
        },
        configOverwrite: {
          disableSimulcast: false,
        },
      };

      api = new window.JitsiMeetExternalAPI(domain, options);
      api.addEventListener('videoConferenceJoined', () => {
        startRecording();
        setLoading(false);
      });
      api.on('readyToClose', () => {
        stopRecording();
        window.location.replace('/live');
      });
    } catch (error) {
      console.error('Failed to load Jitsi API', error);
    }
  }
  const startRecording = () => {
    api.executeCommand('startRecording', {
      mode: 'file',
      dropboxToken: `${process.env.REACT_APP_DROPBOX_TOKEN}`,
      shouldShare: false,
    });
  };

  const stopRecording = () => {
    api.executeCommand('stopRecording', 'file');
  };
  useEffect(() => {
    if (window.JitsiMeetExternalAPI) {
      setLoading(false);
      startConference();
    } else alert('Jitsi Meet API script not loaded');
  }, []);

  return (
    <Flex className="video-session">
      <ContentBox>
        <Box style={containerStyle}>
          {loading && <h2>Loading Screen...</h2>}
          <Box id="jitsi-container" style={jitsiContainerStyle} />
          {!loading && (
            <>
              <Flex m={2} mr={0} className="float-right">
                <Box m={2}>
                  <Button
                    onClick={startRecording}
                    name="Start Recording"
                    className="primary-button-sm"
                  />
                </Box>
                <Box m={2}>
                  <Button
                    onClick={stopRecording}
                    name="Stop Recording"
                    className="primary-button-sm"
                  />
                </Box>
              </Flex>
            </>
          )}
        </Box>
      </ContentBox>
      <RightPanel bg="#F4F6FB">
        <Card className="live-session-nav-card">
          <Card.Header className="card-header">
            <Nav variant="tabs" defaultActiveKey="#first">
              <Nav.Item className="nav-item">
                <Flex>
                  <Nav.Link
                    onClick={() => setPage('notes')}
                    className={page === 'notes' ? 'nav-link-active ' : ''}
                  >
                    {' '}
                    Take Notes{' '}
                  </Nav.Link>
                  <Box m={1} pl={2}></Box>
                </Flex>
              </Nav.Item>
              {/* <Nav.Item className="nav-item">
                <Nav.Link
                  onClick={() => setPage('attendee')}
                  className={page === 'attendee' ? 'nav-link-active ' : ''}
                >
                  {' '}
                  Attendee
                  <Span text="6" className="notification-counts" />
                </Nav.Link>
              </Nav.Item> */}
            </Nav>
          </Card.Header>
          <Card.Body className="editor-live-session">
            {page === 'notes' && (
              <Fragment>
                <Card.Text>
                  <Box className="nav-card-content">
                    <Editor limited={true} onChange={(value) => { }} />
                    <Box className="editor-tip">
                      * Notes will save automatically
                    </Box>
                  </Box>
                </Card.Text>
              </Fragment>
            )}
            {page === 'attendee' && (
              <Fragment>
                <Card.Text>
                  <Box className="nav-card-content"></Box>
                </Card.Text>
              </Fragment>
            )}
          </Card.Body>
        </Card>
      </RightPanel>
    </Flex>
  );
}
