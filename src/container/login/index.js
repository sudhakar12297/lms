import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { isValidFormFieldData, getCookie } from '../../utils/helper';

//action
import { login } from '../../action/account';
//reducer
import { getIsLoading, getIsError, getIsLoggedIn } from '../../reducer/account';
import { Button } from '../../components/asset/button';
import { Head, Label, P } from '../../components/asset/elements';
import Banner from '../../assets/images/loginbg.svg';
// import styles from '../../assets/css/mystyle.module.css';
import { Input } from '../../components/asset/form-inputs';
import { Spacer, Box, FlexWrap } from '../../components/asset/wrapper';
import Loader from '../../components/asset/loader';

import '../../assets/css/login-form.css';

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //get redux store data
  const isError = useSelector(getIsError);
  const isLoading = useSelector(getIsLoading);
  const isLoggedIn = getCookie('user');

  //local state
  const [formData, setFormData] = useState();
  const [errMsg, setErrMsg] = useState();

  useEffect(() => {
    isError && setErrMsg('Invalid username/password');
  }, [isError]);

  //onchange function for formData
  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
    setErrMsg('');
  };

  //form submit function
  const onLogin = (e) => {
    e.preventDefault();
    if (isValidFormFieldData([formData?.username, formData?.password])) {
      dispatch(login(formData?.username, formData?.password));
    } else {
      setErrMsg('Please fill the login data');
    }
  };

  return (
    <FlexWrap className="login-main-container">
      <Box className="login-container">
        {isLoggedIn && history.push('/dashboard')}
        <form className="login-form-container">
          <Head name="Hey, Welcome" className="form-title-login" />
          <Label name="User Id" className="form-label-login" />
          <Input
            seq="1"
            type="text"
            name="username"
            value={formData?.username}
            onChange={(e) => onChange(e)}
            placeholder="Enter User id"
          />
          <Label name="Password" className="form-label-login" />
          <Input
            seq="2"
            type="password"
            name="password"
            value={formData?.password}
            onChange={(e) => onChange(e)}
            placeholder="Enter Password"
          />
          <Spacer mb={2} />
          <p>{errMsg && errMsg}</p>
          <Button name="Login" onClick={(e) => onLogin(e)} disabled={isLoading} loader={true} />
          <P name="Forgot Password?" className="forgot-password" />
        </form>
      </Box>
      <Box className="login-img-container">
        <img className="img" src={Banner} alt="banner" />
      </Box>
    </FlexWrap>
  );
};

export default Login;
