import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import DownloadLink from "react-download-link";

import { Box, Flex } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';

import { getIsLoading, getIsError, getMyNotes } from '../../reducer/note';
import { fetchMyNotes } from '../../action/note';
import { Span } from '../../components/asset/elements';
import { Card } from '../../components/asset/cards';
import { NoDataAvailable } from '../../components/asset/noData';
import { Button } from '../../components/asset/button';

const MyNotes = () => {
    const dispatch = useDispatch();

    const isLoading = useSelector(getIsLoading);
    const isError = useSelector(getIsError);
    const myNotes = useSelector(getMyNotes)

    useEffect(() => {
        dispatch(fetchMyNotes());
    }, [dispatch]);

    return (
        <Flex>
            <ContentBox>
                {
                    (myNotes && myNotes?.length) ?
                        (
                            myNotes.map(note => {
                                return (
                                    <Card>
                                        <Box m={2} p={2} pl={0}><Span text={note.topicName || '- - -'} className="topic-header" /></Box>
                                        <div style={{ paddingLeft: '20px' }} dangerouslySetInnerHTML={{ __html: note.notes }} />
                                        <DownloadLink
                                            label="Download Notes"
                                            filename={`${note.topicName}.txt`}
                                            exportFile={() => note.notes}
                                        />
                                    </Card>
                                )
                            })

                        )
                        : <NoDataAvailable text={`No Note(s) asked yet`} />
                }
            </ContentBox>
            <RightPanel>
                coming soon
            </RightPanel>
        </Flex>
    );
};

export default MyNotes;
