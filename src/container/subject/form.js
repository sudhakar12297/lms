import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Form from '../../components/subject/form';
import 'react-datepicker/dist/react-datepicker.css';

import { createSubject, updateSubject } from '../../action/subject';
import { getSelectedGroup } from '../../reducer/common';

const GroupForm = (props) => {
  const { isFormSubmitting = false } = props;
  const dispatch = useDispatch();

  const selectedGroup = useSelector(getSelectedGroup);

  const submit = (data) => {
    data.isUpdate
      ? dispatch(
        updateSubject({
          id: data.id,
          groupId: selectedGroup.id,
          payload: data.payload,
        })
      )
      : dispatch(
        createSubject({ groupId: selectedGroup.id, payload: data.payload })
      );
  };

  return <Form onSubmit={(data) => submit(data)} isFormSubmitting={isFormSubmitting} />;
};

export default GroupForm;
