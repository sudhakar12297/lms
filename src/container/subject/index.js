/* eslint-disable react/prop-types */
import React, { useEffect, useState, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import _ from 'lodash'

//action
import { fetchTeachersByGroup } from '../../action/account';
import { fetchSubjectsByGroups, deleteSubject, fetchMySubjects } from '../../action/subject';
import { setSubject, setGroup } from '../../action/common';
import { fetchGroupById } from '../../action/group';
//reducer
import { getRole, getTeachers, getIsAccountLoading, getUser } from '../../reducer/account';
import { getSubjects, getIsSubjectLoading, getIsFormSubmitting, getMySubjects } from '../../reducer/subject';
import { getGroup, getIsGroupLoading, getMyGroups } from '../../reducer/group';
import { getSelectedGroup } from '../../reducer/common';
//assets
import { Spacer, Flex, FlexWrap, Box } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import { Info, UserList, Card, UserListLoader, InfoCardLoader } from '../../components/asset/cards';
import Popup from '../../components/asset/popup';
import { Subject, SubjectLoader } from '../../components/subject';

import Form from './form';
import { ROLE } from '../../utils/common';

import '../../assets/css/group.css';
import { isTeacherOrStudent, isTeacher, isStudent } from '../../utils/helper';
import { NoDataAvailable } from '../../components/asset/noData';
import { LiveIcon } from '../../components/asset/icons/live';
import { MobileRightPanel } from '../layout/mobile';
import { Button } from '../../components/asset/button';
import { Close } from '../../components/asset/icons/close';
import { AddIcon } from '../../components/asset/icons/add';

const Subjects = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const loggedInUserRole = useSelector(getRole);
  const loggedInUser = useSelector(getUser);
  const teachers = useSelector(getTeachers);
  const group = useSelector(getGroup);
  const selectedGroup = useSelector(getSelectedGroup);
  const subjectsFromStore = useSelector(getSubjects);
  const mySubjects = useSelector(getMySubjects);
  const isSubjectLoading = useSelector(getIsSubjectLoading);
  const isAccountLoading = useSelector(getIsAccountLoading);
  const isGroupLoading = useSelector(getIsGroupLoading);
  const isFormSubmitting = useSelector(getIsFormSubmitting);
  const myGroups = useSelector(getMyGroups);

  const [subjects, setSubjects] = useState([])

  useEffect(() => {
    const id = selectedGroup.id
    isTeacherOrStudent(loggedInUserRole) ? dispatch(fetchMySubjects()) : dispatch(fetchSubjectsByGroups({ groupIds: [id] }));
    dispatch(fetchTeachersByGroup({ id }))
    !isTeacherOrStudent(loggedInUserRole) && dispatch(fetchGroupById({ id }))
  }, [dispatch]);

  useEffect(() => {
    setSubjects(subjectsFromStore)
  }, [subjectsFromStore]);

  useEffect(() => {
    if (mySubjects?.length) {
      const subjectsGroupByGroupId = _.groupBy(mySubjects, 'groupId')
      setSubjects(subjectsGroupByGroupId[selectedGroup.id])
    }
  }, [mySubjects])

  const [show, setShow] = useState(false);
  const [deleteSubjectId, setDeleteSubjectId] = useState();

  const onEdit = (subject) => {
    history.push({ pathname: '/subject', state: { subject, isUpdate: true } });
  };

  const deleteConfirmed = (id) => {
    dispatch(deleteSubject({ id: deleteSubjectId, groupId: selectedGroup.id }));
    setShow(false);
  };

  const onClickSubject = (subject) => {
    dispatch(setSubject(subject._id, subject.name));
    history.push('/lesson');
  };

  const onClickSwitchGroup = (group) => {
    dispatch(setGroup(group._id, group.name))
    dispatch(fetchMySubjects())
    dispatch(fetchTeachersByGroup({ id: group._id }))
  }

  const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
  const onClickToggleRightPanel = () => {
    setShowRightPanelOnMobile(!showRightPanelOnMobile);
  };

  return (
    <Flex className="group-container">
      <ContentBox blur={showRightPanelOnMobile}>
        <Flex mb={2}>
          <Box>
            <LiveIcon widht="34px" height="34px" />
          </Box>
          <Box m={1} ml={2} className="page-title">
            Subjects
          </Box>
        </Flex>
        <Box>
          <Box>
            <FlexWrap justifyContent="space-between">
              {loggedInUserRole !== ROLE.student &&
                loggedInUserRole !== ROLE.teacher && (
                  <Fragment>
                    <Card width="260px" bg="#138BFC">
                      {!isGroupLoading ? <Info
                        title={`${selectedGroup.name} Group`}
                        data={[
                          { label: 'Strength', data: group?.details?.studentsCount },
                          { label: 'Male', data: group?.details?.maleCount },
                          { label: 'Female', data: group?.details?.femaleCount },
                          { label: 'Subjects', data: subjects?.length },
                          { label: 'Teachers', data: group?.details?.teachersCount },
                        ]}
                      /> : <InfoCardLoader title={`${selectedGroup.name} Group`} />}
                    </Card>
                    <Spacer mr={2} />
                  </Fragment>
                )}
              <Card bg="#FFFFFF" width="480px" p={10}>
                {isAccountLoading ?
                  <UserListLoader title={`${selectedGroup.name} Group teacher's`} />
                  :
                  <UserList
                    title={`${selectedGroup.name} Group teacher's`}
                    users={teachers}
                  />
                }
              </Card>
            </FlexWrap>
            <Spacer pb={10} />
            {isSubjectLoading ?
              <SubjectLoader
                groupName={selectedGroup.name} />
              :
              <Subject
                groupName={selectedGroup.name}
                subjects={subjects}
                edit={(subject) => onEdit(subject)}
                remove={(id) => {
                  setDeleteSubjectId(id);
                  setShow(true);
                }}
                onClick={(subject) => onClickSubject(subject)}
                showEdit={
                  loggedInUserRole !== ROLE.student &&
                  loggedInUserRole !== ROLE.teacher
                }
                showDelete={
                  loggedInUserRole !== ROLE.student &&
                  loggedInUserRole !== ROLE.teacher
                }
              />
            }
          </Box>
        </Box>
      </ContentBox>
      <RightPanel>
        {!isStudent(loggedInUserRole) &&
          !isTeacher(loggedInUserRole) ?
          (
            <Form isFormSubmitting={isFormSubmitting} />
          ) :
          (
            isTeacher(loggedInUserRole) ?
              (
                <Box>
                  <Box pl={3} m={1} className="study-topics-total-info">
                    Groups: {myGroups?.length} (Quick Switch)
                  </Box>
                  <Box className="study-topics-wrapper" m={2}>
                    {
                      myGroups?.length ?
                        myGroups?.map((group, index) => {
                          return (
                            <Flex onClick={() => { onClickSwitchGroup(group) }} p={3} m={1} mb={2} className={`study-topics ${selectedGroup.id === group._id && 'topic-active'}`}>
                              <Box mr={3} className="topic-card-topic-box">{index + 1}</Box>
                              {group.name}
                              {/* <Box className={"timer"}> 16.00 </Box> */}
                            </Flex>
                          )
                        })
                        : <NoDataAvailable text='No Group(s) available' />
                    }
                  </Box>
                </Box>
              ) :
              ('Will we show you something else soon')
          )}
      </RightPanel>
      <MobileRightPanel
        title="Subject Creation"
        show={showRightPanelOnMobile ? 'visible' : 'hide'}
      >
        {!isStudent(loggedInUserRole) && <Form groups={myGroups} />}
      </MobileRightPanel>
      {!showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
        <Button
          name={<AddIcon />}
          title={'Create'}
          className={'mob-primary-button-form-'}
          onClick={onClickToggleRightPanel}
        />
      )}
      {showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
        <Button
          name={<Close />}
          title={'Close'}
          className={'mob-primary-button-form-close'}
          onClick={onClickToggleRightPanel}
        />
      )}
      <Popup
        show={show}
        cancel={() => setShow(false)}
        confirm={deleteConfirmed}
      />
    </Flex>
  );
};

export default Subjects;
