import React from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';
import Form from '../../components/test/create-question';
import { FlexWrap } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import { createQuestion } from '../../action/question';

export default () => {
    const dispatch = useDispatch();
    const location = useLocation()
    const { state } = location
    const { test = {} } = state || {};
    const onsubmit = (data) => {
        dispatch(createQuestion({ payload: data }));
    };

    return (
        <FlexWrap>
            <ContentBox>
                <Form
                    onSubmit={onsubmit}
                    test={test}
                />
            </ContentBox>
            <RightPanel>
                <p>will show you someting</p>
            </RightPanel>
        </FlexWrap>
    );
};
