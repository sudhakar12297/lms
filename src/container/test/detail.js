import React, { useState, Fragment, useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Nav from 'react-bootstrap/Nav'
import { Box, Flex, FlexWrap } from '../../components/asset/wrapper';
// import { Button } from '../asset/button'

import '../../assets/css/nav-card.css';
import '../../assets/css/test.css';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import { Input, CheckBox } from '../../components/asset/form-inputs';
import { ExamIcon } from '../../components/asset/icons/exam';
import { Button } from '../../components/asset/button';
import { Span } from '../../components/asset/elements';

import { fetchLessonsBySubject } from '../../action/lesson';
import { fetchQuestionsBySubject } from '../../action/question';
import { mapQuestions } from '../../action/test';
import { getLessons } from '../../reducer/lesson';
import { getQuestions } from '../../reducer/question';
import { getQuestionMapPayload } from '../../reducer/test';



const TestDetail = () => {
    const dispatch = useDispatch()
    const location = useLocation()
    const history = useHistory();
    const [formData, setFormData] = useState({});
    const { pathname, state } = location
    const { test = {} } = state || {};
    const { name, subjectId, lessonId, topicId, isPractice, start, timing, questions, _id: id } = test;
    const masterQuestions = useSelector(getQuestions);
    const lessons = useSelector(getLessons);
    const [checkedQuestions, setCheckedQuestions] = useState(questions);
    const [tab = '', setTab] = useState('questions');
    const [filteredQuestions, setFilteredQuestions] = useState([]);
    const questionMap = useSelector(getQuestionMapPayload)

    const onChange = (e) => {
        let _formData = {
            [e.target.name]: e.target.value
        };
        if (e.target.name == 'lessonId') {
            _formData['topicId'] = '';
        }
        setFormData({ ...formData, ..._formData });
    };

    const onClickQuestion = (event) => {
        const value = event.target.name;
        const checked = event.target.checked;
        if (checked) {
            setCheckedQuestions([...checkedQuestions, value]);
        } else {
            setCheckedQuestions(checkedQuestions.filter(questionId => questionId != value));
        }
    }

    // Redirect to Live pages once test questions are updated
    // useEffect(() => {
    //     const { success = false } = questionMap || {};
    //     if (success) {
    //         dispatch({ type: 'RESET_QUESION_MAP_RESPONSE' });
    //         history.push({ pathname: '/test', state: { currentPage: "live" } });
    //         history.goBack();
    //     }
    // }, [questionMap]);

    const applyFilter = (key, list, value) => {
        return list.filter(masterQuestion => masterQuestion[key] == value);
    }

    const redirectToAddQuestions = () => {
        history.push({ pathname: '/test/question/create', state: { test } });
    }

    useEffect(() => {
        if (formData?.lessonId) {
            setFilteredQuestions(applyFilter("lessonId", masterQuestions, formData?.lessonId));
        }
    }, [formData?.lessonId]);

    useEffect(() => {
        if (formData?.topicId) {
            setFilteredQuestions(applyFilter("topicId", masterQuestions, formData?.topicId));
        }
    }, [formData?.topicId]);

    useEffect(() => {
        if (formData?.searchKey) {
            const _filteredQuestions = filteredQuestions.filter(masterQuestion => masterQuestion.question.toLowerCase().includes(formData.searchKey.toLowerCase()));
            setFilteredQuestions(_filteredQuestions);
        } else {
            let newFilteredList = masterQuestions;
            if (formData?.lessonId) {
                newFilteredList = applyFilter("lessonId", newFilteredList, formData?.lessonId);
            }
            if (formData?.topicId) {
                newFilteredList = applyFilter("topicId", newFilteredList, formData?.topicId);
            }
            setFilteredQuestions(newFilteredList);
        }
    }, [formData?.searchKey]);

    const onSubmit = () => {
        dispatch(mapQuestions({ id, payload: { questions: checkedQuestions } }));
    }

    useEffect(() => {
        const _filteredQuestions = masterQuestions.filter(masterQuestion => {
            return ((lessonId && lessonId == masterQuestion.lessonId) || !lessonId) && ((topicId && topicId == masterQuestion.topicId) || !topicId)
        });
        setFilteredQuestions(_filteredQuestions);
    }, [masterQuestions]);

    useEffect(() => {
        dispatch(fetchLessonsBySubject({ id: subjectId }))
        dispatch(fetchQuestionsBySubject({ id: subjectId }));
    }, [dispatch])

    const getLessonOptions = () => {
        if (lessonId) {
            const [firstLesson = {}] = lessons.filter(lesson => lesson.id == lessonId);
            const { id = '', name = '' } = firstLesson;
            return <option value={id} selected={true}>{name}</option>
        } else if (!lessons.length) {
            return <option value={''} selected={true}>{'No option'}</option>
        } else {
            let _options = [<option value='' selected>Select Lesson</option>]
            return [..._options, ...lessons.map(lesson => <option value={lesson.id} selected={lesson.id == formData.lessonId}>{lesson.name}</option>)];
        }
    }

    const getTopicOptions = () => {
        let _lessonId = lessonId || formData.lessonId || '';
        if (_lessonId) {
            const [firstLesson = {}] = lessons.filter(lesson => lesson.id == _lessonId);
            const { topics = [] } = firstLesson;
            if (topicId) {
                const [firstTopic = {}] = topics.filter(topic => topic.id == topicId);
                const { id = '', name = '' } = firstTopic;
                return <option value={id} selected={true}>{name}</option>
            } else if (topics.length) {
                let _options = [<option value='' selected>Select Topic</option>]
                return [..._options, ...topics.map(topic => <option value={topic.id} selected={topic.id == formData.topicId}>{topic.name}</option>)];
            } else {
                return <option value={''} selected={true}>{'No option'}</option>
            }
        } else {
            return <option value={''} selected={true}>{'No option'}</option>
        }
    }

    const selectedQuestions = masterQuestions.filter(masterQuestion => checkedQuestions.includes(masterQuestion._id));
    return (
        <Flex className="study-page-wrapper">
            <ContentBox>
                <FlexWrap>
                    <Flex ml={2}>
                        <ExamIcon widht="40px" height="40px" />
                    </Flex>
                    <Flex m={2} className="page-title">
                        Add Questions
                    </Flex>
                    <Flex m={2} onClick={redirectToAddQuestions} className="float-right">
                        <Button name="+ Create Question" className="primary-button-sm" />
                    </Flex>
                </FlexWrap>
                <Box>
                    <Flex m={2} ml={0}>
                        <Box m={2} className="">
                            <select name="lessonId" id="chapter" onChange={onChange}>
                                {getLessonOptions()}
                            </select>
                        </Box>
                        <Box m={2} className="">
                            <select name="topicId" id="chapter-heading" onChange={onChange}>
                                {getTopicOptions()}
                            </select>
                        </Box>
                        <Box m={2}>
                            <Input className="text-input-search-question" placeholder="Search Questions" name="searchKey" value={formData?.searchKey} onChange={onChange} />
                        </Box>
                    </Flex>
                </Box>
                <Card className="nav-card">
                    <Card.Header className="nav-card-header">
                        <Nav variant="tabs" defaultActiveKey="#first" className={'nav-card-navbar'} >
                            <Nav.Item className={'nav-card-nav-item'}>
                                <Nav.Link onClick={() => setTab('questions')} className={tab === 'questions' ? 'nav-link-active' : ''}>Questions</Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={'nav-card-nav-item'}>
                                <Nav.Link onClick={() => setTab('preview')} className={tab === 'preview' ? 'nav-link-active' : ''}>Preview</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Card.Header>
                    <Card.Body className="nav-card-body">
                        {tab === 'questions' && <Fragment>
                            <Card.Title className="content-title">
                                <Flex>
                                    Select Questions
                                </Flex>
                            </Card.Title>
                            <Card.Text>
                                <Box className={'nav-card-content'}>
                                    <Box>
                                        {filteredQuestions.map(masterQuestion => {
                                            const { question = '', _id = '' } = masterQuestion;
                                            return <Flex m={1} mb={2}>
                                                <CheckBox type="checkbox" name={_id} onChange={onClickQuestion} checked={checkedQuestions.includes(_id)} />
                                                <Box mt={2} className="questions">
                                                    {question}
                                                </Box>
                                            </Flex>
                                        })}
                                    </Box>
                                </Box>
                            </Card.Text>
                        </Fragment>}
                        {tab === 'preview' && <Fragment>
                            <Card.Title className="content-title">
                                <Flex>
                                    Preview Questions
                                </Flex>
                            </Card.Title>
                            <Card.Text>
                                <Box className={'nav-card-content'}>
                                    <Box>
                                        {selectedQuestions.map(selectedQuestion => {
                                            const { question = '', _id = '' } = selectedQuestion;
                                            return <Flex m={1} mb={2}>
                                                <Box mt={2} className="questions">
                                                    {question}
                                                </Box>
                                            </Flex>
                                        })}
                                    </Box>
                                </Box>
                            </Card.Text>
                        </Fragment>}
                    </Card.Body>
                </Card>
                <Flex m={2} mt={3} className="float-right">
                    <Button name="Submit" className="primary-button-question-submit" onClick={onSubmit} />
                </Flex>
            </ContentBox>
            <RightPanel bg="#FFFFFF" className="test-detail-right-panel">
                <Box>
                    <Box m={2} mb={4}>
                        <Span text="Practice test" className="span-lg-green" />
                    </Box>
                    <Box m={2}>
                        <Box m={2} className="title-s5">Topic</Box>
                        <Box m={2}><Span text="Topic of the chapter" className="title-s6-grey" /></Box>
                    </Box>
                    <Flex>
                        <Box m={2}>
                            <Box m={2} className="title-s5">Test Starting Time</Box>
                            <Box m={2}><Span text="12:30 pm" className="span-sm-violet" /></Box>
                        </Box>
                        <Box m={2}>
                            <Box m={2} className="title-s5">Test Duration</Box>
                            <Box m={2}><Span text="60 mins" className="span-sm-blue" /></Box>
                        </Box>
                    </Flex>
                    <Box m={2}>
                        <Box m={2} className="title-s5">Questions Added</Box>
                        <Box m={2}><Span text="06" className="span-sm-red" /></Box>
                    </Box>
                </Box>
            </RightPanel>
        </Flex >
    );
};

export default TestDetail