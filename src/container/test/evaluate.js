/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { Flex, FlexWrap, Box, Spacer } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import { getSubmissionByTestId, updateTestResults } from '../../action/test';
import { isStudent, isTeacher, getNumber, arrayEqual } from '../../utils/helper';
import { LiveIcon } from '../../components/asset/icons/live';
import moment from 'moment';
import { Span } from '../../components/asset/elements';
import MyCard from '../../components/asset/cards/card';
import { TextArea, RadioButton, Input } from '../../components/asset/form-inputs';
import { ProfilePic } from '../../components/asset/icons/profilePic';
import { getSubmittiedResults, getloadingTestResults } from '../../reducer/test';
import { isUndefined } from 'lodash';
import Accordion from 'react-bootstrap/Accordion'
import { Card } from 'react-bootstrap'
import { Button } from '../../components/asset/button'
import '../../assets/css/group.css';
import '../../assets/css/test.css'

const Evaluate = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();
    const results = useSelector(getSubmittiedResults);
    const isFetchingResults = useSelector(getloadingTestResults);
    const { state } = location;
    const [selectedUserId, setSelectedUserId] = useState('');
    const [total, setTotal] = useState(0);
    const [totalScored, setTotalScored] = useState(0);
    const [selectedAnswers, setSelectedAnswers] = useState({});
    const [scores, setScores] = useState({});

    const { test = {} } = state || {};
    const { name = '', subjectId = '', lessonId = '', topicId = '', start = '',
        timing = '', questions = [], _id: testId } = test;
    const { test: resultByUser = [], questions: resultQuestions = [] } = results;

    useEffect(() => {
        testId && dispatch(getSubmissionByTestId({ testId }));
    }, testId);

    useEffect(() => {
        const [firstUserData = {}] = resultByUser;
        const { userId = '', answers = [] } = firstUserData;
        const [firstObj = {}] = answers;
        setSelectedUserId(userId);
        setSelectedAnswers(Object.keys(firstObj).reduce((accum, data) => ({ ...accum, [data]: firstObj[data]?.answers || '' }), {}));
        setScores(getScores(firstObj));
    }, [resultByUser]);

    useEffect(() => {
        if (selectedUserId) {
            const selectedData = resultByUser.filter(data => data.userId == selectedUserId);
            const [firstUserData = {}] = selectedData;
            const { userId = '', answers = [] } = firstUserData;
            const [firstObj = {}] = answers;
            setSelectedAnswers(Object.keys(firstObj).reduce((accum, data) => ({ ...accum, [data]: firstObj[data]?.answers || '' }), {}));
            setScores(getScores(firstObj));
        }
    }, [selectedUserId]);

    useEffect(() => {
        const total = Object.keys(scores).reduce((accum, quest) => accum + getNumber(scores[quest]), 0);
        setTotalScored(total);
    }, [scores]);

    useEffect(() => {
        const total = resultQuestions.reduce((accum, quest) => accum + getNumber(quest.weightage), 0);
        setTotal(total);
    }, [resultQuestions]);

    const handleScoreChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setScores({
            ...scores,
            [name]: value
        })
    }

    const handleSubmit = () => {
        const selectedData = resultByUser.filter(data => data.userId == selectedUserId);
        const [firstUserData = {}] = selectedData;
        const { _id = '', answers = [] } = firstUserData;
        const [firstAnswer = {}] = answers;
        const newAnswers = [resultQuestions.reduce((accum, quest) => {
            const { _id: questionId = '' } = quest;
            const data = firstAnswer[questionId] || {};
            return {
                ...accum,
                [questionId]: {
                    ...data,
                    marks: getNumber(scores[questionId]) || 0
                }
            };
        }, {})];
        dispatch(updateTestResults({ testId, submitId: _id, payload: { testId, answers: newAnswers, finalScore: totalScored } }))
    }

    const getScores = (obj) => {
        return resultQuestions.reduce((accum, ques) => {
            const { _id = '', type = '', correctAnswer = '', weightage = '' } = ques;
            let value = 0;
            const _data = obj[_id] || {};
            const _correctAnswer = correctAnswer.split(',') || [];
            if (!isUndefined(_data.marks)) {
                value = _data.marks;
            } else {
                switch (type) {
                    case "single_choice":
                        value = correctAnswer == _data.answers ? weightage : 0;
                        break;
                    case "mul_choice":
                        value = arrayEqual(_correctAnswer, _data.answers || []) ? weightage : 0;
                        break;
                }
            }
            return {
                ...accum,
                [_id]: value
            }
        }, {});
    }

    return (
        <Flex className="test-evaluation">
            <ContentBox>
                <FlexWrap>
                    <Flex ml={2}>
                        <LiveIcon widht="40px" height="40px" />
                    </Flex>
                    <Flex m={2} className="page-title">
                        Test
                    </Flex>
                    <Flex mt={2} className="title-lable float-right">
                        Total Marks Scored :<Span text={` ${totalScored}/${total}`} className="red" />
                    </Flex>
                </FlexWrap>
                <MyCard className="test-evaluation-card">
                    {/* <Box m={2} className="title-lable">
                        <Span text="Choose the best" className="blue" /> -1 Mark
                    </Box> */}
                    <Spacer mb={3} />

                    {resultQuestions.map((question, index) => {
                        const { type = '', options = [], question: name = '', weightage = '', correctAnswer = '', _id: questionId = '' } = question;
                        let score = 0;
                        switch (type) {
                            case "single_choice":
                                score = !isUndefined(scores[questionId]) ? scores[questionId] : correctAnswer == selectedAnswers[questionId] ? weightage : 0;
                                return (
                                    <Flex className="question" m={2} mb={1} mt={1}>
                                        <Box width="80%" mr={2}>
                                            <Box mb={2}>
                                                {index + 1}. {name}
                                            </Box>
                                            <Box>
                                                {options.map((option, optionIndex) => {
                                                    const color = (correctAnswer == selectedAnswers[questionId] && correctAnswer == optionIndex + 1) || optionIndex + 1 == correctAnswer ? 'green'
                                                        : selectedAnswers[questionId] == optionIndex + 1 ? 'red' : '';
                                                    return <RadioButton
                                                        name={questionId}
                                                        group={questionId}
                                                        value={option.id}
                                                        disabled={true}
                                                        displayLabel={option.option}
                                                        type="checkbox"
                                                        className={`choice ${color}`}
                                                        checked={color != ''}
                                                    />
                                                })}
                                            </Box>
                                        </Box>
                                        <Flex m={1}>
                                            <Box>
                                                <Input type="text" height="35px" className="text-input-evaluate" value={scores[questionId]} name={questionId} onChange={handleScoreChange} />
                                            </Box>
                                            <Box p={1} pt={2}>
                                                /
                                            </Box>
                                            <Box width="35px" height="35px" p={"12px"} className="mark-box float-right">
                                                <Span text={weightage} className="red bold" />
                                            </Box>
                                        </Flex>
                                    </Flex>
                                );
                            case "mul_choice":
                                const _correctAnswer = correctAnswer.split(',') || [];
                                const _selectedAnswers = selectedAnswers[questionId] || [];
                                score = !isUndefined(scores[questionId]) ? scores[questionId] : arrayEqual(_correctAnswer, _selectedAnswers) ? weightage : 0;
                                return (<Flex className="question" m={2} mb={1} mt={1}>
                                    <Box mb={2} mt={2} width="80%" mr={2}>
                                        <Box mb={2}>
                                            {index + 1}. {name}
                                        </Box>
                                        <Box>
                                            {options.map((option, optionIndex) => {
                                                const color = (_correctAnswer.includes(`${optionIndex + 1}`) && _selectedAnswers.includes(`${optionIndex + 1}`)) || _correctAnswer.includes(`${optionIndex + 1}`) ? 'green'
                                                    : _selectedAnswers.includes(`${optionIndex + 1}`) ? 'red' : '';
                                                return <RadioButton
                                                    name={questionId}
                                                    group={questionId}
                                                    value={option.id}
                                                    disabled={true}
                                                    displayLabel={option.option}
                                                    type="checkbox"
                                                    className={`choice ${color}`}
                                                    checked={color != ''}
                                                />
                                            })}
                                        </Box>
                                    </Box>
                                    <Flex m={1}>
                                        <Box>
                                            <Input type="text" height="35px" className="text-input-evaluate" value={scores[questionId]} name={questionId} onChange={handleScoreChange} />
                                        </Box>
                                        <Box p={1} pt={2}>
                                            /
                                    </Box>
                                        <Box width="35px" height="35px" p={"12px"} className="mark-box float-right">
                                            <Span text={weightage} className="red bold" />
                                        </Box>
                                    </Flex>
                                </Flex>);
                            case "text":
                                return (<Flex className="question" m={2} mb={1} mt={1}>
                                    <Box mb={2} mt={2} width="80%" mr={2}>
                                        <Box mb={2}>
                                            {index + 1}. {name}
                                        </Box>
                                        <Box>
                                            <TextArea className="text-input-textarea-form" rows={6} value={selectedAnswers[questionId]} disabled={true} />
                                        </Box>
                                    </Box>
                                    <Flex m={1}>
                                        <Box>
                                            <Input type="text" height="35px" className="text-input-evaluate" value={scores[questionId]} name={questionId} onChange={handleScoreChange} />
                                        </Box>
                                        <Box p={1} pt={2}>
                                            /
                                    </Box>
                                        <Box width="35px" height="35px" p={"12px"} className="mark-box float-right">
                                            <Span text={weightage} className="red bold" />
                                        </Box>
                                    </Flex>
                                </Flex>);
                        }
                    })}
                    <Box m={2} width="100px" className="float-right">
                        <Button className="primary-button-sm" name="submit" onClick={handleSubmit} />
                    </Box>
                </MyCard>
            </ContentBox >
            <RightPanel>
                <Accordion defaultActiveKey="">
                    <Card className="rightpanel-cards">
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            <Box className="title-lable" m={2}>
                                Test details
                            </Box>
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <Card.Body>
                                <Box>
                                    <Box m={2}>
                                        <Span text={'Exam'} className="test-type-completed" />
                                    </Box>
                                    <Flex>
                                        <Box m={2}>
                                            <Box m={2} className="test-card-topics">Test Name</Box>
                                            <Box m={2}><Span text={name} className="test-group" /></Box>
                                        </Box>
                                        <Box m={2}>
                                            <Box m={2} className="test-card-topics">Subject</Box>
                                            <Box m={2}><Span text={subjectId} className="test-group" /></Box>
                                        </Box>
                                    </Flex>

                                    <Flex>
                                        {lessonId && <Box m={2}>
                                            <Box m={2} className="test-card-topics">Lesson</Box>
                                            <Box m={2}><Span text={lessonId} className="test-group" /></Box>
                                        </Box>}
                                        {topicId && <Box m={2}>
                                            <Box m={2} className="test-card-topics">Topic</Box>
                                            <Box m={2}><Span text={topicId} className="test-group" /></Box>
                                        </Box>}
                                    </Flex>
                                    <Flex>
                                        <Box m={2}>
                                            <Box m={2} className="test-card-topics">Test Starting Time</Box>
                                            <Box m={2}><Span text={moment(start).format('lll')} className="test-time" /></Box>
                                        </Box>
                                        <Box m={2}>
                                            <Box m={2} className="test-card-topics">Test Duration</Box>
                                            <Box m={2}><Span text={timing} className="test-duration" /></Box>
                                        </Box>
                                    </Flex>
                                    <Flex>
                                        <Box m={2}>
                                            <Box m={2} className="test-card-topics">Total Questions</Box>
                                            <Box m={2}><Span text={questions.length} className="questions-added" /></Box>
                                        </Box>
                                        <Box m={2}>
                                            <Box m={2} className="test-card-topics">Staff Name</Box>
                                            <Flex>
                                                <ProfilePic width="24" height="24" />
                                                <Box m={2}><Span text="Ria Wilson" className="test-group" /></Box>
                                            </Flex>
                                        </Box>
                                    </Flex>
                                </Box>
                            </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
                <Spacer mb={2} />
                <Accordion defaultActiveKey="0">
                    <Card className="rightpanel-cards">
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            <Box m={2} className="title-lable">
                                Students
                            </Box>
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <Card.Body>
                                <Box>
                                    <Box m={2} mb={3}>
                                        {resultByUser.map(user => {
                                            const { userId = '', createdBy = '' } = user;
                                            return (
                                                <Flex p={1} className={selectedUserId == userId ? 'selected-student' : ''} onClick={() => setSelectedUserId(userId)}>
                                                    <ProfilePic width="42" height="42" />
                                                    <Box>
                                                        <Box className="test-group" m={1} >{createdBy || 'Anonymous'}</Box>
                                                        <Box m={1} ><Span className="roll-no" text={userId} /></Box>
                                                    </Box>
                                                </Flex>
                                            )
                                        })}
                                    </Box>
                                    <Spacer mb={1} />
                                </Box>
                            </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </RightPanel>
        </Flex >
    );
};

export default Evaluate;
