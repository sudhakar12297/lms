import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Form from '../../components/test/form';
import 'react-datepicker/dist/react-datepicker.css';

import { createTest, updateTest } from '../../action/test';

const TestForm = (props) => {
    const { groups } = props
    const dispatch = useDispatch()

    const submit = (data) => {
        data.isUpdate
            ? dispatch(
                updateTest({
                    id: data.id,
                    payload: data.payload,
                })
            )
            : dispatch(
                createTest({ payload: data.payload })
            );
    };

    return <Form groups={groups} onSubmit={(data) => {
        submit(data)
    }} />;
};

export default TestForm;
