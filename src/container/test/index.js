/* eslint-disable react/prop-types */
import React, { useState, useEffect, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { Card, Nav } from 'react-bootstrap';
import { Span } from '../../components/asset/elements';

//action
import { fetchTests, deleteTest } from '../../action/test';
//reducer
import { getRole } from '../../reducer/account';
import { getTests, getRedirectPayload } from '../../reducer/test';

import Test from '../../components/test/display';

//assets
import { Box, Flex, FlexWrap } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import Popup from '../../components/asset/popup';

import Form from './form';
import { ROLE } from '../../utils/common';

import '../../assets/css/group.css';
import { getMyGroups } from '../../reducer/group';
import { fetchMyGroups } from '../../action/group';
import { isStudent, isTeacher, isCurrentTimeInBetweenStartAndEndOfLive } from '../../utils/helper';
import moment from 'moment';
import { ExamIcon2 } from '../../components/asset/icons/exam';
import { Button } from '../../components/asset/button';
import { MobileRightPanel } from '../layout/mobile';
import { Close } from '../../components/asset/icons/close';
import { AddIcon } from '../../components/asset/icons/add';
import { isEmpty } from 'lodash'

const Tests = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation()
    const { state = {} } = location;
    const { currentPage = 'live' } = state;
    const loggedInUserRole = useSelector(getRole);
    const allTests = useSelector(getTests);
    const myGroups = useSelector(getMyGroups);
    const [show, setShow] = useState(false);
    const [timer, setTimer] = useState('');
    const redirectPayload = useSelector(getRedirectPayload)
    const [deleteTestId, setDeleteTestId] = useState();
    const [page = '', setPage] = useState(currentPage);
    const [tests, setTests] = useState({
        upcoming: [],
        live: [],
        previous: []
    })

    // useEffect(() => {
    //     const clock = setTimeout(() => {
    //         let _allTests = allTests.filter(test => !test.isPractice);
    //         setTests({
    //             upcoming: _allTests.filter(test => (moment(test.start).isSameOrAfter(new Date()))),
    //             previous: _allTests.filter(test => moment(moment(test?.start).add(test.timing, 'minutes')).isBefore(new Date())),
    //             live: _allTests.filter(test => (isCurrentTimeInBetweenStartAndEndOfLive(test.start, test.timing))),
    //         })
    //     }, 5000);

    //     return () => clearTimeout(clock);
    // });

    useEffect(() => {
        dispatch(fetchTests())
        dispatch(fetchMyGroups())
        dispatch({ type: 'RESET_REDIRECT_TEST' });
        // dispatch({ type: 'RESET_QUESION_MAP_RESPONSE' });
    }, [dispatch])

    useEffect(() => {
        if (!isEmpty(redirectPayload)) {
            dispatch({ type: 'RESET_REDIRECT_TEST' });
            onClickTest(null, redirectPayload);
        }
    }, [redirectPayload]);

    useEffect(() => {
        // Need to remove this logic once backend api is done
        let _allTests = allTests.filter(test => !test.isPractice);
        setTests({
            upcoming: _allTests.filter(test => (moment(test.start).isSameOrAfter(new Date()))),
            previous: _allTests.filter(test => moment(moment(test?.start).add(test.timing, 'minutes')).isBefore(new Date())),
            live: _allTests.filter(test => (isCurrentTimeInBetweenStartAndEndOfLive(test.start, test.timing))),
        })
    }, [allTests]);

    const onEdit = (e, test) => {
        e.stopPropagation()
        history.push({ pathname: '/test', state: { test, isUpdate: true } });
    };

    const deleteConfirmed = (e) => {
        dispatch(deleteTest({ id: deleteTestId }));
        setShow(false);
    };

    const onClickTest = (e, test) => {
        e && e.stopPropagation()
        history.push({ pathname: '/test/detail', state: { test } });
    };

    const onClickAttend = (e, test) => {
        e.stopPropagation()
        history.push({ pathname: '/test/attend', state: { test } });
    };

    const onClickEvaluate = (e, test) => {
        e.stopPropagation()
        history.push({ pathname: '/test/evaluate', state: { test } });
    };

    const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
    const onClickToggleRightPanel = () => {
        setShowRightPanelOnMobile(!showRightPanelOnMobile);
    };


    return (
        <Flex className="group-container">
            <ContentBox blur={showRightPanelOnMobile}>
                <Flex mb={2}>
                    <Box>
                        <ExamIcon2 widht="34px" height="34px" />
                    </Box>
                    <Box m={1} mt={2} className="page-title">
                        Create Assesment
                    </Box>
                </Flex>
                <Card className="live-session-nav-card">
                    <Card.Header className="card-header">
                        <Nav variant="tabs" defaultActiveKey="#first">
                            <Nav.Item className="nav-item">
                                <Flex>
                                    <Nav.Item className="nav-item">
                                        <Nav.Link
                                            onClick={() => setPage('live')}
                                            className={page === 'live' ? 'nav-link-active ' : ''}
                                        >
                                            {' '}
                  Ongoing Tests
                  <Span
                                                text={tests.live?.length}
                                                className="notification-counts"
                                            />
                                        </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Link
                                        onClick={() => setPage('upcoming')}
                                        className={page === 'upcoming' ? 'nav-link-active ' : ''}
                                    >
                                        {' '}
                    Upcoming Tests{' '}
                                        <Span
                                            text={tests.upcoming?.length}
                                            className="notification-counts"
                                        />
                                    </Nav.Link>
                                    <Box m={1} pl={2}></Box>
                                </Flex>
                            </Nav.Item>

                            <Nav.Item className="nav-item">
                                <Nav.Link
                                    onClick={() => setPage('completed')}
                                    className={page === 'completed' ? 'nav-link-active ' : ''}
                                >
                                    {' '}
                  Completed Tests
                  <Span
                                        text={tests.previous?.length}
                                        className="notification-counts"
                                    />
                                </Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Card.Header>
                    <Card.Body>
                        {page === 'upcoming' && (
                            <Fragment>
                                <Card.Text>
                                    <Box mt={3}>
                                        <Test
                                            tab={page}
                                            color={true}
                                            tests={tests.upcoming}
                                            onClick={(e, test) => { onClickTest(e, test) }}
                                            onClickAttend={(e, test) => { onClickAttend(e, test) }}
                                            onClickEvaluate={onClickEvaluate}
                                            edit={onEdit}
                                            remove={(e, id) => {
                                                e.stopPropagation()
                                                setShow(true);
                                                setDeleteTestId(id)
                                            }}
                                            showEdit={isTeacher(loggedInUserRole)}
                                            showRemove={isTeacher(loggedInUserRole)}
                                            showAttend={isStudent(loggedInUserRole)}
                                            disableAttend={isStudent(loggedInUserRole)}
                                            showNoQuestions={isTeacher(loggedInUserRole)}
                                            showEvaluate={false}
                                            groups={myGroups}
                                        />
                                    </Box>
                                </Card.Text>
                            </Fragment>
                        )}
                        {page == 'live' && (
                            <Fragment>
                                <Card.Text>
                                    <Box mt={3}>
                                        <Test
                                            isLive={true}
                                            tab={page}
                                            color={true}
                                            tests={tests.live}
                                            onClick={(e, test) => { onClickTest(e, test) }}
                                            onClickAttend={(e, test) => { onClickAttend(e, test) }}
                                            onClickEvaluate={onClickEvaluate}
                                            edit={onEdit}
                                            remove={(e, id) => {
                                                e.stopPropagation()
                                                setShow(true);
                                                setDeleteTestId(id)
                                            }}
                                            showEdit={isTeacher(loggedInUserRole)}
                                            showRemove={isTeacher(loggedInUserRole)}
                                            showAttend={isStudent(loggedInUserRole)}
                                            disableAttend={isStudent(loggedInUserRole)}
                                            showNoQuestions={isTeacher(loggedInUserRole)}
                                            showEvaluate={false}
                                            groups={myGroups}
                                        />
                                    </Box>
                                </Card.Text>
                            </Fragment>
                        )}
                        {page === 'completed' && (
                            <Fragment>
                                <Card.Text>
                                    <Box mt={3}>
                                        <Test
                                            tab={page}
                                            color={false}
                                            tests={tests.previous}
                                            onClick={(e, test) => { onClickTest(e, test) }}
                                            onClickAttend={(e, test) => { onClickAttend(e, test) }}
                                            onClickEvaluate={onClickEvaluate}
                                            edit={onEdit}
                                            remove={(e, id) => {
                                                e.stopPropagation()
                                                setShow(true);
                                                setDeleteTestId(id)
                                            }}
                                            showEdit={false}
                                            showNoQuestions={isTeacher(loggedInUserRole)}
                                            showRemove={false}
                                            showAttend={false}
                                            showEvaluate={isTeacher(loggedInUserRole)}
                                            groups={myGroups}
                                        />
                                    </Box>
                                </Card.Text>
                            </Fragment>
                        )}
                    </Card.Body>
                </Card>
            </ContentBox>
            <RightPanel>
                {!isStudent(loggedInUserRole) ? (
                    <Form groups={myGroups} />
                ) : (
                        'Will we show you something else soon'
                    )}
            </RightPanel>
            <MobileRightPanel
                title="Test Creation"
                show={showRightPanelOnMobile ? 'visible' : 'hide'}
            >
                {!isStudent(loggedInUserRole) && <Form groups={myGroups} />}
            </MobileRightPanel>
            <Popup
                show={show}
                cancel={() => setShow(false)}
                confirm={deleteConfirmed}
            />
            {!showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
                <Button
                    name={<AddIcon />}
                    title={'Create'}
                    className={'mob-primary-button-form-'}
                    onClick={onClickToggleRightPanel}
                />
            )}
            {showRightPanelOnMobile && !isStudent(loggedInUserRole) && (
                <Button
                    name={<Close />}
                    title={'Close'}
                    className={'mob-primary-button-form-close'}
                    onClick={onClickToggleRightPanel}
                />
            )}
        </Flex>
    );
};

export default Tests;
