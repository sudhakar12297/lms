import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { isEqual } from 'lodash';
import { Box, Flex } from '../../components/asset/wrapper';
import '../../assets/css/test.css'
import { RadioButton, CheckBox, TextArea } from '../../components/asset/form-inputs'
import { Button } from '../../components/asset/button';
import { getNumber } from '../../utils/helper';
import { fetchQuestionsByQuestionIds } from '../../action/question';
import { getQuestions } from '../../reducer/question';
import { getUser } from '../../reducer/account';
import { createSubmission } from '../../action/submission';

const Questions = (props) => {
    const dispatch = useDispatch()
    const location = useLocation()
    const { state } = location
    const { test } = state && state

    const questionIds = test?.questions

    const questions = useSelector(getQuestions)
    const user = useSelector(getUser)

    useEffect(() => {
        questionIds?.length && dispatch(fetchQuestionsByQuestionIds({ questionIds }))
    }, [questionIds])

    const [selectedAnswers, setSelectedAnswers] = useState({});

    const onChange = (type, id, value) => {
        let _value = value;
        if (type == 'checkbox') {
            let oldAnswer = selectedAnswers[id] || [];
            if (oldAnswer.includes(value)) {
                _value = oldAnswer.filter(data => data != value)
            } else {
                _value = [...oldAnswer, value];
            }
        }
        setSelectedAnswers({ ...selectedAnswers, [id]: _value })
    }

    const handleSubmit = () => {
        const payload = {
            testId: test._id,
            userId: user.userId,
            answers: [questions.reduce((accum, ques) => {
                return {
                    ...accum,
                    [ques._id]: {
                        answers: selectedAnswers[ques._id]
                    }
                }
            }, {})]
        }
        dispatch(createSubmission({ payload }))
    }

    const handleRestart = () => {
        setSelectedAnswers({});
    }

    return (
        <Box mr={4}>
            <Box>
                <Box>
                    Test Attend:
                    {/* {
                        questions.map((question, index) => {
                            const { question: name = '', type = '', options = [], correctAnswer = '', _id = '' } = question;
                            switch (type) {
                                case "single_choice":
                                    return (
                                        <Box m={3}>
                                            <Box className="question">
                                                {index + 1}. {name}
                                            </Box>
                                            {options.map(option => {
                                                return <Box m={2}>
                                                    <RadioButton
                                                        name={_id}
                                                        group={_id}
                                                        displayLabel={option.option}
                                                        type="radio"
                                                        className="choice selected"
                                                        onChange={() => onChange("radio", _id, option.id)}
                                                        checked={selectedAnswers[_id] == option.id}
                                                    />
                                                </Box>
                                            })}
                                        </Box>
                                    );
                                case "mul_choice":
                                    return (
                                        <Box m={3}>
                                            <Box className="question">
                                                {index + 1}. {name}
                                            </Box>
                                            {options.map(option => {
                                                return <Box m={2}>
                                                    <CheckBox
                                                        name={_id}
                                                        value={option.id}
                                                        displayLabel={option.option}
                                                        type="checkbox"
                                                        className="choice selected"
                                                        onChange={() => onChange("checkbox", _id, option.id)}
                                                        checked={selectedAnswers[_id] && selectedAnswers[_id].includes(option.id)}
                                                    />
                                                </Box>
                                            })}
                                        </Box>
                                    );
                                case "text":
                                    return (
                                        <Box m={3}>
                                            <Box className="question">
                                                {index + 1}. {name}
                                            </Box>
                                            <TextArea className="text-input-textarea-form" onChange={(e) => onChange('text', _id, e.target.value)} />
                                        </Box>
                                    );
                                default: return "";
                            }
                        })
                    } */}
                </Box >
            </Box>
            <Flex mt={2} className="float-right">
                {/* <Button name="Restart" className="primary-button-sm button-white" onClick={handleRestart} /> */}
                {/* <Button name="Submit test" className="primary-button-sm" onClick={handleSubmit} /> */}
            </Flex>
        </Box>
    );
};

export default Questions;