import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';

import { Form } from '../../components/topic/index';
import { FlexWrap, Spacer } from '../../components/asset/wrapper';
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import ChapterCard from '../../components/lesson/card';

import { createTopic, updateTopic } from '../../action/topic';

export default () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const [editorData, setEditorData] = useState('');

  const { state = {} } = location;
  const { isUpdate = false, topicId, lesson = {} } = state
  const { name = '', description = '' } = lesson;
  const onsubmit = (data) => {
    const { payload, subjectId, lessonId, isUpdate, id } = data;



    dispatch(
      isUpdate
        ? updateTopic({ payload, subjectId, lessonId, id })
        : createTopic({ payload, subjectId, lessonId })
    );
  };

  return (
    <FlexWrap>
      <ContentBox>
        <ChapterCard
          lesson={{ name, description }}
          collapse={false}
        />
        <Spacer mb={4} />
        <Form
          isUpdate={isUpdate}
          topicId={topicId}
          onChangeParent={(value) => {
            setEditorData(value);
          }}
          onSubmit={onsubmit}
        />
      </ContentBox>
      <RightPanel>
        <div
          className="editor-data-display"
          dangerouslySetInnerHTML={{ __html: editorData }}
        />
      </RightPanel>
    </FlexWrap>
  );
};
