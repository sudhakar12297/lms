import React, { useState, Fragment, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Nav from 'react-bootstrap/Nav'
import moment from 'moment'
import { useSelector, useDispatch } from 'react-redux';

import { Box, Flex, FlexWrap } from '../../components/asset/wrapper';
// import { Button } from '../asset/button'

import '../../assets/css/topic.css';
import '../../assets/css/nav-card.css'
import { ContentBox } from '../../components/layout';
import RightPanel from '../layout/rightPanel';
import Editor from '../../components/editor';
import Popup from '../../components/asset/popup';

import { updateNote, fetchNote } from '../../action/note';
import { fetchDoubtsByForTopic, createDoubt, deleteDoubt, updateDoubt } from '../../action/doubt';
import { setTopic } from '../../action/common';
import { fetchQuestionsByQuestionIds } from '../../action/question';
import { fetchTestsByTopic } from '../../action/test';

import { getSelectedTopic, getSelectedLesson, getSelectedSubject, getHomeWorks } from '../../reducer/common';
import { getUser } from '../../reducer/account';
import { getNote } from '../../reducer/note';
import { getLessons } from '../../reducer/lesson';
import { getDoubts } from '../../reducer/doubt';
import { getTests } from '../../reducer/test';
import { getQuestions } from '../../reducer/question';

import Discussion from '../../components/doubt/discussion';
import { Send } from '../../components/asset/icons/send'
import { isValidFormFieldData, getOnGoingHomeworks } from '../../utils/helper';
import Question from './test';
import { Button } from '../../components/asset/button';
import { Input } from '../../components/asset/form-inputs'
import { fetchTopicById, saveTrackTopicData } from '../../action/topic';
import { getTopic, getIsLoading, getIsError } from '../../reducer/topic';
import { fetchLessonsBySubject } from '../../action/lesson';
import { getRole } from '../../reducer/account';
import { isStudent } from '../../utils/helper';
import { MobileRightPanel } from '../layout/mobile';
import { Close } from '../../components/asset/icons/close';
import { IconNotes } from '../../components/asset/icons';
import { NoDataAvailable } from '../../components/asset/noData';
import { getHomeworks, getIsHomeworkLoading } from '../../reducer/homework';

const StudyTopic = () => {
    const dispatch = useDispatch()
    const [page = '', setPage] = useState('description');
    const [note, setNote] = useState('')
    const [allTopics, setAllTopic] = useState([])
    const [errMsg, setErrMsg] = useState('')
    const [show, setShow] = useState(false)
    const [showMeaning, setShowMeaning] = useState(false)
    const [findMeaning, setFindMeaning] = useState('')

    const user = useSelector(getUser)
    const topic = useSelector(getTopic)
    const isLoading = useSelector(getIsLoading)
    const selectedTopic = useSelector(getSelectedTopic)
    const selectedLesson = useSelector(getSelectedLesson)
    const selectedSubject = useSelector(getSelectedSubject)
    const lessons = useSelector(getLessons)
    const noteFromStore = useSelector(getNote)
    const doubts = useSelector(getDoubts)
    const tests = useSelector(getTests);
    const questions = useSelector(getQuestions);
    const isHomeworkLoading = useSelector(getIsHomeworkLoading);
    const homeworks = useSelector(getHomeworks);

    const subjectId = selectedSubject.id
    const topicId = selectedTopic.id
    const lessonId = selectedLesson.id

    useEffect(() => {
        const userId = user.userId
        if (topicId && userId) {
            dispatch({ type: 'SET_USER_ONBOARD_TIME', moment: moment() })
            dispatch(fetchTopicById({ subjectId, lessonId, topicId }))
            dispatch(fetchNote({ topicId, userId }))
            dispatch(fetchDoubtsByForTopic({ id: topicId }))
            dispatch(fetchTestsByTopic({ id: topicId, isPractice: true }))
            dispatch(fetchLessonsBySubject({ id: subjectId }))
        }
    }, [dispatch, topicId])

    //component unmount
    useEffect(() => {
        return () => {
            dispatch(saveTrackTopicData())
            const payload = {
                subjectId,
                chapterId: lessonId
            }
            dispatch(updateNote({ payload, topicId, userId: user.userId }))
            dispatch({ type: 'UNSET_TOPIC' })
        }
    }, [])

    useEffect(() => {
        let _questions = [];
        tests.forEach(test => {
            const { questions = [] } = test;
            _questions = [..._questions, ...questions];
        })
        _questions.length && dispatch(fetchQuestionsByQuestionIds({ questionIds: _questions }))
    }, [tests]);

    //filter all topic to display on right card
    useEffect(() => {
        const filteredLesson = lessons?.filter(lesson => lesson.id === lessonId)
        setAllTopic(filteredLesson[0]?.topics)
    }, [lessons])

    useEffect(() => {
        setNote(noteFromStore)
    }, [noteFromStore])

    useEffect(() => {
        if (homeworks.length) {
            const onGoingHomeworks = getOnGoingHomeworks(homeworks)
            console.log(onGoingHomeworks);
            const filteredHomeworks = onGoingHomeworks?.filter(homework => homework.topicIds.includes(topicId))
            dispatch({ type: 'SET_ONGOING_HOMEWORKS_CONTAINS_CURRENT_TOPIC', homeworks: filteredHomeworks })
        }
    }, [homeworks, isHomeworkLoading])

    const onChange = (value) => {
        setNote(value)
        dispatch({ type: 'ON_CHANGE_NOTE', note: value })
    }

    const onClickTopic = (topic) => {
        console.log(topic);
        dispatch(saveTrackTopicData())
        dispatch({ type: 'SET_USER_ONBOARD_TIME', moment: moment() })
        const payload = {
            subjectId,
            chapterId: lessonId
        }
        dispatch(updateNote({ payload, topicId, userId: user.userId }))
        dispatch(setTopic(topic.id, topic.name))
        dispatch(fetchTopicById({ subjectId, lessonId, topicId: topic.id }))
    }

    const [formData, setformData] = useState('')
    const onChangeDoubt = (e) => {
        setformData({ ...formData, [e.target.name]: e.target.value })
    }

    const [isEdit, setIsEdit] = useState(false)
    const onEdit = (isParent, msg, parentId, replyId) => {
        setIsEdit(true)
        isParent ? setformData({ ...formData, doubt: msg, parentId }) : setformData({ ...formData, reply: msg, parentId, replyId })
    }

    const [deleteData, setDeleteData] = useState('')
    const onDelete = (isParent, parentId, replyId) => {
        setDeleteData({ isParent, parentId, replyId, topicId })
        setShow(true)
    }

    const deleteConfirmed = () => {
        const { isParent, parentId, replyId, topicId } = deleteData
        dispatch(deleteDoubt({ isParent, parentId, replyId, topicId }))
        setShow(false)
    }

    const onSubmitDoubt = () => {
        if (isValidFormFieldData([formData?.doubt])) {
            const payload = {
                "message": formData?.doubt,
                "userId": user.userId,
                topicId
            }
            isEdit ? dispatch(updateDoubt({ payload, parentId: formData.parentId })) : dispatch(createDoubt({ payload }))
            setIsEdit(false)
            setformData('')
        } else {
            setErrMsg('Please ask your doubt')
        }
    }

    const onSubmitReply = (parentId) => {
        if (isValidFormFieldData([formData?.reply])) {
            const payload = {
                "message": formData?.reply,
                "userId": user.userId,
                topicId
            }
            isEdit ? dispatch(updateDoubt({ payload, parentId: formData?.parentId, replyId: formData?.replyId, isEdit })) : dispatch(createDoubt({ payload, parentId }))
            setIsEdit(false)
            setformData('')

        } else {
            setErrMsg('Please reply')
        }
    }

    const onChangeFindMeaning = (e) => {
        setFindMeaning(e.target.value)
    }

    const getData = ({ noun = [], verb = [], adverb = [], adjective = [], others = [] }) => {
        if (noun.length) {
            return noun[0] || {}
        }
        if (verb.length) {
            return verb[0] || {}
        }
        if (adverb.length) {
            return adverb[0] || {}
        }
        if (adjective.length) {
            return adjective[0] || {}
        }
        if (others.length) {
            return others[0] || {}
        }
    }

    const [searchResult, setSearchResult] = useState('No results found')
    const onClickFindMeaning = async () => {
        const response = await fetch(`https://api.dictionaryapi.dev/api/v1/entries/en/${findMeaning}`)
        const data = await response.json()
        const { noun, verb, adverb, adjective, ['transitive verb']: others } = data[0].meaning
        const meaning = getData({ noun, verb, adverb, adjective, others })
        setSearchResult(meaning)
        setShowMeaning(true)
    }

    const loggedInUserRole = useSelector(getRole);
    const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
    const onClickToggleRightPanel = () => {
        setShowRightPanelOnMobile(!showRightPanelOnMobile);
    };

    return (
        <Flex className="study-page-wrapper">
            <ContentBox blur={showRightPanelOnMobile} className="study-topic">
                <FlexWrap mb={1}>
                    <Flex>
                        <Box className="study-topic-chapter-box">
                            Lesson
                        </Box>
                        <Box m={1} ml={2} className="page-title">
                            {`Subject - ${selectedSubject.name}`}
                        </Box>
                    </Flex>
                    <Box m={2} ml={3} className="float-right">
                        <Input className="text-input-dict" onChange={onChangeFindMeaning} placeholder="Find meaning" />
                    </Box>
                    <Box mt={2}>
                        <Button name="Find" onClick={onClickFindMeaning} className="primary-button-sm" />
                    </Box>
                </FlexWrap>
                <Card className="nav-card">
                    <Card.Header className="nav-card-header">
                        <Nav variant="tabs" defaultActiveKey="#first" className={'nav-card-navbar'} >
                            <Nav.Item className={'nav-card-nav-item'}>
                                <Nav.Link onClick={() => setPage('description')} className={page === 'description' ? 'nav-link-active' : ''}>Explanation</Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={'nav-card-nav-item'}>
                                <Nav.Link onClick={() => setPage('discussion')} className={page === 'discussion' ? 'nav-link-active' : ''}>Discussion</Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={'nav-card-nav-item'}>
                                <Nav.Link onClick={() => setPage('assessment')} className={page === 'assessment' ? 'nav-link-active' : ''}>Practice</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Card.Header>
                    <Card.Body className="nav-card-body">
                        {
                            isLoading ? 'loading...' :
                                <Box>
                                    {page === 'description' &&
                                        <Fragment>
                                            <Card.Title className="nav-card-content-title">
                                                <Flex>
                                                    <Box className="topic-card-topic-box">{allTopics?.findIndex(topic => topic.id === topicId) + 1}</Box>
                                                    <Box className='nav-card-title'>{topic?.name}</Box>
                                                </Flex>
                                            </Card.Title>
                                            <Card.Text>
                                                <Flex className={'nav-card-content'}>
                                                    <div dangerouslySetInnerHTML={{ __html: topic?.content }} />
                                                </Flex>
                                            </Card.Text>
                                        </Fragment>
                                    }
                                    {page === 'discussion' &&
                                        <Fragment>
                                            <Card.Title className="nav-card-content-title">
                                                <Flex m={1}>
                                                    <Box className='title_lable'>{'Discussion'}</Box>
                                                </Flex>
                                            </Card.Title>
                                            <Card.Text>
                                                <Box className={'nav-card-content discussion'}>
                                                    {/* <div dangerouslySetInnerHTML={{ __html: content }} /> */}
                                                    {
                                                        doubts?.length ? (
                                                            doubts.map((doubt, index) => {
                                                                return <Discussion doubt={doubt} value={formData} showEdit={user.userId === doubt.userId} showRemove={user.userId === doubt.userId} edit={(isParent, msg, replyId) => onEdit(isParent, msg, doubt._id, replyId)} remove={(isParent, replyId) => onDelete(isParent, doubt._id, replyId)} onChange={onChangeDoubt} onSubmit={() => onSubmitReply(doubt._id)} />
                                                            })
                                                        )
                                                            : <NoDataAvailable text='No discussion(s) on this topic' />
                                                    }
                                                </Box>
                                                <Flex mt={2}>
                                                    <input name="doubt" value={formData?.doubt || ''} onChange={onChangeDoubt} placeholder="Type your message" className="reply-input-box" />
                                                    <Box mt={"10px"} className="reply-send-button" onClick={onSubmitDoubt}><Send /></Box>
                                                </Flex>
                                            </Card.Text>
                                        </Fragment>
                                    }
                                    {
                                        page === 'assessment' &&
                                        <Fragment>
                                            <Card.Title>
                                                <Flex>
                                                    <Box className='nav-card-title'>Test your knowledge..</Box>
                                                </Flex>
                                            </Card.Title>
                                            <Card.Text>
                                                {questions.length ? <Question
                                                    questions={questions}
                                                /> : <Box className={'p-grey'}>No practice questions lined to this topic</Box>}
                                            </Card.Text>
                                        </Fragment>
                                    }
                                </Box>
                        }
                    </Card.Body>
                </Card>
            </ContentBox>
            <RightPanel>
                <Box>
                    <Box pl={3} m={1} className="study-topics-total-info">
                        Total Topics: {allTopics?.length} (Quick Switch)
                    </Box>
                    <Box className="study-topics-wrapper" m={2}>
                        {
                            allTopics?.map((topic, index) => {
                                return (
                                    <Flex onClick={() => onClickTopic(topic)} p={3} m={1} mb={2} className={`study-topics ${topicId === topic.id && 'topic-active'}`}>
                                        <Box mr={3} className="topic-card-topic-box">{index + 1}</Box>
                                        {topic.name}
                                        {/* <Box className={"timer"}> 16.00 </Box> */}
                                    </Flex>
                                )
                            })
                        }
                    </Box>
                    <Box className="editor-study-notes">
                        <Card className={'editor-card'} width="100%">
                            <Box ml={0} m={1} className="editor-study-notes-title">
                                Take Notes
                            </Box>
                            <Editor limited={true} value={note} onChange={(value) => { onChange(value) }} rows={30} />
                            <Box className="editor-tip">
                                * Notes will save automatically
                            </Box>
                        </Card>
                    </Box>
                </Box>
            </RightPanel>
            <MobileRightPanel
                title="Group Creation"
                show={showRightPanelOnMobile ? 'visible editor-study-notes-right-panel' : 'hide editor-study-notes-right-panel'}
            >
                {isStudent(loggedInUserRole) &&
                    <Box className="editor-study-notes">
                        <Card className={'editor-card'} width="100%">
                            <Box ml={0} m={1} className="editor-study-notes-title">
                                Take Notes
                    </Box>
                            <Editor limited={true} value={note} onChange={(value) => { onChange(value) }} rows={30} />
                            <Box className="editor-tip">
                                * Notes will save automatically
                    </Box>
                        </Card>
                    </Box>}
            </MobileRightPanel>
            {!showRightPanelOnMobile && isStudent(loggedInUserRole) && (
                <Button
                    name={<IconNotes fill="#FFFFFF" />}
                    title={'Create'}
                    className={'mob-primary-button-form-'}
                    onClick={onClickToggleRightPanel}
                />
            )}
            {showRightPanelOnMobile && isStudent(loggedInUserRole) && (
                <Button
                    name={<Close />}
                    title={'Close'}
                    className={'mob-primary-button-form-close'}
                    onClick={onClickToggleRightPanel}
                />
            )}
            <Popup
                show={show}
                cancel={() => setShow(false)}
                confirm={deleteConfirmed}
            />
            <Popup
                show={showMeaning}
                cancel={() => setShowMeaning(false)}
                title='Meaning'
                buttonName='Got it'
                confirm={() => { setShowMeaning(false) }}
            >
                <div dangerouslySetInnerHTML={{
                    __html: `&nbsp;&nbsp;<b>Definition:</b><br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${searchResult?.definition || '---'}</br></br>
                &nbsp;&nbsp;<b>Example:</b><br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${searchResult?.example || '---'}</br></br>
                &nbsp;&nbsp;<b>Synonyms:</b><br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${searchResult?.synonyms?.join(', ') || '---'}`
                }} />
            </Popup>
        </Flex >
    );
};

export default StudyTopic