import React, { Fragment, useState } from 'react';
import { isEqual } from 'lodash';
import { Box, Flex, Spacer } from '../../components/asset/wrapper';
import '../../assets/css/test.css'
import { RadioButton, CheckBox } from '../../components/asset/form-inputs'
import { Button } from '../../components/asset/button';
import { getNumber } from '../../utils/helper';

const Question = (props) => {
    const [selectedAnswers, setSelectedAnswers] = useState({});

    const onChange = (type, id, value) => {
        let _value = value;
        if (type == 'checkbox') {
            let oldAnswer = selectedAnswers[id] || [];
            if (oldAnswer.includes(value)) {
                _value = oldAnswer.filter(data => data != value)
            } else {
                _value = [...oldAnswer, value];
            }
        }
        setSelectedAnswers({ ...selectedAnswers, [id]: _value })
    }

    const handleSubmit = () => {
        let totalScore = 0, scoreAchieved = 0;
        questions.forEach(quiestion => {
            const { type = '', _id = '', correctAnswer = '', weightage = '0' } = quiestion;
            const _value = selectedAnswers[_id];
            switch (type) {
                case "single_choice":
                    if (_value == correctAnswer) {
                        scoreAchieved += getNumber(weightage);
                    }
                    break;
                case "mul_choice":
                    if (_value && correctAnswer && isEqual(_value.sort(), correctAnswer.split(',').sort())) {
                        scoreAchieved += getNumber(weightage);
                    }
                    break;
            }
            totalScore += getNumber(weightage);
        });
        alert(`Scored ${scoreAchieved} out of ${totalScore}`);
    }

    const handleRestart = () => {
        setSelectedAnswers({});
    }
    const { questions = [] } = props;
    return (
        <Fragment>
            <Box className={'nav-card-content assessment-box'}>
                <Box>
                    {/* first question - single choice */}
                    {questions.map((question, index) => {
                        const { question: name = '', type = '', options = [], correctAnswer = '', _id = '' } = question;
                        switch (type) {
                            case "single_choice":
                                return (
                                    <Box m={3}>
                                        <Box className="question">
                                            {index + 1}. {name}
                                        </Box>
                                        {options.map(option => {
                                            return <Box m={2}>
                                                <RadioButton
                                                    name={_id}
                                                    group={_id}
                                                    displayLabel={option.option}
                                                    type="radio"
                                                    className="choice selected"
                                                    onChange={() => onChange("radio", _id, option.id)}
                                                    checked={selectedAnswers[_id] == option.id}
                                                />
                                            </Box>
                                        })}
                                    </Box>
                                );
                            case "mul_choice":
                                return (
                                    <Box m={3}>
                                        <Box className="question">
                                            {index + 1}. {name}
                                        </Box>
                                        {options.map(option => {
                                            return <Box m={2}>
                                                <CheckBox
                                                    name={_id}
                                                    value={option.id}
                                                    displayLabel={option.option}
                                                    type="checkbox"
                                                    className="choice selected"
                                                    onChange={() => onChange("checkbox", _id, option.id)}
                                                    checked={selectedAnswers[_id] && selectedAnswers[_id].includes(option.id)}
                                                />
                                            </Box>
                                        })}
                                    </Box>
                                );
                            default: return "";
                        }
                    })}
                </Box >
            </Box>
            <Flex mt={2} className="float-right">
                <Button name="Restart" className="primary-button-sm button-white" onClick={handleRestart} />
                <Spacer mr={2} />
                <Button name="Submit test" className="primary-button-sm" onClick={handleSubmit} />
            </Flex>
        </Fragment>

    );
};

export default Question;
