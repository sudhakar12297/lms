import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import FormComponent from '../../components/user/form';

//action
import { createUser, updateUser, fetchTeachers } from '../../action/account';
import { fetchGroups } from '../../action/group';
//reducer
import { getTeachers, getIsFormSubmitting } from '../../reducer/account';
import { getGroups } from '../../reducer/group';

const Create = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const { pathname } = location;

  const page = pathname.split('/').pop();
  const groups = useSelector(getGroups);
  const teachers = useSelector(getTeachers);
  const isFormSubmitting = useSelector(getIsFormSubmitting);

  useEffect(() => {
    dispatch(fetchGroups());
  }, [dispatch]);

  useEffect(() => {
    page === 'student' && dispatch(fetchTeachers());
  }, [page, dispatch]);

  const onsubmit = (data) => {
    const { isCreate, payload, id } = data;
    dispatch(isCreate ? createUser({ payload: { users: [payload] } }) : updateUser({ id, payload: { users: [payload] } }));
  };

  return (
    <FormComponent
      groups={groups}
      teachers={teachers}
      onsubmit={(data) => {
        onsubmit(data);
      }}
      isFormSubmitting={isFormSubmitting}
    />
  );
};

export default Create;
