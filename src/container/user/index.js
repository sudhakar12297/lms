import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';

//action
import { fetchUsersByRole, deleteUser } from '../../action/account';
//reducer
import { getUsers, getRole, getIsLoading, getList, getListFetching } from '../../reducer/account';
import { User, UserLoader } from '../../components/user';
import { Flex } from '../../components/asset/wrapper';
import RightPanel from '../layout/rightPanel';
import { MobileRightPanel } from '../layout/mobile';
import Form from './form';
import { ContentBox } from '../../components/layout';
import { toTitleCase } from '../../utils/helper';
import Popup from '../../components/asset/popup';
import Loader from '../../components/asset/loader';
import { Button } from '../../components/asset/button';
import { Close } from '../../components/asset/icons/close';
import { AddIcon } from '../../components/asset/icons/add';

const Users = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const role = location.pathname.split('/').pop();

  const loggedInUserRole = useSelector(getRole);
  const users = useSelector((state) => getList(state, role));
  const listLoading = useSelector((state) => getListFetching(state, role));
  const isLoading = useSelector(getIsLoading);

  useEffect(() => {
    dispatch(fetchUsersByRole({ role }));
  }, [dispatch, role]);

  const [show, setShow] = useState(false);
  const [deleteUserId, setDeleteUserId] = useState();

  const onClickEdit = (userObj) => {
    history.push({ pathname: `/user/${role}`, state: { userObj } });
  };

  const deleteConfirmed = () => {
    dispatch(deleteUser({ id: deleteUserId }));
    setShow(false);
  };

  const [showRightPanelOnMobile, setShowRightPanelOnMobile] = useState(false);
  const onClickToggleRightPanel = () => {
    setShowRightPanelOnMobile(!showRightPanelOnMobile);
  };
  return (
    <Flex>
      <ContentBox blur={showRightPanelOnMobile}>
        <User
          users={users}
          edit={(userObj) => onClickEdit(userObj)}
          remove={(id) => {
            setDeleteUserId(id);
            setShow(true);
          }}
          showEdit={
            loggedInUserRole !== 'student' && loggedInUserRole !== 'teacher'
          }
          showDelete={
            loggedInUserRole !== 'student' && loggedInUserRole !== 'teacher'
          }
          emptyMessage={`No ${toTitleCase(role)} Available`}
          isLoading={listLoading}
        />
      </ContentBox>
      <RightPanel>
        <Form />
      </RightPanel>
      <MobileRightPanel
        title="Group Creation"
        show={showRightPanelOnMobile ? 'visible' : 'hide'}
      >
        <Form />
      </MobileRightPanel>
      {/* </Box> */}
      {!showRightPanelOnMobile && (
        <Button
          name={<AddIcon />}
          title={'Create'}
          className={'mob-primary-button-form-'}
          onClick={onClickToggleRightPanel}
        />
      )}
      {showRightPanelOnMobile && (
        <Button
          name={<Close />}
          title={'Close'}
          className={'mob-primary-button-form-close'}
          onClick={onClickToggleRightPanel}
        />
      )}
      <Popup
        show={show}
        cancel={() => setShow(false)}
        confirm={deleteConfirmed}
      />
    </Flex>
  );
};

export default Users;
