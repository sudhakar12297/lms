import {
  GET_ADMINS_SUCCESS,
  GET_TEACHERS_SUCCESS,
  GET_STUDENTS_SUCCESS,
} from '../action/defined/account';

const initialState = {
  isError: false,
  isLoading: false,
  isLoggedIn: false,
  isFormSubmitted: false,
  isFormSubmitting: false,
  user: {},
  users: [],
  admins: [],
  teachers: [],
  students: [],
  adminFetching: false,
  teacherFetching: false,
  studentFetching: false,
  adminList: [],
  teacherList: [],
  studentList: [],
};

export const account = (state = initialState, action) => {
  switch (action.type) {
    case 'UNSET_USER_IS_FORM_SUBMITTED':
      return {
        ...state,
        isFormSubmitted: false,
      };
    case 'UNSET_USER_IS_LOADING':
      return {
        ...state,
        isLoading: false,
      };
    case 'LOGIN_START':
      return {
        ...state,
        isError: false,
        isLoading: true,
      };
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isError: false,
        isLoading: false,
        isLoggedIn: true,
        user: action.data.user,
      };
    case 'LOGIN_FAIL':
      return {
        ...state,
        isError: true,
        isLoading: false,
      };
    case 'USER_START':
      return {
        ...state,
        isError: false,
        isLoading: true,
        isFormSubmitted: false,
      };
    case 'CREATE_USER_START':
    case 'UPDATE_USER_START':
      return {
        ...state,
        isError: false,
        isFormSubmitting: true,
      };
    case 'USER_FAIL':
      return {
        ...state,
        isError: true,
        isLoading: false,
        isFormSubmitted: false,
        isFormSubmitting: false,
      };
    case 'USER_CREATE_SUCCESS':
      return {
        ...state,
        isError: false,
        isLoading: false,
        isFormSubmitted: true,
        isFormSubmitting: false,
      };
    case 'USER_UPDATE_SUCCESS':
      return {
        ...state,
        isError: false,
        isLoading: false,
        isFormSubmitted: true,
        isFormSubmitting: false,
      };
    case 'USERS_SUCCESS':
      return {
        ...state,
        isError: false,
        isLoading: false,
        users: action.users,
      };
    case GET_ADMINS_SUCCESS:
      return {
        ...state,
        isError: false,
        isLoading: false,
        admins: action.admins,
      };
    case GET_TEACHERS_SUCCESS:
      return {
        ...state,
        isError: false,
        isLoading: false,
        teachers: action.teachers,
      };
    case GET_STUDENTS_SUCCESS:
      return {
        ...state,
        isError: false,
        isLoading: false,
        students: action.students,
      };
    case "RESET_LOADER":
      return {
        ...state,
        isFormSubmitting: false,
        isLoading: false
      }

    // List cases
    case "USERS_LIST_FETHCING":
    case "USER_LIST_FAIL":
    case "USERS_LIST_SUCCESS":
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
};

export const getIsLoading = (state) => state.account.isLoading;
export const getIsAccountLoading = (state) => state.account.isLoading;
export const getIsError = (state) => state.account.isError;
export const getIsLoggedIn = (state) => state.account.isLoggedIn;
export const getUser = (state) => state.account.user;
export const getRole = (state) => state.account.user?.role;

export const getUsers = (state) => state.account.users;
export const getAdmins = (state) => state.account.admins;
export const getTeachers = (state) => state.account.teachers;
export const getStudents = (state) => state.account.students;
export const getIsFormSubmitted = (state) => state.account.isFormSubmitted;
export const getIsFormSubmitting = (state) => state.account.isFormSubmitting;


export const getList = (state, role) => state.account[role + 'List'];
export const getListFetching = (state, role) => state.account[role + 'Fetching'];