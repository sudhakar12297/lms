import { SET_GROUP, SET_SUBJECT, SET_LESSON, SET_TOPIC } from '../action/defined/common';

const initialState = {
  group: {},
  subject: {},
  lesson: {},
  topic: {},
  homeworks: [],
  userOnboardTimeInStudyTopic: ''
};
export const common = (state = initialState, action) => {
  switch (action.type) {
    case SET_GROUP:
      return { ...state, group: action.data };
    case SET_SUBJECT:
      return { ...state, subject: action.data };
    case SET_LESSON:
      return { ...state, lesson: action.data };
    case SET_TOPIC:
      return { ...state, topic: action.data };
    case 'SET_ONGOING_HOMEWORKS_CONTAINS_CURRENT_TOPIC':
      return { ...state, homeworks: action.homeworks };
    case 'SET_USER_ONBOARD_TIME':
      return { ...state, userOnboardTimeInStudyTopic: action.moment };
    case 'UNSET_ONGOING_HOMEWORKS_CONTAINS_CURRENT_TOPIC':
      return { ...state, homeworks: [] };
    case 'UNSET_USER_ONBOARD_TIME':
      return { ...state, userOnboardTimeInStudyTopic: '' };
    default:
      return state;
  }
};

export const getSelectedGroup = (state) => state.common.group;
export const getSelectedSubject = (state) => state.common.subject;
export const getSelectedLesson = (state) => state.common.lesson;
export const getSelectedTopic = (state) => state.common.topic;

export const getHomeWorks = (state) => state.common.homeworks;
export const getUserOnboardTimeInStudyTopic = (state) => state.common.userOnboardTimeInStudyTopic;

