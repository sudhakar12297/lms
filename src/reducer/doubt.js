import {
    GET_DOUBTS_START,
    GET_DOUBTS_SUCCESS,
    GET_DOUBTS_FAIL,
    POST_DOUBT_START,
    POST_DOUBT_SUCCESS,
    POST_DOUBT_FAIL,
    UPDATE_DOUBT_START,
    UPDATE_DOUBT_SUCCESS,
    UPDATE_DOUBT_FAIL,
    DELETE_DOUBT_START,
    DELETE_DOUBT_SUCCESS,
    DELETE_DOUBT_FAIL,
} from '../action/defined/doubt';

const initialState = {
    isLoading: false,
    isError: false,
    doubts: [],
    myDoubts: [],
};
export const doubt = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_MY_DOUBTS_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isError: false,
                myDoubts: action.doubts,
            };
        case GET_DOUBTS_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
                doubts: []
            };
        case GET_DOUBTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                doubts: action.doubts,
            };
        case GET_DOUBTS_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case POST_DOUBT_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case POST_DOUBT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
            };
        case POST_DOUBT_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case UPDATE_DOUBT_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case UPDATE_DOUBT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
            };
        case UPDATE_DOUBT_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case DELETE_DOUBT_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case DELETE_DOUBT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
            };
        case DELETE_DOUBT_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        default:
            return state;
    }
};

export const getIsLoading = (state) => state.doubt.isLoading;
export const getIsError = (state) => state.doubt.isError;

export const getDoubts = (state) => state.doubt.doubts;
export const getMyDoubts = (state) => state.doubt.myDoubts;