const initialState = {
  isError: false,
  isLoading: false,
  isGroupCreated: false,
  isGroupUpdated: false,
  isFormSubmitted: false,
  isFormSubmitting: false,
  myGroups: [],
  groups: [],
};

export const group = (state = initialState, action) => {
  switch (action.type) {
    case 'UNSET_GROUP_IS_FORM_SUBMITTED':
      return {
        ...state,
        isFormSubmitted: false,
      };
    case 'GROUP_START':
      return {
        ...state,
        isLoading: true,
        isError: false,
        isFormSubmitted: false,
      };
    case 'GET_MY_GROUPS_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        myGroups: action.groups,
      };
    case 'DELETE_GROUP_START':
      return {
        ...state,
        isError: false,
        isFormSubmitted: false,
      };
    case 'CREATE_GROUP_START':
    case 'UPDATE_GROUP_START':
      return {
        ...state,
        isError: false,
        isFormSubmitted: false,
        isFormSubmitting: true,
      };
    case 'GROUP_FAIL':
      return {
        ...state,
        isLoading: false,
        isError: true,
        isFormSubmitting: false,
        isFormSubmitted: false,
      };
    case 'GROUP_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
      };
    case 'POST_GROUP_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        isFormSubmitting: false,
        isFormSubmitted: true,
      };
    case 'GET_GROUPS_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        groups: action.data.groups,
      };
    case 'GET_GROUP_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        group: action.data,
      };
    case 'PUT_GROUP_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        isFormSubmitting: false,
        isFormSubmitted: true,
      };
    case 'DELETE_GROUP_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        isFormSubmitting: false,
        groups: action.groups,
      };
    case "RESET_LOADER":
      return {
        ...state,
        isFormSubmitting: false,
        isLoading: false
      }
    default:
      return state;
  }
};

export const getIsLoading = (state) => state.group.isLoading;
export const getIsGroupLoading = (state) => state.group.isLoading;
export const getIsError = (state) => state.group.isError;
export const getIsGroupCreated = (state) => state.group.isGroupCreated;
export const getIsGroupUpdated = (state) => state.group.isGroupUpdated;
export const getIsFormSubmitted = (state) => state.group.isFormSubmitted;

export const getMyGroups = (state) => state.group.myGroups;
export const getGroups = (state) => state.group.groups;
export const getGroup = (state) => state.group.group;
export const getIsFormSubmitting = (state) => state.group.isFormSubmitting;
