import {
    GET_HOMEWORKS_START,
    GET_HOMEWORKS_SUCCESS,
    GET_HOMEWORKS_FAIL,
    POST_HOMEWORK_START,
    POST_HOMEWORK_SUCCESS,
    POST_HOMEWORK_FAIL,
    UPDATE_HOMEWORK_START,
    UPDATE_HOMEWORK_SUCCESS,
    UPDATE_HOMEWORK_FAIL,
    DELETE_HOMEWORK_START,
    DELETE_HOMEWORK_SUCCESS,
    DELETE_HOMEWORK_FAIL,
} from '../action/defined/homework';

const initialState = {
    isLoading: false,
    isError: false,
    isFormSubmiting: false,
    isFormFailed: false,
    isFormSubmitted: false,
    homeworks: [],
    homework: {},
};
export const homework = (state = initialState, action) => {
    switch (action.type) {
        case 'UNSET_HOMEWORK_IS_FORM_SUBMITTED':
            return {
                ...state,
                isFormSubmitted: false,
            };
        case GET_HOMEWORKS_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case GET_HOMEWORKS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                homeworks: action.homeworks,
            };
        case GET_HOMEWORKS_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case POST_HOMEWORK_START:
            return {
                ...state,
                isFormSubmiting: true,
                isFormFailed: false,
                isFormSubmitted: false,
            };
        case POST_HOMEWORK_SUCCESS:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: false,
                isFormSubmitted: true,
            };
        case POST_HOMEWORK_FAIL:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: true,
                isFormSubmitted: false,
            };
        case UPDATE_HOMEWORK_START:
            return {
                ...state,
                isFormSubmiting: true,
                isFormFailed: false,
                isFormSubmitted: false,
            };
        case UPDATE_HOMEWORK_SUCCESS:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: false,
                isFormSubmitted: true,
            };
        case UPDATE_HOMEWORK_FAIL:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: true,
                isFormSubmitted: false,
            };
        case DELETE_HOMEWORK_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case DELETE_HOMEWORK_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                homeworks: action.homeworks
            };
        case DELETE_HOMEWORK_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        default:
            return state;
    }
};

export const getIsHomeworkLoading = (state) => state.homework.isLoading;
export const getIsError = (state) => state.homework.isError;

export const getIsFormSubmiting = (state) => state.homework.isFormSubmiting;
export const getIsFormFailed = (state) => state.homework.isFormFailed;
export const getIsFormSubmitted = (state) => state.homework.isFormSubmitted;

export const getHomeworks = (state) => state.homework.homeworks;
export const getHomework = (state) => state.homework.homework;
