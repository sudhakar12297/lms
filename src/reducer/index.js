import { combineReducers } from 'redux';
import { common } from './common';
import { account } from './account';
import { group } from './group';
import { subject } from './subject';
import { lesson } from './lesson';
import { topic } from './topic';
import { note } from './note';
import { doubt } from './doubt';
import { test } from './test';
import { question } from './question';
import { live } from './live';
import { homework } from './homework';

const appReducer = combineReducers({
  account,
  common,
  group,
  subject,
  lesson,
  topic,
  note,
  doubt,
  test,
  question,
  live,
  homework
});

const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT') {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
