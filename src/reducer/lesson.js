import {
  GET_LESSONS_START,
  GET_LESSONS_SUCCESS,
  GET_LESSONS_FAIL,
  POST_LESSON_START,
  POST_LESSON_SUCCESS,
  POST_LESSON_FAIL,
  UPDATE_LESSON_START,
  UPDATE_LESSON_SUCCESS,
  UPDATE_LESSON_FAIL,
  DELETE_LESSON_START,
  DELETE_LESSON_SUCCESS,
  DELETE_LESSON_FAIL,
} from '../action/defined/lesson';

const initialState = {
  isLoading: false,
  isError: false,
  isCreating: false,
  isUpdating: false,
  isFormFailed: false,
  isFormSubmitted: false,
  isFormSubmitting: false,
  lessons: [],
  lesson: {},
};
export const lesson = (state = initialState, action) => {
  switch (action.type) {
    case 'UNSET_LESSON_IS_FORM_SUBMITTED':
      return {
        ...state,
        isFormSubmitted: false,
      };
    case GET_LESSONS_START:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case GET_LESSONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        lessons: action.lessons,
      };
    case GET_LESSONS_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    case POST_LESSON_START:
      return {
        ...state,
        isCreating: true,
        isFormFailed: false,
        isFormSubmitting: true,
        isFormSubmitted: false,
      };
    case POST_LESSON_SUCCESS:
      return {
        ...state,
        isCreating: false,
        isFormFailed: false,
        isFormSubmitting: false,
        isFormSubmitted: true,
      };
    case POST_LESSON_FAIL:
      return {
        ...state,
        isCreating: false,
        isFormFailed: true,
        isFormSubmitting: false,
        isFormSubmitted: false,
      };
    case UPDATE_LESSON_START:
      return {
        ...state,
        isUpdating: true,
        isFormFailed: false,
        isFormSubmitting: true,
        isFormSubmitted: false,
      };
    case UPDATE_LESSON_SUCCESS:
      return {
        ...state,
        isUpdating: false,
        isFormFailed: false,
        isFormSubmitting: false,
        isFormSubmitted: true,
      };
    case UPDATE_LESSON_FAIL:
      return {
        ...state,
        isCreating: false,
        isFormFailed: true,
        isFormSubmitting: false,
        isFormSubmitted: false,
      };
    case DELETE_LESSON_START:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case DELETE_LESSON_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
      };
    case DELETE_LESSON_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    case "RESET_LOADER":
      return {
        ...state,
        isFormSubmitting: false,
        isLoading: false
      }
    default:
      return state;
  }
};

export const getIsLoading = (state) => state.lesson.isLoading;
export const getIsError = (state) => state.lesson.isError;
export const getLessonState = (state) => state.lesson;

export const getIsCreating = (state) => state.lesson.isCreating;
export const getIsFormFailed = (state) => state.lesson.isFormFailed;
export const getIsFormSubmitted = (state) => state.lesson.isFormSubmitted;
export const getIsFormSubmitting = (state) => state.lesson.isFormSubmitting;

export const getLessons = (state) => state.lesson.lessons;
export const getLesson = (state) => state.lesson.lesson;
