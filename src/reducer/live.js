import {
    GET_LIVES_START,
    GET_LIVES_SUCCESS,
    GET_LIVES_FAIL,
    POST_LIVE_START,
    POST_LIVE_SUCCESS,
    POST_LIVE_FAIL,
    UPDATE_LIVE_START,
    UPDATE_LIVE_SUCCESS,
    UPDATE_LIVE_FAIL,
    DELETE_LIVE_START,
    DELETE_LIVE_SUCCESS,
    DELETE_LIVE_FAIL,
} from '../action/defined/live';

const initialState = {
    isLoading: false,
    isError: false,
    isCreating: false,
    isUpdating: false,
    isFormFailed: false,
    isFormSubmitted: false,
    isFormSubmitting: false,
    lives: [],
    live: {},
};
export const live = (state = initialState, action) => {
    switch (action.type) {
        case 'UNSET_LIVE_IS_FORM_SUBMITTED':
            return {
                ...state,
                isFormSubmitted: false,
            };
        case GET_LIVES_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case GET_LIVES_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                lives: action.lives,
            };
        case GET_LIVES_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case POST_LIVE_START:
            return {
                ...state,
                isCreating: true,
                isFormFailed: false,
                isFormSubmitting: true,
                isFormSubmitted: false,
            };
        case POST_LIVE_SUCCESS:
            return {
                ...state,
                isCreating: false,
                isFormFailed: false,
                isFormSubmitting: false,
                isFormSubmitted: true,
            };
        case POST_LIVE_FAIL:
            return {
                ...state,
                isCreating: false,
                isFormFailed: true,
                isFormSubmitting: false,
                isFormSubmitted: false,
            };
        case UPDATE_LIVE_START:
            return {
                ...state,
                isUpdating: true,
                isFormFailed: false,
                isFormSubmitting: true,
                isFormSubmitted: false,
            };
        case UPDATE_LIVE_SUCCESS:
            return {
                ...state,
                isUpdating: false,
                isFormFailed: false,
                isFormSubmitting: false,
                isFormSubmitted: true,
            };
        case UPDATE_LIVE_FAIL:
            return {
                ...state,
                isCreating: false,
                isFormFailed: true,
                isFormSubmitting: false,
                isFormSubmitted: false,
            };
        case DELETE_LIVE_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case DELETE_LIVE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                lives: action.lives
            };
        case DELETE_LIVE_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case "RESET_LOADER":
            return {
                ...state,
                isFormSubmitting: false,
                isLoading: false
            }

        default:
            return state;
    }
};

export const getIsLoading = (state) => state.live.isLoading;
export const getIsLiveLoading = (state) => state.live.isLoading;
export const getIsError = (state) => state.live.isError;
export const getLiveState = (state) => state.live;

export const getIsCreating = (state) => state.live.isCreating;
export const getIsFormFailed = (state) => state.live.isFormFailed;
export const getIsFormSubmitted = (state) => state.live.isFormSubmitted;
export const getIsFormSubmitting = (state) => state.live.isFormSubmitting;

export const getLives = (state) => state.live.lives;
export const getLive = (state) => state.live.live;
