import {
    GET_NOTE_START,
    GET_NOTE_SUCCESS,
    GET_NOTE_FAIL,
    UPDATE_NOTE_START,
    UPDATE_NOTE_SUCCESS,
    UPDATE_NOTE_FAIL,
} from '../action/defined/note';

const initialState = {
    isLoading: false,
    isError: false,
    note: '',
    myNotes: []
};
export const note = (state = initialState, action) => {
    switch (action.type) {
        case 'ON_CHANGE_NOTE':
            return {
                ...state,
                isLoading: false,
                isError: false,
                note: action.note,
            };
        case 'GET_MY_NOTES_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isError: false,
                myNotes: action.notes,
            };
        case GET_NOTE_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
                note: ''
            };
        case GET_NOTE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                note: action.note,
            };
        case GET_NOTE_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case UPDATE_NOTE_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case UPDATE_NOTE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
            };
        case UPDATE_NOTE_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        default:
            return state;
    }
};

export const getIsLoading = (state) => state.note.isLoading;
export const getIsError = (state) => state.note.isError;
export const getNote = (state) => state.note.note;
export const getMyNotes = (state) => state.note.myNotes;
