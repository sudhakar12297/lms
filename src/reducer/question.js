import {
    GET_QUESTIONS_START,
    GET_QUESTIONS_SUCCESS,
    GET_QUESTIONS_FAIL,
    POST_QUESTION_START,
    POST_QUESTION_SUCCESS,
    POST_QUESTION_FAIL,
    UPDATE_QUESTION_START,
    UPDATE_QUESTION_SUCCESS,
    UPDATE_QUESTION_FAIL,
    DELETE_QUESTION_START,
    DELETE_QUESTION_SUCCESS,
    DELETE_QUESTION_FAIL,
} from '../action/defined/question';

const initialState = {
    isLoading: false,
    isError: false,
    isFormSubmiting: false,
    isFormFailed: false,
    isFormSubmitted: false,
    questions: [],
    question: {},
};
export const question = (state = initialState, action) => {
    switch (action.type) {
        case 'UNSET_QUESTION_IS_FORM_SUBMITTED':
            return {
                ...state,
                isFormSubmitted: false,
            };
        case GET_QUESTIONS_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case GET_QUESTIONS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                questions: action.questions,
            };
        case GET_QUESTIONS_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case POST_QUESTION_START:
            return {
                ...state,
                isFormSubmiting: true,
                isFormFailed: false,
                isFormSubmitted: false,
            };
        case POST_QUESTION_SUCCESS:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: false,
                isFormSubmitted: true,
            };
        case POST_QUESTION_FAIL:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: true,
                isFormSubmitted: false,
            };
        case UPDATE_QUESTION_START:
            return {
                ...state,
                isFormSubmiting: true,
                isFormFailed: false,
                isFormSubmitted: false,
            };
        case UPDATE_QUESTION_SUCCESS:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: false,
                isFormSubmitted: true,
            };
        case UPDATE_QUESTION_FAIL:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: true,
                isFormSubmitted: false,
            };
        case DELETE_QUESTION_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case DELETE_QUESTION_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
            };
        case DELETE_QUESTION_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        default:
            return state;
    }
};

export const getIsQuestionLoading = (state) => state.question.isLoading;
export const getIsError = (state) => state.question.isError;
export const getQuestionState = (state) => state.question;

export const getIsFormSubmiting = (state) => state.question.isFormSubmiting;
export const getIsFormFailed = (state) => state.question.isFormFailed;
export const getIsFormSubmitted = (state) => state.question.isFormSubmitted;

export const getQuestions = (state) => state.question.questions;
export const getQuestion = (state) => state.question.question;
