import {
  GET_SUBJECTS_START,
  GET_SUBJECTS_SUCCESS,
  GET_SUBJECTS_FAIL,
  POST_SUBJECT_START,
  POST_SUBJECT_SUCCESS,
  POST_SUBJECT_FAIL,
  UPDATE_SUBJECT_START,
  UPDATE_SUBJECT_SUCCESS,
  UPDATE_SUBJECT_FAIL,
  DELETE_SUBJECT_START,
  DELETE_SUBJECT_SUCCESS,
  DELETE_SUBJECT_FAIL,
} from '../action/defined/subject';

const initialState = {
  isLoading: false,
  isError: false,
  isCreating: false,
  isUpdating: false,
  isFormFailed: false,
  isFormSubmitted: false,
  isFormSubmitting: false,
  mySubjects: [],
  subjects: [],
  subject: {},
};
export const subject = (state = initialState, action) => {
  switch (action.type) {
    case 'UNSET_SUBJECT_IS_FORM_SUBMITTED':
      return {
        ...state,
        isFormSubmitted: false,
      };
    case GET_SUBJECTS_START:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case GET_SUBJECTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        subjects: action.subjects,
      };
    case GET_SUBJECTS_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    case 'GET_MY_SUBJECTS_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        mySubjects: action.subjects,
      };
    case POST_SUBJECT_START:
      return {
        ...state,
        isCreating: true,
        isFormFailed: false,
        isFormSubmitting: true,
        isFormSubmitted: false,
      };
    case POST_SUBJECT_SUCCESS:
      return {
        ...state,
        isCreating: false,
        isFormFailed: false,
        isFormSubmitting: false,
        isFormSubmitted: true,
      };
    case POST_SUBJECT_FAIL:
      return {
        ...state,
        isCreating: false,
        isFormFailed: true,
        isFormSubmitting: false,
        isFormSubmitted: false,
      };
    case UPDATE_SUBJECT_START:
      return {
        ...state,
        isUpdating: true,
        isFormFailed: false,
        isFormSubmitting: true,
        isFormSubmitted: false,
      };
    case UPDATE_SUBJECT_SUCCESS:
      return {
        ...state,
        isUpdating: false,
        isFormFailed: false,
        isFormSubmitting: false,
        isFormSubmitted: true,
      };
    case UPDATE_SUBJECT_FAIL:
      return {
        ...state,
        isCreating: false,
        isFormFailed: true,
        isFormSubmitting: false,
        isFormSubmitted: false,
      };
    case DELETE_SUBJECT_START:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case DELETE_SUBJECT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
      };
    case DELETE_SUBJECT_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    case "RESET_LOADER":
      return {
        ...state,
        isFormSubmitting: false,
        isLoading: false
      }

    default:
      return state;
  }
};

export const getIsLoading = (state) => state.subject.isLoading;
export const getIsSubjectLoading = (state) => state.subject.isLoading;
export const getIsError = (state) => state.subject.isError;
export const getSubjectState = (state) => state.subject;

export const getIsCreating = (state) => state.subject.isCreating;
export const getIsFormFailed = (state) => state.subject.isFormFailed;
export const getIsFormSubmitted = (state) => state.subject.isFormSubmitted;
export const getIsFormSubmitting = (state) => state.subject.isFormSubmitting;

export const getMySubjects = (state) => state.subject.mySubjects;
export const getSubjects = (state) => state.subject.subjects;
export const getSubject = (state) => state.subject.subject;
