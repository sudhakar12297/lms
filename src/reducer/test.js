import {
    GET_TESTS_START,
    GET_TESTS_SUCCESS,
    GET_TESTS_FAIL,
    POST_TEST_START,
    POST_TEST_SUCCESS,
    POST_TEST_FAIL,
    UPDATE_TEST_START,
    UPDATE_TEST_SUCCESS,
    UPDATE_TEST_FAIL,
    DELETE_TEST_START,
    DELETE_TEST_SUCCESS,
    DELETE_TEST_FAIL,
    MAP_QUESTION_START,
    MAP_QUESTION_SUCCESS,
    MAP_QUESTION_FAIL,
    SUBMITTED_RESULT_START,
    SUBMITTED_RESULT_SUCCESS,
    SUBMITTED_RESULT_FAIL,
    UPDATE_RESULT_START,
    UPDATE_RESULT_SUCCESS,
    UPDATE_RESULT_FAIL,
    REDIRECT_TEST,
    RESET_REDIRECT_TEST,
    RESET_QUESION_MAP_RESPONSE,
} from '../action/defined/test';

const initialState = {
    isLoading: false,
    isError: false,
    isFormSubmiting: false,
    isFormFailed: false,
    isFormSubmitted: false,
    tests: [],
    test: {},
    loadingTestResults: true,
    testResults: [],
    redirectPayload: {},
    questionMapPayload: {},
};
export const test = (state = initialState, action) => {
    switch (action.type) {
        case 'UNSET_TEST_IS_FORM_SUBMITTED':
            return {
                ...state,
                isFormSubmitted: false,
            };
        case GET_TESTS_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case GET_TESTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                tests: action.tests,
            };
        case GET_TESTS_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case POST_TEST_START:
            return {
                ...state,
                isFormSubmiting: true,
                isFormFailed: false,
                isFormSubmitted: false,
            };
        case POST_TEST_SUCCESS:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: false,
                isFormSubmitted: true,
            };
        case POST_TEST_FAIL:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: true,
                isFormSubmitted: false,
            };
        case UPDATE_TEST_START:
            return {
                ...state,
                isFormSubmiting: true,
                isFormFailed: false,
                isFormSubmitted: false,
            };
        case UPDATE_TEST_SUCCESS:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: false,
                isFormSubmitted: true,
            };
        case UPDATE_TEST_FAIL:
            return {
                ...state,
                isFormSubmiting: false,
                isFormFailed: true,
                isFormSubmitted: false,
            };
        case DELETE_TEST_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case DELETE_TEST_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
            };
        case DELETE_TEST_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case MAP_QUESTION_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case MAP_QUESTION_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                questionMapPayload: action.payload
            };
        case RESET_QUESION_MAP_RESPONSE:
            return {
                ...state,
                questionMapPayload: {}
            };
        case MAP_QUESTION_FAIL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case SUBMITTED_RESULT_START:
            return {
                ...state,
                testResults: [],
                loadingTestResults: true
            }
        case SUBMITTED_RESULT_SUCCESS:
            return {
                ...state,
                testResults: action.data,
                loadingTestResults: false
            }
        case SUBMITTED_RESULT_FAIL:
            return {
                ...state,
                testResults: [],
                loadingTestResults: false
            }
        case UPDATE_RESULT_START:
            return {
                ...state,

            }
        case UPDATE_RESULT_SUCCESS:
            return {
                ...state,

            }
        case UPDATE_RESULT_FAIL:
            return {
                ...state,

            }
        case REDIRECT_TEST:
            return {
                ...state,
                redirectPayload: action.payload,
            }
        case RESET_REDIRECT_TEST:
            return {
                ...state,
                redirectPayload: {},
            }
        default:
            return state;
    }
};

export const getIsTestLoading = (state) => state.test.isLoading;
export const getIsError = (state) => state.test.isError;
export const getTestState = (state) => state.test;

export const getIsFormSubmiting = (state) => state.test.isFormSubmiting;
export const getIsFormFailed = (state) => state.test.isFormFailed;
export const getIsFormSubmitted = (state) => state.test.isFormSubmitted;

export const getTests = (state) => state.test.tests;
export const getTest = (state) => state.test.test;

export const getSubmittiedResults = (state) => state.test.testResults;
export const getloadingTestResults = (state) => state.test.loadingTestResults;

export const getRedirectPayload = (state) => state.test.redirectPayload;
export const getQuestionMapPayload = (state) => state.test.questionMapPayload;