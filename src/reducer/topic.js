import {
  GET_TOPIC_START,
  GET_TOPIC_SUCCESS,
  GET_TOPIC_FAIL,
  POST_TOPIC_START,
  POST_TOPIC_SUCCESS,
  POST_TOPIC_FAIL,
  UPDATE_TOPIC_START,
  UPDATE_TOPIC_SUCCESS,
  UPDATE_TOPIC_FAIL,
  DELETE_TOPIC_START,
  DELETE_TOPIC_SUCCESS,
  DELETE_TOPIC_FAIL,
} from '../action/defined/topic';

const initialState = {
  isLoading: false,
  isError: false,
  isCreating: false,
  isUpdating: false,
  isFormFailed: false,
  isFormSubmitted: false,
  topics: [],
  topic: {},
};
export const topic = (state = initialState, action) => {
  switch (action.type) {
    case 'UNSET_TOPIC':
      return {
        ...state,
        topic: {}
      }
    case 'UNSET_TOPIC_IS_FORM_SUBMITTED':
      return {
        ...state,
        isFormSubmitted: false,
      };
    case GET_TOPIC_START:
      return {
        ...state,
        isLoading: true,
        isError: false,
        topic: {},
      };
    case GET_TOPIC_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        topic: action.topic,
      };
    case GET_TOPIC_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    case POST_TOPIC_START:
      return {
        ...state,
        isCreating: true,
        isFormFailed: false,
        isFormSubmitted: false,
      };
    case POST_TOPIC_SUCCESS:
      return {
        ...state,
        isCreating: false,
        isFormFailed: false,
        isFormSubmitted: true,
      };
    case POST_TOPIC_FAIL:
      return {
        ...state,
        isCreating: false,
        isFormFailed: true,
        isFormSubmitted: false,
      };
    case UPDATE_TOPIC_START:
      return {
        ...state,
        isUpdating: true,
        isFormFailed: false,
        isFormSubmitted: false,
      };
    case UPDATE_TOPIC_SUCCESS:
      return {
        ...state,
        isUpdating: false,
        isFormFailed: false,
        isFormSubmitted: true,
      };
    case UPDATE_TOPIC_FAIL:
      return {
        ...state,
        isCreating: false,
        isFormFailed: true,
        isFormSubmitted: false,
      };
    case DELETE_TOPIC_START:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case DELETE_TOPIC_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
      };
    case DELETE_TOPIC_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};

export const getIsLoading = (state) => state.topic.isLoading;
export const getIsError = (state) => state.topic.isError;
export const getTopicState = (state) => state.topic;

export const getIsCreating = (state) => state.topic.isCreating;
export const getIsFormFailed = (state) => state.topic.isFormFailed;
export const getIsFormSubmitted = (state) => state.topic.isFormSubmitted;

export const getTopics = (state) => state.topic.topics;
export const getTopic = (state) => state.topic.topic;
