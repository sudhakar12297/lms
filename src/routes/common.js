import React, { useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import jwt from "jsonwebtoken";
import Cookies from "js-cookie";
import { Header, MainBox, DisplayBox } from '../components/layout';
import LeftNav from '../container/layout/leftNav';

import { MobileLeftNav } from '../container/layout/mobile';

//reducer
import { getRole } from '../reducer/account';
import { setUserData } from '../action/account';

export const UNAUTHORIZED = <p>You are not aucookiesthorized to access this page</p>;

export const Protect = (props) => {
  const dispatch = useDispatch()
  const loggedInUserRole = useSelector(getRole);
  const [showMobileNav, setMobileNav] = useState(false);

  const handleMenuClick = () => {
    setMobileNav(true);
  };

  const closeNavBar = () => {
    setMobileNav(false);
  };

  const ref = useRef(null);
  const token = Cookies.get('token') || Cookies.get('user');
  if (!token) {
    return <Redirect to="/login" />;
  } else {
    const user = jwt.verify(token, 'my_secret_key');
    dispatch(setUserData({ user }));
    return (
      <Route exact path={props.path}>
        <MainBox>
          <MobileLeftNav
            closeNavBar={closeNavBar}
            className={showMobileNav ? 'visible' : 'hide'}
          />
          <LeftNav />
          <DisplayBox>
            <Header
              refs={ref}
              institute_name="Demo School of Institution"
              person_name={user?.firstName}
              role={loggedInUserRole}
              menuHandler={handleMenuClick}
            />
            {props.component}
          </DisplayBox>
        </MainBox>
      </Route>
    );
  }
};
