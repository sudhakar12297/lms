import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import { getRole } from '../reducer/account';
import { resetLoaders } from './../action/common';
import { ProtectedRouters } from './protected-routers';
import Designs from '../components/designs';
import Login from '../container/login';
import { VideoCall } from '../container/video-call';

export default function VicRouter() {
  const role = useSelector(getRole);
  const dispatch = useDispatch();

  const cleanupOnRefresh = (e) => {
    dispatch(resetLoaders());
  }

  useEffect(() => {
    window.addEventListener('beforeunload', cleanupOnRefresh);
    return () => {
      cleanupOnRefresh();
      window.removeEventListener('beforeunload', cleanupOnRefresh);
    }
  }, []);
  return (
    <Router>
      <Switch>
        <Route
          exact
          path="/"
          name="home"
          render={() => <Redirect to="/login" />}
        />
        <Route exact path="/designs">
          <Designs />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/video-call">
          <VideoCall />
        </Route>
        <ProtectedRouters role={role} />
        <Route path="">
          <p>We do not have this pages</p>
        </Route>
      </Switch>
    </Router>
  );
}
