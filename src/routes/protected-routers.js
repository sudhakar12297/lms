import React from 'react';
import { UNAUTHORIZED, Protect } from './common';
import ViewUser from '../container/user';
import { ROLE, ROLES } from '../utils/common';
import ViewGroup from '../container/group';
import Dashboard from '../container/dashboard';
import Subject from '../container/subject';
import Lesson from '../container/lesson';
import TopicCreate from '../container/topic/form';
import TopicStudy from '../container/topic/studyTopic';
import Test from '../container/test';
import TestDetail from '../container/test/detail';
import QuestionCreate from '../container/test/create-question';
import AttendTest from '../container/test/question';
import Evaluate from '../container/test/evaluate';
import Live from '../container/live';
import LiveVideo from '../container/live/video';
import Homework from '../container/homework';

import MyDoubts from '../container/doubt';
import MyNotes from '../container/note';
//temp
import TopicDetail from '../components/topicDetail';

const Temp = [
  {
    path: '/detail',
    component: <TopicDetail />,
    roles: ['super-admin', 'admin', 'teacher', 'student'],
  },
];

const userRouters = [
  {
    path: '/user/admin',
    component: <ViewUser user={ROLE.admin} />,
    roles: ['super-admin'],
  },
  {
    path: '/user/teacher',
    component: <ViewUser user={ROLE.teacher} />,
    roles: ['super-admin', 'admin'],
  },
  {
    path: '/user/student',
    component: <ViewUser user={ROLE.student} />,
    roles: ['super-admin', 'admin'],
  },
];

const GroupRouters = [
  {
    path: '/group',
    component: <ViewGroup />,
    roles: ['super-admin', 'admin', 'teacher'],
  },
];

const DashboardRouters = [
  {
    path: '/dashboard',
    component: <Dashboard />,
    roles: ['super-admin', 'admin', 'teacher', 'student'],
  },
];

const SubjectRouters = [
  {
    path: '/subject',
    component: <Subject />,
    roles: ['super-admin', 'admin', 'teacher', 'student'],
  },
];

const LessonRouters = [
  {
    path: '/lesson',
    component: <Lesson />,
    roles: ['super-admin', 'admin', 'teacher', 'student'],
  },
];

const TopicRouters = [
  {
    path: '/topic/create',
    component: <TopicCreate />,
    roles: ['super-admin', 'admin', 'teacher'],
  },
  {
    path: '/topic/study',
    component: <TopicStudy />,
    roles: ['super-admin', 'admin', 'teacher', 'student'],
  },
];

const TestRouters = [
  {
    path: '/test',
    component: <Test />,
    // isOnlyAdminAccess: true,
    roles: ['super-admin', 'admin', 'teacher', 'student'],
  },
  {
    path: '/test/detail',
    component: <TestDetail />,
    roles: ['teacher'],
  },
  {
    path: '/test/question/create',
    component: <QuestionCreate />,
    roles: ['teacher'],
  },
  {
    path: '/test/attend',
    component: <AttendTest />,
    roles: ['student'],
  },
  {
    path: '/test/evaluate',
    component: <Evaluate />,
    roles: ['teacher'],
  },
];

const LiveRouters = [
  {
    path: '/live',
    component: <Live />,
    roles: ['super-admin', 'admin', 'teacher', 'student'],
  },
  {
    path: '/live/video',
    component: <LiveVideo />,
    roles: ['super-admin', 'admin', 'teacher', 'student'],
  },
];

const DoubtRouters = [
  {
    path: '/my-doubts',
    component: <MyDoubts />,
    roles: ['student'],
  },
];


const noteRouters = [
  {
    path: '/my-notes',
    component: <MyNotes />,
    roles: ['student'],
  },
];

const announcementRouters = [
  {
    path: '/announcement',
    component: <TopicDetail />,
    roles: ['super-admin', 'admin', 'teacher', 'student'],
  },
]

const homeworkRouters = [
  {
    path: '/homework',
    component: <Homework />,
    roles: ['teacher', 'student'],
  },
]

export const ProtectedRouters = (props) => {
  const { role } = props;

  return (
    <>
      {[
        ...Temp,
        ...userRouters,
        ...GroupRouters,
        ...DashboardRouters,
        ...SubjectRouters,
        ...LessonRouters,
        ...TopicRouters,
        ...TestRouters,
        ...LiveRouters,
        ...DoubtRouters,
        ...noteRouters,
        ...announcementRouters,
        ...homeworkRouters,
      ].map((router, findIndex) => (
        <Protect
          key={findIndex}
          path={router.path}
          component={
            router.roles?.includes(role) ? router.component : UNAUTHORIZED
          }
        />
      ))}
    </>
  );
};
