import {
  IconDashboard,
  IconGroup,
  IconTeacher,
  IconStudent,
  IconAdmin,
  IconSubject,
  IconLiveClass,
  IconAnnouncement,
  IconNotes,
  IconDoubt,
  IconTest,
  IconHomework,
} from '../components/asset/icons';

export const ROLE = {
  superAdmin: 'super-admin',
  admin: 'admin',
  teacher: 'teacher',
  student: 'student',
};

//roles
export const ROLES = ['super-admin', 'admin', 'teacher', 'student'];

export const LEFT_NAV_LIST = {
  'super-admin': [
    {
      Icon: IconDashboard,
      name: 'Dashboard',
      path: '/dashboard',
    },
    {
      Icon: IconGroup,
      name: 'Groups',
      path: '/group',
    },
    {
      Icon: IconAdmin,
      name: 'Admins',
      path: '/user/admin',
    },
    {
      Icon: IconTeacher,
      name: 'Teachers',
      path: '/user/teacher',
    },
    {
      Icon: IconStudent,
      name: 'Students',
      path: '/user/student',
    },
    {
      Icon: IconAnnouncement,
      name: 'Announcements',
      path: '/announcement',
    },
  ],
  admin: [
    {
      Icon: IconDashboard,
      name: 'Dashboard',
      path: '/dashboard',
    },
    {
      Icon: IconGroup,
      name: 'Groups',
      path: '/group',
    },
    {
      Icon: IconAnnouncement,
      name: 'Announcements',
      path: '/announcement',
    },
  ],
  teacher: [
    {
      Icon: IconDashboard,
      name: 'Dashboard',
      path: '/dashboard',
    },
    {
      Icon: IconGroup,
      name: 'Groups',
      path: '/group',
    },
    {
      Icon: IconHomework,
      name: 'Homework',
      path: '/homework',
    },
    {
      Icon: IconTest,
      name: 'Exams',
      path: '/test',
    },
    {
      Icon: IconLiveClass,
      name: 'Live Classes',
      path: '/live',
    },
    {
      Icon: IconAnnouncement,
      name: 'Announcements',
      path: '/announcement',
    },
  ],
  student: [
    {
      Icon: IconDashboard,
      name: 'Dashboard',
      path: '/dashboard',
    },
    {
      Icon: IconSubject,
      name: 'Subjects',
      path: '/subject',
    },
    {
      Icon: IconHomework,
      name: 'Homework',
      path: '/homework',
    },
    {
      Icon: IconTest,
      name: 'Exams',
      path: '/test',
    },
    {
      Icon: IconLiveClass,
      name: 'Live Classes',
      path: '/live',
    },
    {
      Icon: IconDoubt,
      name: 'Doubts',
      path: '/my-doubts',
    },
    {
      Icon: IconNotes,
      name: 'Notes',
      path: '/my-notes',
    },
    {
      Icon: IconAnnouncement,
      name: 'Announcements',
      path: '/announcement',
    },
  ],
};
