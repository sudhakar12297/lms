export const errorMessage = {
    400: 'Something went wrong',
    1000: 'Invalid Email id',
    1004: 'Invalid Mobile number',
    1005: 'Invalid Password',
    1010: 'Please enter all values',
    1013: 'User id already exists, please create a new one',
    1014: 'Subject code already exists'
}