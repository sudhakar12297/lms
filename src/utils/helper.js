import { toast } from 'react-toastify';
import { ROLE } from "./common";
import moment from 'moment';
import _ from 'lodash';
import Cookie from "react-cookies";
import { errorMessage } from './errorMapping';

//check form field is invalid
export const isValidFormFieldData = (arrData) => {
  return arrData.every(
    (data) => data !== '' && data !== null && data !== undefined && data !== []
  );
};

//api call
export const apiCall = async (route, method, data) => {
  // eslint-disable-next-line no-undef
  const url = `${process.env.REACT_APP_SERVER_URL}${route}`;
  const token = getCookie('user');
  let options = {
    method,
    credentials: 'include',
    headers: {
      // 'Authorization': `Bearer ${jwt}`,
      token,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  };
  return await fetch(url, options).then((response) => {
    if (!response.ok) {
      response.json().then((result) => {
        console.log(result);

        const { errors = [] } = result
        if (Array.isArray(errors)) {
          errors.forEach(error => {
            toast.error(errorMessage[error.errCode] || errorMessage['400'], {
              autoClose: 5000,
            });
          })
        } else {

        }
      });
    }
    return response;
  });
};

//check response is ok
export const checkAPIResponse = (response) => {
  if (!response.ok) throw response;
  return response;
};

export const convertToStringIfUndefined = (data) => {
  return data === undefined ? '' : data;
};

export const toTitleCase = (str) => {
  return str.replace(
    /\w\S*/g,
    function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
};

export const isTeacher = (role) => {
  return role === ROLE.teacher
}

export const isStudent = (role) => {
  return role === ROLE.student
}

export const isTeacherOrStudent = (role) => {
  return role === ROLE.teacher || role === ROLE.student
}

export const isAdmin = (role) => {
  return role === ROLE.admin
}

export const isSuperAdmin = (role) => {
  return role === ROLE.superAdmin
}

export const isAdmins = (role) => {
  return role === ROLE.admin || role === ROLE.superAdmin
}

export const getGroupNameById = ({ groups, id }) => {
  const filteredGroup = groups.filter(group => group._id === id)
  return filteredGroup[0]?.name
}

export const getNumber = (str, defaultValue = 0) => {
  if (isNaN(parseInt(str))) return defaultValue;
  return parseInt(str);
}

export const isToday = (date) => moment().diff(date, 'days') == 0

export const isCurrentTimeInBetweenStartAndEndOfLive = (start, timing) => {
  const endTime = moment(start).add(timing, 'minutes')
  return (moment().isAfter(start) && moment().isBefore(endTime))
}

export const getOnGoingHomeworks = (homeworks) => {
  const a = homeworks?.filter(homework => {
    let { assignedDate, dueDate } = homework
    return moment(assignedDate, 'YYYY-MM-DD').isSameOrBefore(moment().format('YYYY-MM-DD'))
      && moment(dueDate, 'YYYY-MM-DD').isSameOrAfter(moment().format('YYYY-MM-DD'))
  })
  return a
}

export const arrayEqual = (arr1, arr2) => {
  const _arr1 = arr1.sort();
  const _arr2 = arr2.sort();
  if (arr1.length != arr2.length) {
    return false;
  }
  return _.isEqual(_arr1, _arr2);
}

// export const filterSubjects = (subjects, user) => {
//   const { role = '', subjectId = [] } = user;
//   if (role == 'teacher' || role == 'student') {
//     return subjects.filter(subject => subjectId.includes(subject._id));
//   }
//   return subjects;
// };

export const addToCookie = (key, value) => {
  Cookie.save(key, value, { path: "/", maxAge: 86400, domain: 'eduzy.co' });
};

export const removeCookie = (key) => {
  Cookie.remove(key, { path: '/', domain: 'eduzy.co' });
};

export const getCookie = (key) => {
  return Cookie.load(key);
};


export const isEmpty = (obj) => {

  if (obj == undefined || obj == 0 || obj == "0" || obj === false || obj == null) {
    return true;
  }

  if (_.isObject(obj) && _.size(obj) === 0) {
    return true;
  }

  if (_.isNumber(obj) || _.isBoolean(obj) || _.isDate(obj) || (_.isObject(obj) && _.size(obj) > 0)) {
    return false;
  }

  return _.isEmpty(obj);
}