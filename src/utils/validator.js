import { isUndefined } from "lodash";
import { isEmpty } from './helper'

import Json from './validatorJson'

const validator = (field, value, key, restConfig, payload) => {
    const { minLength = 0, maxLength = 0, message = '', fields = {} } = restConfig[key] || {};
    switch (key) {
        case "nonEmpty": {
            if (isEmpty(value)) {
                return { valid: false, message: message ? message : `${field} can not be blank` };
            }
            return { valid: true };
        }

        case "selectionNonEmpty": {
            if (isEmpty(value)) {
                return { valid: false, message: message ? message : `Please select ${field}` };
            }
            return { valid: true };
        }

        case "nonEmptyOnCondition": {
            const fieldKeys = Object.keys(fields);
            const valid = fieldKeys.reduce((accum, _key) => {
                const data = isUndefined(payload[_key]) ? "" : payload[_key];
                return accum && fields[_key].includes(data);
            }, true);
            if (valid && isEmpty(value)) {
                return { valid: false, message: message ? message : `${field} can not be blank` };
            }
            return { valid: true };
        }

        case "email": {
            if (value !== "") {
                const valid = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
                if (valid) {
                    return { valid: true };
                } else {
                    return { valid: false, message: message ? message : `Invalid ${field} format` };
                }
            }
            return { valid: true };
        }

        case "minLength": {
            if (value.length < minLength) {
                return { valid: false, message: message ? message : `Minimum length should be ${minLength}` };
            }
            return { valid: true };
        }

        default:
            return { valid: true };
    }
};

const runValidation = (value, field, validations, restConfig, payload) => {
    for (let index = 0; index < validations.length; index++) {
        const res = validator(field, value, validations[index], restConfig, payload);
        if (!res.valid) {
            return res;
        }
    }
    return {
        valid: true,
        message: "",
    };
};

/**
 *
 * @param {*} payload
 * @param {*} validateJson
 * {
 * firstName: ["nonEmpty"]
 * }
 *
 *
 *
 *
 *
 */
const validateEngine = (payload, validateJsonKey) => {
    const validateJson = Json[validateJsonKey] || {};
    const validateKeys = Object.keys(validateJson);
    const validationResult = validateKeys.reduce((accum, key) => {
        const value = isUndefined(payload[key]) ? "" : payload[key];
        const { field = "", validations = [], restConfig = {} } = validateJson[key];
        const result = runValidation(value, field, validations, restConfig, payload);
        const { message = "" } = result;
        return {
            ...accum,
            [key]: message,
        };
    }, {});

    const validationKeys = Object.keys(validationResult);
    const result = validationKeys.reduce((accum, data) => {
        return accum && validationResult[data] === "";
    }, true);
    return { result, errors: validationResult };
};

export default validateEngine;
