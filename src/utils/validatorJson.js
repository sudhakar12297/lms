const Json = {
    userCreateJson: {
        firstName: { field: "FirstName", validations: ["nonEmpty"] },
        group: { field: 'Group', validations: ["selectionNonEmpty"] },
        subjectIds: { field: 'Subject', validations: ["selectionNonEmpty"] },
        id: { field: 'Id', validations: ["nonEmpty"] },
        email: { field: "Email", validations: ["nonEmpty", "email"] },
        mobile: { field: "Mobile", validations: ["nonEmpty"] },
        gender: { field: "Gender", validations: ["selectionNonEmpty"] },
    },
    studentCreateJson: {
        firstName: { field: "FirstName", validations: ["nonEmpty"] },
        group: { field: 'Group', validations: ["selectionNonEmpty"] },
        subjectIds: { field: 'Subject', validations: ["selectionNonEmpty"] },
        id: { field: 'Id', validations: ["nonEmpty"] },
        email: { field: "Email", validations: ["nonEmpty", "email"] },
        mobile: { field: "Mobile", validations: ["nonEmpty"] },
        gender: { field: "Gender", validations: ["selectionNonEmpty"] },
        tutor: { field: 'Tutor', validations: ["selectionNonEmpty"] },
    },
    groupCreateJson: {
        name: { field: "Group Name", validations: ["nonEmpty"] },
    },
    subjectCreateJson: {
        name: { field: "Subject Name", validations: ["nonEmpty"] },
        code: { field: "Subject Code", validations: ["nonEmpty"] },
    },
    lessonCreateJson: {
        name: { field: "Lesson Name", validations: ["nonEmpty"] },
        desc: {
            field: "Desciption", validations: ["nonEmpty", "minLength"],
            restConfig: { minLength: { minLength: 2, message: 'Description should be minimum 2 Characters' } }
        },
    },
    topicCreateJson: {
        name: { field: "Topic Name", validations: ["nonEmpty"] },
    },
    homewrokCreation: {
        name: { field: "Homework Name", validations: ["nonEmpty"] },
        description: {
            field: "Description", validations: ["nonEmpty", "minLength"],
            restConfig: { minLength: { minLength: 2, message: 'Description should be minimum 2 Characters' } }
        },
        groupId: { field: "Group", validations: ["selectionNonEmpty"] },
        subjectId: { field: "Subject", validations: ["selectionNonEmpty"] },
        chapterId: { field: "Chapter", validations: ["selectionNonEmpty"] },
        topicIds: { field: "Topic", validations: ["selectionNonEmpty"] },
        estimatedTime: { field: "Estimated Time", validations: ["nonEmpty"] },
    },
    testCreation: {
        name: { field: "Test Name", validations: ["nonEmpty"] },
        groupId: { field: "Group", validations: ["selectionNonEmpty"] },
        subjectId: { field: "Subject", validations: ["selectionNonEmpty"] },
        lessonId: { field: "Lesson", validations: ["selectionNonEmpty"] },
        topicId: { field: "Topic", validations: ["selectionNonEmpty"] },
        timing: {
            field: "Estimated Time", validations: ["nonEmptyOnCondition"],
            restConfig: { nonEmptyOnCondition: { fields: { isPractice: [false, ''] } } }
        },
    }
}

export default {
    ...Json
}